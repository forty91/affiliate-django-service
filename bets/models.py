from decimal import Decimal

from django.db import models

from base.consts import Currency
from base.help_texts import CYBERBET_HASH, PLAYER, BET_STAKE, BET_INCOME, BET_OUTCOME, CURRENCY, CREATED_AT, \
    MODIFIED_AT, BET_STATUS, BET_IS_FREEBET, BET_STATE, BET_INCOME_EUR, BET_OUTCOME_EUR, BET_NET_GAMING_EUR, \
    BET_PROFIT_EUR, CURRENCY_RATE
from base.models import AffiliateModel
from bets.consts import Status, State


class Bet(AffiliateModel):
    cyberbet_hash = models.CharField(max_length=255, unique=True, null=False, help_text=CYBERBET_HASH)
    player = models.ForeignKey('players.Player', on_delete=models.CASCADE, related_name='bets', help_text=PLAYER)
    stake = models.DecimalField(default=Decimal(0), max_digits=12, decimal_places=4, help_text=BET_STAKE)
    income = models.DecimalField(default=Decimal(0), max_digits=12, decimal_places=4, help_text=BET_INCOME)
    outcome = models.DecimalField(default=Decimal(0), max_digits=12, decimal_places=4, help_text=BET_OUTCOME)
    currency = models.SlugField(null=True, choices=Currency.choices, help_text=CURRENCY)
    currency_rate = models.DecimalField(default=Decimal(0), max_digits=12, decimal_places=4, help_text=CURRENCY_RATE)
    income_eur = models.DecimalField(default=Decimal(0), max_digits=12, decimal_places=4, help_text=BET_INCOME_EUR)
    outcome_eur = models.DecimalField(default=Decimal(0), max_digits=12, decimal_places=4, help_text=BET_OUTCOME_EUR)
    net_gaming = models.DecimalField(default=Decimal(0), max_digits=12, decimal_places=4, help_text=BET_NET_GAMING_EUR)
    profit = models.DecimalField(default=Decimal(0), max_digits=12, decimal_places=4, help_text=BET_PROFIT_EUR)
    status = models.SlugField(null=True, choices=Status.choices, default=Status.PENDING, help_text=BET_STATUS)
    state = models.SlugField(null=True, choices=State.choices, default=State.UNKNOWN, help_text=BET_STATE)
    is_freebet = models.BooleanField(default=False, help_text=BET_IS_FREEBET)
    created_at = models.DateTimeField(auto_now_add=True, help_text=CREATED_AT)
    modified_at = models.DateTimeField(auto_now=True, help_text=MODIFIED_AT)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.original_state = self.state
