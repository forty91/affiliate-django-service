from collections import namedtuple

from drf_yasg.utils import swagger_auto_schema
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.filters import OrderingFilter
from django_filters import rest_framework as filters
from rest_framework.status import HTTP_200_OK, HTTP_201_CREATED

from api_public.pagination import AffiliatePagination
from api_public.permissions import IsPartner, staff_permission
from base.utils.currency import aggregate_to_one_currency
from base.utils.db import aggregate_currencies_sum
from base.views import AffiliateViewSet, AffiliateListModelMixin, AffiliateRetrieveModelMixin
from bets.bl import CreateUpdateBet
from bets.consts import Status
from bets.models import Bet
from bets.rest.filters import BetFilter
from bets.rest.serializers import BetSerializer, BetCreateModifySerializer, BetSumSerializer
from permissions.consts import Block, Action


class BetViewSet(AffiliateViewSet, AffiliateListModelMixin, AffiliateRetrieveModelMixin):
    """
    list:
    For staff user return a list of all the existing bets.
    For partner user return a list with bets which corresponds to players who went on promo created by current partner.

    retrieve:
    Return a single bet. Bet must exists in list for current user.

    create:
    Create a new bet instance. Allow for users with staff permissions only.

    sum:
    Return bets sums by filtered with query parameters queryset.
    """
    serializer_class = BetSerializer
    queryset = Bet.objects.all()
    queryset_user_lookup_expr = 'player__promo__partner__user'
    pagination_class = AffiliatePagination
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter)
    filter_class = BetFilter
    ordering_fields = ('modified_at', 'created_at')
    ordering = ('-modified_at', )

    permission_classes_by_action = {
        'create': [staff_permission(Block.BETS, Action.CREATE)],
        'list': [IsPartner, staff_permission(Block.BETS, Action.READ)],
        'sum': [IsPartner, staff_permission(Block.BETS, Action.READ)],
        'retrieve': [IsPartner, staff_permission(Block.BETS, Action.READ)]
    }

    @swagger_auto_schema(request_body=BetCreateModifySerializer,
                         responses={HTTP_200_OK: BetSerializer, HTTP_201_CREATED: BetSerializer})
    def create(self, request):
        serializer = BetCreateModifySerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        bet, status = CreateUpdateBet(serializer.validated_data).run()
        return Response(data=BetSerializer(instance=bet).data,
                        status=status)

    @swagger_auto_schema(responses={HTTP_200_OK: BetSumSerializer})
    @action(methods=['get'], detail=False, url_path='sum')
    def sum(self, request, *args, **kwargs):
        """
        Расчет суммы денег для принятых ставок и суммы проигранных денег.
        """
        self.queryset_user_filter = {self.queryset_user_lookup_expr: self.request.user}
        bets = self.filter_queryset(self.get_queryset())
        successed_bets = bets.filter(status=Status.ACCEPTED)

        aggregated_data = {
            'stake_sum': aggregate_currencies_sum(successed_bets, 'stake'),
            'outcome_sum': aggregate_currencies_sum(successed_bets, 'outcome'),
            'income_sum': aggregate_currencies_sum(successed_bets, 'income'),
        }

        aggregated_data_eur = {
            _sum: aggregate_to_one_currency(aggregated_data[_sum])
            for _sum in ('stake_sum', 'outcome_sum', 'income_sum')
        }

        instance = namedtuple('instance', ['stake_sum', 'outcome_sum', 'income_sum',
                                           'stake_sum_eur', 'outcome_sum_eur', 'income_sum_eur'])(
            stake_sum=aggregated_data['stake_sum'],
            outcome_sum=aggregated_data['outcome_sum'],
            income_sum=aggregated_data['income_sum'],
            stake_sum_eur=aggregated_data_eur['stake_sum'],
            outcome_sum_eur=aggregated_data_eur['outcome_sum'],
            income_sum_eur=aggregated_data_eur['income_sum'],
        )
        return Response(data=BetSumSerializer(instance=instance).data,
                        status=HTTP_200_OK)
