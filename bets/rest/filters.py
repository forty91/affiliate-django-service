from django_filters import rest_framework as filters

from api_public.filters import IsoDateTimeFilter
from base.help_texts import CYBERBET_HASH_EXACT, PLAYER_EXACT, STAKE_EXACT, STAKE_LTE, STAKE_GTE, INCOME_EXACT, \
    INCOME_LTE, INCOME_GTE, OUTCOME_EXACT, OUTCOME_LTE, OUTCOME_GTE, CURRENCY_EXACT, CREATED_AT_LTE, CREATED_AT_GTE, \
    MODIFIED_AT_LTE, MODIFIED_AT_GTE, BET_STATUS_EXACT, BET_IS_FREEBET_BOOL, PROMO_EXACT, PROMO_NAME_ICONTAINS, \
    BET_STATE_EXACT
from bets.models import Bet


class BetFilter(filters.FilterSet):
    cyberbet_hash = filters.CharFilter(field_name='cyberbet_hash', help_text=CYBERBET_HASH_EXACT)
    player = filters.UUIDFilter(field_name='player', help_text=PLAYER_EXACT)
    promo = filters.UUIDFilter(field_name='player__promo', help_text=PROMO_EXACT)
    promo__name = filters.CharFilter(field_name='player__promo', lookup_expr='name__icontains', help_text=PROMO_NAME_ICONTAINS)
    stake = filters.NumberFilter(field_name='stake', help_text=STAKE_EXACT)
    stake__lte = filters.NumberFilter(field_name='stake', lookup_expr='lte', help_text=STAKE_LTE)
    stake__gte = filters.NumberFilter(field_name='stake', lookup_expr='gte', help_text=STAKE_GTE)
    income = filters.NumberFilter(field_name='income', help_text=INCOME_EXACT)
    income__lte = filters.NumberFilter(field_name='income', lookup_expr='lte', help_text=INCOME_LTE)
    income__gte = filters.NumberFilter(field_name='income', lookup_expr='gte', help_text=INCOME_GTE)
    outcome = filters.NumberFilter(field_name='outcome', help_text=OUTCOME_EXACT)
    outcome__lte = filters.NumberFilter(field_name='outcome', lookup_expr='lte', help_text=OUTCOME_LTE)
    outcome__gte = filters.NumberFilter(field_name='outcome', lookup_expr='gte', help_text=OUTCOME_GTE)
    currency = filters.CharFilter(field_name='currency', help_text=CURRENCY_EXACT)
    status = filters.CharFilter(field_name='status', help_text=BET_STATUS_EXACT)
    state = filters.CharFilter(field_name='state', help_text=BET_STATE_EXACT)
    is_freebet = filters.BooleanFilter(field_name='is_freebet', help_text=BET_IS_FREEBET_BOOL)
    created_at__lte = IsoDateTimeFilter(field_name='created_at', lookup_expr='lte', help_text=CREATED_AT_LTE)
    created_at__gte = IsoDateTimeFilter(field_name='created_at', lookup_expr='gte', help_text=CREATED_AT_GTE)
    modified_at__lte = IsoDateTimeFilter(field_name='modified_at', lookup_expr='lte', help_text=MODIFIED_AT_LTE)
    modified_at__gte = IsoDateTimeFilter(field_name='modified_at', lookup_expr='gte', help_text=MODIFIED_AT_GTE)

    class Meta:
        model = Bet
        fields = ('cyberbet_hash', 'player', 'stake', 'income', 'outcome', 'currency', 'status', 'state', 'is_freebet')
