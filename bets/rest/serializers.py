from decimal import Decimal

from drf_queryfields import QueryFieldsMixin
from rest_framework import serializers

from base.help_texts import CYBERBET_HASH, CYBERBET_ID, BET_STAKE, BET_INCOME, BET_OUTCOME, CURRENCY, \
    BET_STAKE_SUM, BET_OUTCOME_SUM, BET_INCOME_SUM, BET_STATUS, BET_IS_FREEBET, \
    BET_STAKE_SUM_EUR, BET_OUTCOME_SUM_EUR, BET_INCOME_SUM_EUR, BET_STATE, BET_OUTCOME_EUR, BET_INCOME_EUR, \
    BET_NET_GAMING_EUR, BET_PROFIT_EUR
from bets.consts import Status, State
from bets.models import Bet
from api_public.validators import check_player_cyberbet_id
from base.consts import Currency
from api_public.serializers import PlayerNameSerializer


class BetSerializer(serializers.ModelSerializer):
    player = PlayerNameSerializer()
    stake = serializers.DecimalField(max_digits=9, decimal_places=2, min_value=Decimal(0),
                                     help_text=BET_STAKE)
    income = serializers.DecimalField(max_digits=9, decimal_places=2, min_value=Decimal(0),
                                      help_text=BET_INCOME)
    outcome = serializers.DecimalField(max_digits=9, decimal_places=2, min_value=Decimal(0),
                                       help_text=BET_OUTCOME)
    income_eur = serializers.DecimalField(max_digits=9, decimal_places=2, min_value=Decimal(0),
                                          help_text=BET_INCOME_EUR)
    outcome_eur = serializers.DecimalField(max_digits=9, decimal_places=2, min_value=Decimal(0),
                                           help_text=BET_OUTCOME_EUR)
    net_gaming = serializers.DecimalField(max_digits=9, decimal_places=2, min_value=Decimal(0),
                                          help_text=BET_NET_GAMING_EUR)
    profit = serializers.DecimalField(max_digits=9, decimal_places=2, min_value=Decimal(0),
                                      help_text=BET_PROFIT_EUR)

    class Meta:
        model = Bet
        fields = ('id', 'cyberbet_hash', 'player', 'stake', 'income', 'outcome', 'currency',
                  'income_eur', 'outcome_eur', 'net_gaming', 'profit', 'status', 'state', 'is_freebet',
                  'created_at', 'modified_at')


class BetCreateModifySerializer(QueryFieldsMixin, serializers.Serializer):
    cyberbet_hash = serializers.CharField(max_length=255, allow_null=False, allow_blank=False, help_text=CYBERBET_HASH)
    player_cyberbet_id = serializers.IntegerField(min_value=1, validators=[check_player_cyberbet_id],
                                                  help_text=CYBERBET_ID)
    stake = serializers.DecimalField(max_digits=12, decimal_places=4, min_value=Decimal(0), required=False,
                                     help_text=BET_STAKE)
    income = serializers.DecimalField(max_digits=12, decimal_places=4, min_value=Decimal(0), required=False,
                                      help_text=BET_INCOME)
    outcome = serializers.DecimalField(max_digits=12, decimal_places=4, min_value=Decimal(0), required=False,
                                       help_text=BET_OUTCOME)
    currency = serializers.ChoiceField(choices=Currency.choices, default=Currency.EUR, help_text=CURRENCY)
    status = serializers.ChoiceField(choices=Status.choices, allow_null=False, required=False, help_text=BET_STATUS)
    state = serializers.ChoiceField(choices=State.choices, allow_null=False, required=False, help_text=BET_STATE)
    is_freebet = serializers.BooleanField(required=False, help_text=BET_IS_FREEBET)


class BetSumSerializer(serializers.Serializer):
    stake_sum = serializers.DictField(help_text=BET_STAKE_SUM)
    outcome_sum = serializers.DictField(help_text=BET_OUTCOME_SUM)
    income_sum = serializers.DictField(help_text=BET_INCOME_SUM)
    stake_sum_eur = serializers.FloatField(help_text=BET_STAKE_SUM_EUR)
    outcome_sum_eur = serializers.FloatField(help_text=BET_OUTCOME_SUM_EUR)
    income_sum_eur = serializers.FloatField(help_text=BET_INCOME_SUM_EUR)
