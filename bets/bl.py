from decimal import Decimal

from rest_framework.status import HTTP_200_OK, HTTP_201_CREATED

from base.utils.currency import get_currency_rates
from base.utils.mongo import MongoLogging
from bets.consts import BET_COLLECTION, BetAction
from bets.models import Bet
from bets.rest.serializers import BetSerializer
from players.models import Player


class CreateUpdateBet:
    def __init__(self, validated_data):
        self.validated_data = validated_data
        self.action = BetAction.CREATE

    def run(self):
        cyberbet_hash = self.validated_data['cyberbet_hash']
        player = Player.objects.get(cyberbet_id=self.validated_data['player_cyberbet_id'])

        if Bet.objects.filter(cyberbet_hash=cyberbet_hash).exists():
            self.action = BetAction.UPDATE

        bet, _ = Bet.objects.get_or_create(
            cyberbet_hash=cyberbet_hash,
            player=player
        )

        for attr in ('income', 'outcome', 'stake', 'currency', 'status', 'state', 'is_freebet'):
            attr_value = self.validated_data.get(attr)
            if attr_value is not None:
                setattr(bet, attr, attr_value)

        bet = self.calculate_attrs(bet)
        bet.save()

        MongoLogging().log(collection=BET_COLLECTION, action=self.action,
                           data=BetSerializer(instance=bet).data)

        status = {
            BetAction.CREATE: HTTP_201_CREATED,
            BetAction.UPDATE: HTTP_200_OK
        }[self.action]

        return bet, status

    def calculate_attrs(self, bet):
        """
        Расчет вычисляемых полей ставки.
        """
        if self.action == BetAction.CREATE:
            currency_rates = get_currency_rates()
            bet.currency_rate = Decimal(currency_rates[bet.currency])

        program_rate = bet.player.promo.program.rate
        bet.outcome_eur = bet.outcome / bet.currency_rate
        bet.income_eur = bet.income / bet.currency_rate
        bet.net_gaming = bet.outcome_eur - bet.income_eur
        # TODO в зависимости от программы, по разному считается profit, формула будет браться из ProgramType
        bet.profit = bet.net_gaming * Decimal(program_rate / 100)

        return bet
