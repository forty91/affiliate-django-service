# Generated by Django 2.1.7 on 2019-02-26 10:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bets', '0007_auto_20190225_1552'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bet',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, help_text='Creation UTC datetime.'),
        ),
        migrations.AlterField(
            model_name='bet',
            name='modified_at',
            field=models.DateTimeField(auto_now=True, help_text='Last modification UTC datetime.'),
        ),
    ]
