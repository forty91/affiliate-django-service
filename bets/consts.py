from base.utils.enumerate import BaseEnumerate


class Status(BaseEnumerate):
    PENDING = 'pending'
    ACCEPTED = 'accepted'
    REJECTED = 'rejected'
    CANCELED = 'canceled'
    FAILED = 'failed'


class State(BaseEnumerate):
    UNKNOWN = 'unknown'
    WIN = 'win'
    LOSE = 'lose'
    REFUND = 'refund'


BET_COLLECTION = 'bets'


class BetAction(BaseEnumerate):
    CREATE = 'create'
    UPDATE = 'update'
