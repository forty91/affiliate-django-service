import uuid
from decimal import Decimal

import factory

from bets.consts import Status, State
from bets.models import Bet
from base.consts import Currency
from players.tests.factories import PlayerFactory


class BetFactory(factory.django.DjangoModelFactory):
    cyberbet_hash = factory.LazyAttribute(lambda x: str(uuid.uuid4()))
    player = factory.SubFactory(PlayerFactory)
    stake = Decimal(10)
    income = Decimal(20)
    outcome = Decimal(10)
    income_eur = Decimal(20)
    outcome_eur = Decimal(10)
    net_gaming = factory.LazyAttribute(lambda x: x.outcome_eur - x.income_eur)
    profit = factory.LazyAttribute(lambda x: x.net_gaming * Decimal(x.player.promo.program.rate / 100))
    currency = Currency.EUR
    currency_rate = Decimal(1)
    status = Status.ACCEPTED
    state = State.WIN
    is_freebet = False

    class Meta:
        model = Bet
