import uuid

from django.test import TestCase
from rest_framework.exceptions import ValidationError

from base.consts import Currency
from bets.rest.serializers import BetCreateModifySerializer


class BetCreateModifySerializerTestCase(TestCase):
    def test_incorrect_player_cyberbet_id(self):
        cyberbet_hash = str(uuid.uuid4())
        player_cyberbet_id = 12345
        stake = 10.0
        currency = Currency.EUR

        income = 0.0
        outcome = 0.0
        seconds = 1540893894
        nanos = 440214540

        data = {
            "cyberbet_hash": cyberbet_hash,
            "player_cyberbet_id": player_cyberbet_id,
            "stake": stake,
            "income": income,
            "outcome": outcome,
            "currency": currency,
            "created_at": {
                "seconds": seconds,
                "nanos": nanos
            }
        }

        serializer = BetCreateModifySerializer(data=data)

        with self.assertRaisesRegex(
                ValidationError,
                "player with such cyberbet_id not exists."
        ):
            serializer.is_valid(raise_exception=True)
