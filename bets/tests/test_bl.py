import uuid
import datetime
from decimal import Decimal

from django.test import TestCase, tag
from freezegun import freeze_time
from rest_framework.status import HTTP_200_OK

from base.consts import Currency
from base.utils.mongo import MongoLogging
from bets.bl import CreateUpdateBet
from bets.tests.factories import BetFactory
from bets.consts import Status, BET_COLLECTION, BetAction
from bets.rest.serializers import BetSerializer
from players.tests.factories import PlayerFactory


class CreateUpdateBetTestCase(TestCase):
    def setUp(self):
        self.cyberbet_hash = str(uuid.uuid4())
        self.player = PlayerFactory()
        self.stake = Decimal(100.0)
        self.income = Decimal(222.22)
        self.outcome = Decimal(100.0)
        self.is_freebet = False
        self.currency = Currency.EUR
        self.status = Status.PENDING

    @tag('local')
    def test_update_mongo_logging(self):
        now = datetime.datetime(2018, 10, 10, 10, 10, 10)
        player = PlayerFactory()
        bet = BetFactory(player=player)
        validated_data = {
            "cyberbet_hash": bet.cyberbet_hash,
            "player_cyberbet_id": player.cyberbet_id,
            "stake": self.stake,
            "income": self.income,
            "outcome": self.outcome,
            "currency": self.currency,
            "status": Status.ACCEPTED,
            "is_freebet": self.is_freebet,
        }
        with freeze_time(now):
            bet, status = CreateUpdateBet(validated_data).run()

        logged_data = BetSerializer(instance=bet).data
        logged_bet = MongoLogging().get(collection=BET_COLLECTION, data=logged_data)
        logged_bet.pop('_id', None)

        self.assertEqual(status, HTTP_200_OK)
        self.assertEqual(logged_bet, dict(**logged_data, **{'action': BetAction.UPDATE, 'timestamp': now}))
