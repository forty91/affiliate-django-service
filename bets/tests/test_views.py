import datetime
import uuid

from django.test import tag
from django.urls import reverse
from freezegun import freeze_time
from mock import patch
from rest_framework.status import HTTP_201_CREATED, HTTP_200_OK

from base.consts import Currency
from base.tests import AffiliateAPITestCase
from base.utils.currency import aggregate_to_one_currency
from bets.consts import Status, State
from bets.models import Bet
from api_public.serializers import PlayerNameSerializer
from bets.tests.factories import BetFactory
from players.tests.factories import PlayerFactory
from base.utils.datetime import utc_datetime_to_iso
from projects.tests.factories import ProgramFactory
from promos.tests.factories import PromoFactory
from users.tests.factories import PartnerFactory


class BetViewSetTestCase(AffiliateAPITestCase):
    @patch('bets.bl.get_currency_rates', autospec=True)
    def test_create_and_modify(self, get_currency_rates_mock):
        """
        В cyberbet происходит создание ставки (ожидает подтверждения, счет не меняется),
        затем подтверждение ее (снятие денег со счета),
        затем ставка выигрывает с коэффициентом 2.0 (пополнение счета),
        затем ставка была отменена.
        """
        currency_rates = {'gbp': 0.8, 'eur': 1.0, 'usd': 1.1, 'rub': 74.4}
        get_currency_rates_mock.return_value = currency_rates

        long_long_long_ago = datetime.datetime(2018, 7, 7, 7, 7, 7)
        long_long_ago = datetime.datetime(2018, 8, 8, 8, 8, 8)
        long_ago = datetime.datetime(2018, 9, 9, 9, 9, 9)
        now = datetime.datetime(2018, 10, 10, 10, 10, 10)
        url = reverse('public:bet-list')
        cyberbet_hash = str(uuid.uuid4())
        partner = PartnerFactory()
        program_rate = 77
        program = ProgramFactory(rate=program_rate)
        promo = PromoFactory(partner=partner, program=program)
        player = PlayerFactory(promo=promo)
        stake = 10.0
        currency = Currency.RUB

        create_income = 0.0
        create_outcome = 0.0
        create_data = {
            "cyberbet_hash": cyberbet_hash,
            "player_cyberbet_id": player.cyberbet_id,
            "stake": stake,
            "income": create_income,
            "outcome": create_outcome,
            "currency": currency,
            "status": Status.PENDING,
            "state": State.UNKNOWN,
            "is_freebet": False,
        }

        with freeze_time(long_long_long_ago):
            response = self.client.post(url, create_data, format='json',
                                        HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token)
        created_bet = Bet.objects.get(cyberbet_hash=cyberbet_hash)

        create_income_eur = create_income / currency_rates[currency]
        create_outcome_eur = create_outcome / currency_rates[currency]
        create_net_gaming = create_outcome_eur - create_income_eur
        create_profit = create_net_gaming * program_rate / 100
        expected_data = {
            'id': created_bet.uuid,
            'cyberbet_hash': cyberbet_hash,
            'player': PlayerNameSerializer(instance=player).data,
            'stake': '%.2f' % stake,
            'income': '%.2f' % create_income,
            'outcome': '%.2f' % create_outcome,
            'currency': currency,
            'income_eur': '%.2f' % create_income_eur,
            'outcome_eur': '%.2f' % create_outcome_eur,
            'net_gaming': '%.2f' % create_net_gaming,
            'profit': '%.2f' % create_profit,
            'status': Status.PENDING,
            "state": State.UNKNOWN,
            'is_freebet': False,
            'created_at': utc_datetime_to_iso(long_long_long_ago),
            'modified_at': utc_datetime_to_iso(long_long_long_ago)
        }
        self.assertEqual(response.status_code, HTTP_201_CREATED)
        self.assertEqual(response.data, expected_data)

        accepted_income = 20.0
        accepted_outcome = 10.0
        accepted_data = {
            "cyberbet_hash": cyberbet_hash,
            "player_cyberbet_id": player.cyberbet_id,
            "stake": stake,
            "income": accepted_income,
            "outcome": accepted_outcome,
            "currency": currency,
            "status": Status.ACCEPTED,
            "state": State.UNKNOWN,
            "is_freebet": True,
        }

        with freeze_time(long_long_ago):
            response = self.client.post(url, accepted_data, format='json',
                                        HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token)

        accepted_income_eur = accepted_income / currency_rates[currency]
        accepted_outcome_eur = accepted_outcome / currency_rates[currency]
        accepted_net_gaming = accepted_outcome_eur - accepted_income_eur
        accepted_profit = accepted_net_gaming * program_rate / 100
        expected_data = {
            'id': created_bet.uuid,
            'cyberbet_hash': cyberbet_hash,
            'player': PlayerNameSerializer(instance=player).data,
            'stake': '%.2f' % stake,
            'income': '%.2f' % accepted_income,
            'outcome': '%.2f' % accepted_outcome,
            'currency': currency,
            'income_eur': '%.2f' % accepted_income_eur,
            'outcome_eur': '%.2f' % accepted_outcome_eur,
            'net_gaming': '%.2f' % accepted_net_gaming,
            'profit': '%.2f' % accepted_profit,
            'status': Status.ACCEPTED,
            "state": State.UNKNOWN,
            'is_freebet': True,
            'created_at': utc_datetime_to_iso(long_long_long_ago),
            'modified_at': utc_datetime_to_iso(long_long_ago)
        }
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertEqual(response.data, expected_data)

        accepted_income = 20.0
        accepted_outcome = 10.0
        accepted_data = {
            "cyberbet_hash": cyberbet_hash,
            "player_cyberbet_id": player.cyberbet_id,
            "stake": stake,
            "income": accepted_income,
            "outcome": accepted_outcome,
            "currency": currency,
            "status": Status.ACCEPTED,
            "state": State.WIN,
            "is_freebet": True,
        }

        with freeze_time(long_ago):
            response = self.client.post(url, accepted_data, format='json',
                                        HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token)

        accepted_income_eur = accepted_income / currency_rates[currency]
        accepted_outcome_eur = accepted_outcome / currency_rates[currency]
        accepted_net_gaming = accepted_outcome_eur - accepted_income_eur
        accepted_profit = accepted_net_gaming * program_rate / 100
        expected_data = {
            'id': created_bet.uuid,
            'cyberbet_hash': cyberbet_hash,
            'player': PlayerNameSerializer(instance=player).data,
            'stake': '%.2f' % stake,
            'income': '%.2f' % accepted_income,
            'outcome': '%.2f' % accepted_outcome,
            'currency': currency,
            'income_eur': '%.2f' % accepted_income_eur,
            'outcome_eur': '%.2f' % accepted_outcome_eur,
            'net_gaming': '%.2f' % accepted_net_gaming,
            'profit': '%.2f' % accepted_profit,
            'status': Status.ACCEPTED,
            "state": State.WIN,
            'is_freebet': True,
            'created_at': utc_datetime_to_iso(long_long_long_ago),
            'modified_at': utc_datetime_to_iso(long_ago)
        }
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertEqual(response.data, expected_data)

        win_income = 20.0
        win_outcome = 10.0
        win_data = {
            "cyberbet_hash": cyberbet_hash,
            "player_cyberbet_id": player.cyberbet_id,
            "stake": stake,
            "income": win_income,
            "outcome": win_outcome,
            "currency": currency,
            'status': Status.CANCELED,
            "state": State.REFUND,
        }

        with freeze_time(now):
            response = self.client.post(url, win_data, format='json',
                                        HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token)

        win_income_eur = win_income / currency_rates[currency]
        win_outcome_eur = win_outcome / currency_rates[currency]
        win_net_gaming = win_outcome_eur - win_income_eur
        win_profit = win_net_gaming * program_rate / 100
        expected_data = {
            'id': created_bet.uuid,
            'cyberbet_hash': cyberbet_hash,
            'player': PlayerNameSerializer(instance=player).data,
            'stake': '%.2f' % stake,
            'income': '%.2f' % win_income,
            'outcome': '%.2f' % win_outcome,
            'currency': currency,
            'income_eur': '%.2f' % win_income_eur,
            'outcome_eur': '%.2f' % win_outcome_eur,
            'net_gaming': '%.2f' % win_net_gaming,
            'profit': '%.2f' % win_profit,
            'status': Status.CANCELED,
            "state": State.REFUND,
            'is_freebet': True,
            'created_at': utc_datetime_to_iso(long_long_long_ago),
            'modified_at': utc_datetime_to_iso(now)
        }
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertEqual(response.data, expected_data)

    @tag('local')
    def test_get_sum(self):
        """
        Проверим корректность взятия сумм ставок для текущего партнера с фильтрацией по игроку и без
        """
        BetFactory(stake=15.0, income=20.0, outcome=15.0)
        BetFactory(stake=5.0, income=0.0, outcome=5.0)

        player1 = PlayerFactory(promo=PromoFactory(partner=self.partner))
        bet1_1 = BetFactory(stake=9.99, income=10.01, outcome=9.99, currency=Currency.EUR, player=player1)
        bet1_2 = BetFactory(stake=2.0, income=0.0, outcome=0.0, currency=Currency.RUB, status=Status.PENDING, player=player1)       # not succesed
        bet1_3 = BetFactory(stake=1, income=0, outcome=1, currency=Currency.USD, player=player1)
        bet1_4 = BetFactory(stake=3.01, income=7.2, outcome=0, currency=Currency.GBP, is_freebet=True, player=player1)              # freebet
        bet1_5 = BetFactory(stake=2.11, income=2.11, outcome=2.11, currency=Currency.EUR, status=Status.FAILED, player=player1)     # not succesed
        player2 = PlayerFactory(promo=PromoFactory(partner=self.partner))
        bet2_1 = BetFactory(stake=2, income=3.2, outcome=0, currency=Currency.EUR, player=player2)
        bet2_2 = BetFactory(stake=3.33, income=4.99, outcome=3.33, currency=Currency.RUB, player=player2)
        bet2_3 = BetFactory(stake=6.01, income=0, outcome=6.01, currency=Currency.USD, player=player2)
        bet2_4 = BetFactory(stake=15.0, income=7.01, outcome=15.0, currency=Currency.GBP, player=player2)
        bet2_5 = BetFactory(stake=2.11, income=2.11, outcome=2.11, currency=Currency.EUR, status=Status.CANCELED, player=player2)   # not succesed

        url = reverse('public:bet-sum')
        response = self.client.get(url, format='json', HTTP_AUTHORIZATION='JWT %s' % self.partner_jwt_token)

        expected_data = {
            'stake_sum': {
                Currency.EUR: round(bet1_1.stake + bet2_1.stake, 2),
                Currency.RUB: round(bet2_2.stake, 2),
                Currency.GBP: round(bet1_4.stake + bet2_4.stake, 2),
                Currency.USD: round(bet1_3.stake + bet2_3.stake, 2),
            },
            'outcome_sum': {
                Currency.EUR: round(bet1_1.outcome + bet2_1.outcome, 2),
                Currency.RUB: round(bet2_2.outcome, 2),
                Currency.GBP: round(bet1_4.outcome + bet2_4.outcome, 2),
                Currency.USD: round(bet1_3.outcome + bet2_3.outcome, 2),
            },
            'income_sum': {
                Currency.EUR: round(bet1_1.income + bet2_1.income, 2),
                Currency.RUB: round(bet2_2.income, 2),
                Currency.GBP: round(bet1_4.income + bet2_4.income, 2),
                Currency.USD: round(bet1_3.income + bet2_3.income, 2),
            },
            'stake_sum_eur': aggregate_to_one_currency(
                {
                    Currency.EUR: bet1_1.stake + bet2_1.stake,
                    Currency.RUB: bet2_2.stake,
                    Currency.GBP: bet1_4.stake + bet2_4.stake,
                    Currency.USD: bet1_3.stake + bet2_3.stake,
                }
            ),
            'outcome_sum_eur': aggregate_to_one_currency(
                {
                    Currency.EUR: bet1_1.outcome + bet2_1.outcome,
                    Currency.RUB: bet2_2.outcome,
                    Currency.GBP: bet1_4.outcome + bet2_4.outcome,
                    Currency.USD: bet1_3.outcome + bet2_3.outcome,
                }
            ),
            'income_sum_eur': aggregate_to_one_currency(
                {
                    Currency.EUR: bet1_1.income + bet2_1.income,
                    Currency.RUB: bet2_2.income,
                    Currency.GBP: bet1_4.income + bet2_4.income,
                    Currency.USD: bet1_3.income + bet2_3.income,
                }
            )
        }
        self.assertEqual(response.data, expected_data)

        url = "%s?player=%s" % (reverse('public:bet-sum'), player2.uuid)
        response = self.client.get(url, format='json', HTTP_AUTHORIZATION='JWT %s' % self.partner_jwt_token)

        expected_data = {
            'stake_sum': {
                Currency.EUR: round(bet2_1.stake, 2),
                Currency.RUB: round(bet2_2.stake, 2),
                Currency.GBP: round(bet2_4.stake, 2),
                Currency.USD: round(bet2_3.stake, 2),
            },
            'outcome_sum': {
                Currency.EUR: round(bet2_1.outcome, 2),
                Currency.RUB: round(bet2_2.outcome, 2),
                Currency.GBP: round(bet2_4.outcome, 2),
                Currency.USD: round(bet2_3.outcome, 2)
            },
            'income_sum': {
                Currency.EUR: round(bet2_1.income, 2),
                Currency.RUB: round(bet2_2.income, 2),
                Currency.GBP: round(bet2_4.income, 2),
                Currency.USD: round(bet2_3.income, 2),
            },
            'stake_sum_eur': aggregate_to_one_currency(
                {
                    Currency.EUR: bet2_1.stake,
                    Currency.RUB: bet2_2.stake,
                    Currency.GBP: bet2_4.stake,
                    Currency.USD: bet2_3.stake,
                }
            ),
            'outcome_sum_eur': aggregate_to_one_currency(
                {
                    Currency.EUR: bet2_1.outcome,
                    Currency.RUB: bet2_2.outcome,
                    Currency.GBP: bet2_4.outcome,
                    Currency.USD: bet2_3.outcome,
                }
            ),
            'income_sum_eur': aggregate_to_one_currency(
                {
                    Currency.EUR: bet2_1.income,
                    Currency.RUB: bet2_2.income,
                    Currency.GBP: bet2_4.income,
                    Currency.USD: bet2_3.income,
                }
            )
        }
        self.assertEqual(response.data, expected_data)
