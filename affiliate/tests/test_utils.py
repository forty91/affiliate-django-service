from datetime import datetime

from django.test import TestCase

from base.utils.datetime import monthrange, utc_datetime, dayrange


class MonthrangeTestCase(TestCase):
    def test_ok(self):
        start_date = datetime(2018, 10, 4).date()
        end_date = datetime(2019, 4, 5).date()
        result = monthrange(start_date, end_date)

        self.assertEqual(
            list(result),
            [utc_datetime(2018, 10, 1, 0, 0, 0), utc_datetime(2018, 11, 1, 0, 0, 0),
             utc_datetime(2018, 12, 1, 0, 0, 0), utc_datetime(2019, 1, 1, 0, 0, 0),
             utc_datetime(2019, 2, 1, 0, 0, 0), utc_datetime(2019, 3, 1, 0, 0, 0),
             utc_datetime(2019, 4, 1, 0, 0, 0)]
        )

    def test_border_conditions(self):
        start_date = datetime(2019, 2, 28).date()
        end_date = datetime(2019, 3, 1).date()
        result = monthrange(start_date, end_date)

        self.assertEqual(
            list(result),
            [utc_datetime(2019, 2, 1, 0, 0, 0), utc_datetime(2019, 3, 1, 0, 0, 0)]
        )


class DayrangeTestCase(TestCase):
    def test_ok(self):
        start_date = datetime(2018, 10, 4).date()
        end_date = datetime(2018, 10, 14).date()
        result = dayrange(start_date, end_date)

        self.assertEqual(
            list(result),
            [utc_datetime(2018, 10, 4, 0, 0, 0), utc_datetime(2018, 10, 5, 0, 0, 0),
             utc_datetime(2018, 10, 6, 0, 0, 0), utc_datetime(2018, 10, 7, 0, 0, 0),
             utc_datetime(2018, 10, 8, 0, 0, 0), utc_datetime(2018, 10, 9, 0, 0, 0),
             utc_datetime(2018, 10, 10, 0, 0, 0), utc_datetime(2018, 10, 11, 0, 0, 0),
             utc_datetime(2018, 10, 12, 0, 0, 0), utc_datetime(2018, 10, 13, 0, 0, 0),
             utc_datetime(2018, 10, 14, 0, 0, 0)]
        )

    def test_border_conditions(self):
        start_date = datetime(2018, 12, 28).date()
        end_date = datetime(2019, 1, 2).date()
        result = dayrange(start_date, end_date)

        self.assertEqual(
            list(result),
            [utc_datetime(2018, 12, 28, 0, 0, 0), utc_datetime(2018, 12, 29, 0, 0, 0),
             utc_datetime(2018, 12, 30, 0, 0, 0), utc_datetime(2018, 12, 31, 0, 0, 0),
             utc_datetime(2019, 1, 1, 0, 0, 0), utc_datetime(2019, 1, 2, 0, 0, 0)]
        )
