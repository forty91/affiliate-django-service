from django.conf.urls import include, url
from django.conf import settings
from rest_framework.permissions import AllowAny
from rest_framework import routers
from drf_yasg.views import get_schema_view
from drf_yasg import openapi


schema_view = get_schema_view(
   openapi.Info(
      title="Affiliate API",
      default_version='v1',
      description="Affiliate API",
   ),
   # validators=['flex', 'ssv'],
   validators=[],
   public=True,
   permission_classes=(AllowAny,),
)


urlpatterns = [
    # url(r'^control_panel/', admin.site.urls),
    url(r'^api/', include(('api_public.urls', 'api'), namespace='public')),
    url(r'^private/api/', include(('api_private.urls', 'api'), namespace='private')),
    url(r'^docs/swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    url(r'^docs/redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]


if settings.DEBUG:
    from django.conf.urls.static import static

    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
