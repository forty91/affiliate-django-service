from .base import *


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': env.str('POSTGRES_DB', None),
        'USER': env.str('POSTGRES_USER', None),
        'PASSWORD': env.str('POSTGRES_PASSWORD', None),
        'HOST': env.str('POSTGRES_HOST', 'postgres'),
        'PORT': env.str('POSTGRES_PORT', '5432'),
    }
}


MONGO = {
    'DB': env.str('MONGO_DB', None),
    'HOST': env.str('MONGO_HOST', None),
    'PORT': env.str('MONGO_PORT', '27017'),
    'USER': env.str('MONGO_USER', None),
    'PASSWORD': env.str('MONGO_PASSWORD', None),
    'REPLICASET': env.str('MONGO_REPLICASET', None)
}
MONGO_CONNECTION_TEMPLATE = "mongodb://{user}:{password}@{host}:{port}/?authSource={db}&replicaSet={replicaset}"


CLICKHOUSE = {
    'DB': env.str('CLICKHOUSE_DB', None),
    'HOST': env.str('CLICKHOUSE_HOST', None),
    'PORT': env.str('CLICKHOUSE_PORT', '9000'),
    'USER': env.str('CLICKHOUSE_USER', None),
    'PASSWORD': env.str('CLICKHOUSE_PASSWORD', None)
}


REDIS = {
    'DB': env.int('REDIS_DB', 0),
    'HOST': env.str('REDIS_HOST', '127.0.0.1'),
    'PORT': env.int('REDIS_PORT', 6379),
    'PASSWORD': env.str('REDIS_PASSWORD', 'root')
}
