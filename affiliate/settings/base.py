import datetime
import logging
import os

from environs import Env
from raven.transport import RequestsHTTPTransport

from base.utils.http import ThreadPoolRequests

env = Env()

THREAD_POOL_REQUESTS = ThreadPoolRequests()


# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env.str('SECRET_KEY', None)

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = ['*']


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'raven.contrib.django.raven_compat',
    'drf_yasg',
    'api_public',
    'api_private',
    'users',
    'players',
    'promos',
    'bets',
    'payments',
    'permissions',
    'constance.backends.database',
    'finances',
    'projects',
    'reports',
    'traffic',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    # 'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'request_logging.middleware.LoggingMiddleware',
]

ROOT_URLCONF = 'affiliate.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',

            ],
        },
    },
]

WSGI_APPLICATION = 'affiliate.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'affiliate',
        'USER': 'admin',
        'PASSWORD': 'admin',
        'HOST': '127.0.0.1',
        'PORT': '5432',
        'ATOMIC_REQUESTS': True,
        'OPTIONS': {
            'sslmode': 'disable',
        }
    }
}


MONGO = {
    'DB': 'affiliate',
    'HOST': '127.0.0.1',
    'PORT': '27017',
    'USER': 'root',
    'PASSWORD': 'root',
    'REPLICASET': 'rs0'
}
MONGO_CONNECTION_TEMPLATE = "mongodb://{user}:{password}@{host}:{port}/?authSource={db}&replicaSet={replicaset}"


CLICKHOUSE = {
    'DB': 'default',
    'HOST': '127.0.0.1',
    'PORT': '9000',
    'USER': 'default',
    'PASSWORD': '',
}


REDIS = {
    'DB': 0,
    'HOST': '127.0.0.1',
    'PORT': 6379,
    'PASSWORD': 'root'
}


# Password validation
# https://docs.djangoproject.com/en/2.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
    ),
    'DEFAULT_FILTER_BACKENDS': (
        'django_filters.rest_framework.DjangoFilterBackend',
    ),
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    'DEFAULT_VERSIONING_CLASS': 'rest_framework.versioning.NamespaceVersioning',
}


SWAGGER_SETTINGS = {
    'SECURITY_DEFINITIONS': {
        'JWT': {
            'type': 'apiKey',
            'in': 'header',
            'name': 'Authorization'
        }
    },
}


JWT_AUTH = {
    # 'JWT_GET_USER_SECRET_KEY': 'bazalt.rest.auth.bazalt_jwt_get_user_secret_key',
    'JWT_VERIFY_EXPIRATION': True,
    'JWT_EXPIRATION_DELTA': datetime.timedelta(days=1),
    'JWT_PAYLOAD_HANDLER': 'base.utils.auth.affiliate_jwt_payload_handler',
    # 'JWT_DECODE_HANDLER': 'bazalt.rest.auth.bazalt_jwt_decode_handler',
    # 'JWT_RESPONSE_PAYLOAD_HANDLER': 'base.utils.auth.affiliate_jwt_response_payload_handler'
}


AUTHENTICATION_BACKENDS = [
    'django.contrib.auth.backends.ModelBackend',
]


AUTH_USER_MODEL = 'users.User'


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'root': {
        'level': 'DEBUG',
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s '
                      '%(process)d %(thread)d %(message)s'
        },
    },
    'handlers': {
        'sentry': {
            'level': 'ERROR',  # To capture more than ERROR, change to WARNING, INFO, etc.
            'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
            'tags': {'custom-tag': 'x'},
        },
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        }
    },
    'loggers': {
        'django': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },
        'django.db.backends': {
            'level': 'ERROR',
            'handlers': ['console', 'sentry'],
            'propagate': False,
        },
        'raven': {
            'level': 'ERROR',
            'handlers': ['sentry'],
            'propagate': False,
        },
        'sentry.errors': {
            'level': 'ERROR',
            'handlers': ['sentry'],
            'propagate': False,
        },
    },
}


REQUEST_LOGGING_HTTP_4XX_LOG_LEVEL = logging.INFO


RAVEN_CONFIG = {
    'dsn': env.str('SENTRY_DSN', None),
    'transport': RequestsHTTPTransport  # боремся с SNI
}


# Internationalization

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


CONSTANCE_BACKEND = 'constance.backends.database.DatabaseBackend'
CONSTANCE_CONFIG = {
    'CREATE_PLAYER_KNOCK_URL': ('', "URL of server which should be knocked with GET request on player creation."),
    'CURRENCY_RATES': ('', 'Actual currency rates on date.')
}


# Static files (CSS, JavaScript, Images)
FILE_STORAGE_PATH = os.path.join(BASE_DIR, '..', 'file_storage')

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(FILE_STORAGE_PATH, 'static')

MEDIA_URL = '/fs/'
MEDIA_ROOT = os.path.join(FILE_STORAGE_PATH, 'media')


AFFILIATE_STATIC_SERVICE_URL = env.str('AFFILIATE_STATIC_SERVICE_URL', None)


# Default entities
DEFAULT_STAFF_NAME = 'admin@cyber.bet'
DEFAULT_STAFF_PASSWORD = 'admin'

DEFAULT_PARTNER_NAME = 'cyberbet@cyber.bet'
DEFAULT_PARTNER_PASSWORD = 'cyberbet'
DEFAULT_PARTNER_HOLD = 7

DEFAULT_PROMO_NAME = 'cyberbet'

DEFAULT_ROLE = 'root'

DEFAULT_LANDING_NAME = 'default'
DEFAULT_LANDING_LINK = env.str('CYBER_BET_URL', '')


DEFAULT_PROJECT_NAME = 'cyber.bet'


DEFAULT_PROGRAM_TYPE_NAME = 'revshare'
DEFAULT_PROGRAM_RATE = 40


# thirdparty services
S3_UPLOAD_SERVICE_URL = env.str('S3_UPLOAD_SERVICE_URL', None)
CURRENCY_SERVICE_URL = env.str('CURRENCY_SERVICE_URL', None)
FRONTEND_SERVICE_URL = env.str('FRONTEND_SERVICE_URL', None)


# AWS configs
AWS_REGION = env.str('AWS_REGION', None)
AWS_ACCESS_KEY_ID = env.str('AWS_ACCESS_KEY_ID', None)
AWS_SECRET_ACCESS_KEY = env.str('AWS_SECRET_ACCESS_KEY', None)
