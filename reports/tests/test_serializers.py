from datetime import datetime, timedelta

from django.test import TestCase
from rest_framework.exceptions import ValidationError

from base.consts import DATE_FORMAT
from base.utils.datetime import datetime_now
from reports.rest.serializers import ReportParamsSerilizer


class ReportParamsSerilizerTestCase(TestCase):
    def test_date__gte_later_date__lte(self):
        long_ago = datetime(2018, 8, 8, 8, 8, 8)
        now = datetime(2018, 9, 9, 9, 9, 9)

        data = {
            'date__gte': now.strftime(DATE_FORMAT),
            'date__lte': long_ago.strftime(DATE_FORMAT),
        }

        serializer = ReportParamsSerilizer(data=data)

        with self.assertRaisesRegex(
                ValidationError,
                "Parameter date__gte should be lower or equal date__lte."
        ):
            serializer.is_valid(raise_exception=True)

    def test_date__lte_later_today(self):
        today = datetime_now().date()
        tomorow = today + timedelta(days=1)

        data = {
            'date__gte': today.strftime(DATE_FORMAT),
            'date__lte': tomorow.strftime(DATE_FORMAT),
        }

        serializer = ReportParamsSerilizer(data=data)

        with self.assertRaisesRegex(
                ValidationError,
                "Parameter date__lte should be lower or equal today."
        ):
            serializer.is_valid(raise_exception=True)
