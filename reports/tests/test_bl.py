from datetime import datetime, date

from django.test import TestCase

from base.utils.redis import RedisClient
from promos.consts import TrafficActionType
from promos.models import Promo
from traffic.bl import GetTraffic
from users.tests.factories import PartnerFactory


class GetTrafficTestCase(TestCase):
    def test_empty_promos(self):
        """
        Проверка что если у партнера нет promos то вернутся нули.
        """
        redis_client = RedisClient()
        redis_client.flushall()

        promos = Promo.objects.none()
        partner = PartnerFactory()
        days = [
            datetime.combine(day, datetime.min.time())
            for day in (date(2018, 8, 8), date(2018, 8, 9))
        ]
        actions = (TrafficActionType.DOWNLOAD, TrafficActionType.CLICK)
        traffic = GetTraffic(partner.user, promos).get_days_traffic(days, actions)

        self.assertEqual(traffic, {})
