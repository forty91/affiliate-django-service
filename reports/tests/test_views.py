import datetime

from django.test import tag
from django.urls import reverse
from freezegun import freeze_time
from rest_framework.status import HTTP_200_OK

from base.consts import Currency, DATE_FORMAT
from base.tests import AffiliateAPITestCase
from base.utils.redis import RedisClient
from base.utils.tests import ClickhouseInfrastructure
from bets.tests.factories import BetFactory
from bets.consts import Status as BetStatus, State as BetState
from payments.tests.factories import PaymentFactory
from payments.consts import Status as PaymentStatus, Direction as PaymentDirection
from players.tests.factories import PlayerFactory
from projects.tests.factories import ProgramFactory
from promos.tests.factories import PromoFactory
from reports.utils import datetime_to_date_repr, datetime_to_month_repr
from users.tests.factories import PartnerFactory


class ReportsTestCase(AffiliateAPITestCase):
    @tag('local')
    def test_list_days(self):
        """
        Проверка что валидности сагрегированных по дням значений.
        В нулевой день происходит действия для стороннего partner0:
            * Регистрация 1 игрока
            * 1 первый депозит
            * 1 ставка

        В первый день происходит:
            * Регистрация 2-х игроков.
            * Каждый игрок создает 3 платежа из которых 2 валидны и 1 является первым депозитом.
            * 1-й игрок создает 3 ставки из которых 2 валидны.
            * 2-й игрок создает 3 ставки из которых 1 валидна.

        Во второй день происходит:
            * регистрация 1 нового игрока
            * новый игрок создает 1 первый депозит
            * старый игрок создает 1 депозит
            * новый игрок создает проигранную ставку
            * старый игрок создает выйгранную ставку

        Проверяем корректность:
            * Админ запросил репорты за все дни и получил информацию по всем партнерам и промо
            * Партнер запросил репорты за все дни и получил только информацию о своих promo
        """
        redis_client = RedisClient()
        redis_client.flushall()

        url = reverse('public:report-list-days')
        long_long_long_ago = datetime.datetime(2018, 8, 7, 8, 8, 8)
        long_long_ago = datetime.datetime(2018, 8, 8, 8, 8, 8)
        long_ago = datetime.datetime(2018, 8, 9, 8, 8, 8)
        now = datetime.datetime(2018, 8, 10, 8, 8, 8)

        partner0 = PartnerFactory()
        promo0 = PromoFactory(partner=partner0)
        program_rate0 = promo0.program.rate
        program_rate = 60
        program = ProgramFactory(rate=program_rate)
        promo1 = PromoFactory(program=program, partner=self.partner)
        promo2 = PromoFactory(program=program, partner=self.partner)

        with ClickhouseInfrastructure(
                partners=[partner0],
                timestamp_range=[long_long_long_ago, long_long_ago],
        ) as clickhouse_client0, ClickhouseInfrastructure(
            partners=[self.partner],
            timestamp_range=[long_long_ago, long_ago],
        ) as clickhouse_client1, ClickhouseInfrastructure(
            partners=[self.partner],
            timestamp_range=[long_ago, now],
        ) as clickhouse_client2:
            with freeze_time(long_long_long_ago):
                player0 = PlayerFactory(promo=promo0)
                payment0_1 = PaymentFactory(player=player0, status=PaymentStatus.SUCCESS,
                                            currency=Currency.USD, direction=PaymentDirection.INCOMING, is_first=True)
                bet0_1 = BetFactory(player=player0, status=BetStatus.ACCEPTED, state=BetState.WIN,
                                    currency=Currency.USD)

            with freeze_time(long_long_ago):
                player1 = PlayerFactory(promo=promo1)
                player2 = PlayerFactory(promo=promo1)

                payment1_1 = PaymentFactory(player=player1, status=PaymentStatus.SUCCESS,
                                            currency=Currency.USD, direction=PaymentDirection.INCOMING, is_first=True)
                payment1_2 = PaymentFactory(player=player1, status=PaymentStatus.SUCCESS,
                                            currency=Currency.USD, direction=PaymentDirection.INCOMING)
                payment1_3 = PaymentFactory(player=player1, status=PaymentStatus.SUCCESS,
                                            currency=Currency.USD, direction=PaymentDirection.OUTGOING)
                payment2_1 = PaymentFactory(player=player2, status=PaymentStatus.SUCCESS,
                                            currency=Currency.GBP, direction=PaymentDirection.INCOMING, is_first=True)
                payment2_2 = PaymentFactory(player=player2, status=PaymentStatus.SUCCESS,
                                            currency=Currency.GBP, direction=PaymentDirection.INCOMING)
                payment2_3 = PaymentFactory(player=player2, status=PaymentStatus.CANCELED,
                                            currency=Currency.GBP, direction=PaymentDirection.INCOMING)

                bet1_1 = BetFactory(player=player1, status=BetStatus.ACCEPTED, state=BetState.WIN,
                                    currency=Currency.USD)
                bet1_2 = BetFactory(player=player1, status=BetStatus.ACCEPTED, state=BetState.LOSE,
                                    currency=Currency.USD)
                bet1_3 = BetFactory(player=player1, status=BetStatus.CANCELED, state=BetState.WIN,
                                    currency=Currency.USD)
                bet2_1 = BetFactory(player=player2, status=BetStatus.ACCEPTED, state=BetState.WIN,
                                    currency=Currency.GBP)
                bet2_2 = BetFactory(player=player2, status=BetStatus.ACCEPTED, state=BetState.WIN,
                                    currency=Currency.GBP, is_freebet=True)
                bet2_3 = BetFactory(player=player2, status=BetStatus.ACCEPTED, state=BetState.UNKNOWN,
                                    currency=Currency.GBP)

            with freeze_time(long_ago):
                player3 = PlayerFactory(promo=promo2)
                payment3_1 = PaymentFactory(player=player3, status=PaymentStatus.SUCCESS,
                                            currency=Currency.RUB, direction=PaymentDirection.INCOMING, is_first=True)
                payment2_4 = PaymentFactory(player=player2, status=PaymentStatus.SUCCESS,
                                            currency=Currency.RUB, direction=PaymentDirection.INCOMING)

                bet3_1 = BetFactory(player=player3, status=BetStatus.ACCEPTED, state=BetState.LOSE,
                                    currency=Currency.RUB)
                bet1_4 = BetFactory(player=player1, status=BetStatus.ACCEPTED, state=BetState.WIN,
                                    currency=Currency.RUB)

            response_staff_4_days = self.client.get(
                url,
                data={
                    'date__gte': long_long_long_ago.strftime(DATE_FORMAT),
                    'date__lte': now.strftime(DATE_FORMAT),
                },
                HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token
            )
            response_staff_4_days_from_cache = self.client.get(
                url,
                data={
                    'date__gte': long_long_long_ago.strftime(DATE_FORMAT),
                    'date__lte': now.strftime(DATE_FORMAT),
                },
                HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token
            )
            response_staff_3_days_ordered = self.client.get(
                url,
                data={
                    'date__gte': long_long_ago.strftime(DATE_FORMAT),
                    'date__lte': now.strftime(DATE_FORMAT),
                    'ordering': '-signups',
                },
                HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token
            )
            response_staff_3_days_ordered_from_cache = self.client.get(
                url,
                data={
                    'date__gte': long_long_ago.strftime(DATE_FORMAT),
                    'date__lte': now.strftime(DATE_FORMAT),
                    'ordering': '-signups',
                },
                HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token
            )
            response_partner_4_days = self.client.get(
                url,
                data={
                    'date__gte': long_long_long_ago.strftime(DATE_FORMAT),
                    'date__lte': now.strftime(DATE_FORMAT),
                },
                HTTP_AUTHORIZATION='JWT %s' % self.partner_jwt_token
            )
            response_partner_4_days_from_cache = self.client.get(
                url,
                data={
                    'date__gte': long_long_long_ago.strftime(DATE_FORMAT),
                    'date__lte': now.strftime(DATE_FORMAT),
                },
                HTTP_AUTHORIZATION='JWT %s' % self.partner_jwt_token
            )
            response_partner_4_days_filtered = self.client.get(
                url,
                data={
                    'date__gte': long_long_long_ago.strftime(DATE_FORMAT),
                    'date__lte': now.strftime(DATE_FORMAT),
                    'promo': promo1.uuid
                },
                HTTP_AUTHORIZATION='JWT %s' % self.partner_jwt_token
            )
            response_partner_4_days_filtered_from_cache = self.client.get(
                url,
                data={
                    'date__gte': long_long_long_ago.strftime(DATE_FORMAT),
                    'date__lte': now.strftime(DATE_FORMAT),
                    'promo': promo1.uuid
                },
                HTTP_AUTHORIZATION='JWT %s' % self.partner_jwt_token
            )

        expected_data = {
            'count': 4,
            'totals': {
                'signups': 4,
                'first_deposits_count': 4,
                'first_deposits_amount': '%.2f' % round(payment0_1.amount_eur +
                                                        payment1_1.amount_eur +
                                                        payment2_1.amount_eur +
                                                        payment3_1.amount_eur, 2),
                'deposits_count': 7,
                'deposits_amount': '%.2f' % round(payment0_1.amount_eur +
                                                  payment1_1.amount_eur +
                                                  payment1_2.amount_eur +
                                                  payment2_1.amount_eur +
                                                  payment2_2.amount_eur +
                                                  payment3_1.amount_eur +
                                                  payment2_4.amount_eur, 2),
                'net_gaming': '%.2f' % round(
                    (bet0_1.outcome_eur - bet0_1.income_eur) +
                    (bet1_1.outcome_eur - bet1_1.income_eur) +
                    (bet1_2.outcome_eur - bet1_2.income_eur) +
                    (bet2_1.outcome_eur - bet2_1.income_eur) +
                    (bet3_1.outcome_eur - bet3_1.income_eur) +
                    (bet1_4.outcome_eur - bet1_4.income_eur), 2
                ),
                'profit': '%.2f' % round(
                    (bet0_1.outcome_eur - bet0_1.income_eur) * program_rate0 / 100 +
                    (bet1_1.outcome_eur - bet1_1.income_eur) * program_rate / 100 +
                    (bet1_2.outcome_eur - bet1_2.income_eur) * program_rate / 100 +
                    (bet2_1.outcome_eur - bet2_1.income_eur) * program_rate / 100 +
                    (bet3_1.outcome_eur - bet3_1.income_eur) * program_rate / 100 +
                    (bet1_4.outcome_eur - bet1_4.income_eur) * program_rate / 100, 2
                ),  # TODO в зависимости от программы, по разному считается profit, формула будет браться из ProgramType
                'clicks': 25,
                'unique_clicks': 20,
            },
            'results': [
                {
                    'date': datetime_to_date_repr(now),
                    'signups': 0,
                    'first_deposits_count': 0,
                    'first_deposits_amount': '0.00',
                    'deposits_count': 0,
                    'deposits_amount': '0.00',
                    'net_gaming': '0.00',
                    'profit': '0.00',
                    'clicks': 6,
                    'unique_clicks': 6,
                },
                {
                    'date': datetime_to_date_repr(long_ago),
                    'signups': 1,
                    'first_deposits_count': 1,
                    'first_deposits_amount': '%.2f' % round(payment3_1.amount_eur, 2),
                    'deposits_count': 2,
                    'deposits_amount': '%.2f' % round(payment3_1.amount_eur +
                                                      payment2_4.amount_eur, 2),
                    'net_gaming': '%.2f' % round((bet3_1.outcome_eur - bet3_1.income_eur) +
                                                 (bet1_4.outcome_eur - bet1_4.income_eur), 2),
                    'profit': '%.2f' % round(
                        (bet3_1.outcome_eur - bet3_1.income_eur) * program_rate / 100 +
                        (bet1_4.outcome_eur - bet1_4.income_eur) * program_rate / 100, 2
                    ),
                    'clicks': 10,
                    'unique_clicks': 8,
                },
                {
                    'date': datetime_to_date_repr(long_long_ago),
                    'signups': 2,
                    'first_deposits_count': 2,
                    'first_deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                            payment2_1.amount_eur, 2),
                    'deposits_count': 4,
                    'deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                      payment1_2.amount_eur +
                                                      payment2_1.amount_eur +
                                                      payment2_2.amount_eur, 2),
                    'net_gaming': '%.2f' % round((bet1_1.outcome_eur - bet1_1.income_eur) +
                                                 (bet1_2.outcome_eur - bet1_2.income_eur) +
                                                 (bet2_1.outcome_eur - bet2_1.income_eur), 2),
                    'profit': '%.2f' % round(
                        (bet1_1.outcome_eur - bet1_1.income_eur) * program_rate / 100 +
                        (bet1_2.outcome_eur - bet1_2.income_eur) * program_rate / 100 +
                        (bet2_1.outcome_eur - bet2_1.income_eur) * program_rate / 100, 2),
                    'clicks': 7,
                    'unique_clicks': 5,
                },
                {
                    'date': datetime_to_date_repr(long_long_long_ago),
                    'signups': 1,
                    'first_deposits_count': 1,
                    'first_deposits_amount': '%.2f' % round(payment0_1.amount_eur, 2),
                    'deposits_count': 1,
                    'deposits_amount': '%.2f' % round(payment0_1.amount_eur, 2),
                    'net_gaming': '%.2f' % round((bet0_1.outcome_eur - bet0_1.income_eur), 2),
                    'profit': '%.2f' % round((bet0_1.outcome_eur - bet0_1.income_eur) * program_rate0 / 100, 2),
                    'clicks': 2,
                    'unique_clicks': 1,
                },
            ],
        }

        self.assertEqual(response_staff_4_days.status_code, HTTP_200_OK)
        self.assertEqual(response_staff_4_days.data, expected_data)
        self.assertEqual(response_staff_4_days_from_cache.data, expected_data)

        expected_data = {
            'count': 3,
            'totals': {
                'signups': 3,
                'first_deposits_count': 3,
                'first_deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                        payment2_1.amount_eur +
                                                        payment3_1.amount_eur, 2),
                'deposits_count': 6,
                'deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                  payment1_2.amount_eur +
                                                  payment2_1.amount_eur +
                                                  payment2_2.amount_eur +
                                                  payment3_1.amount_eur +
                                                  payment2_4.amount_eur, 2),
                'net_gaming': '%.2f' % round(
                    (bet1_1.outcome_eur - bet1_1.income_eur) +
                    (bet1_2.outcome_eur - bet1_2.income_eur) +
                    (bet2_1.outcome_eur - bet2_1.income_eur) +
                    (bet3_1.outcome_eur - bet3_1.income_eur) +
                    (bet1_4.outcome_eur - bet1_4.income_eur), 2
                ),
                'profit': '%.2f' % round(
                    (bet1_1.outcome_eur - bet1_1.income_eur) * program_rate / 100 +
                    (bet1_2.outcome_eur - bet1_2.income_eur) * program_rate / 100 +
                    (bet2_1.outcome_eur - bet2_1.income_eur) * program_rate / 100 +
                    (bet3_1.outcome_eur - bet3_1.income_eur) * program_rate / 100 +
                    (bet1_4.outcome_eur - bet1_4.income_eur) * program_rate / 100, 2
                ),  # TODO в зависимости от программы, по разному считается profit, формула будет браться из ProgramType
                'clicks': 23,
                'unique_clicks': 19,
            },
            'results': [
                {
                    'date': datetime_to_date_repr(long_long_ago),
                    'signups': 2,
                    'first_deposits_count': 2,
                    'first_deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                            payment2_1.amount_eur, 2),
                    'deposits_count': 4,
                    'deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                      payment1_2.amount_eur +
                                                      payment2_1.amount_eur +
                                                      payment2_2.amount_eur, 2),
                    'net_gaming': '%.2f' % round((bet1_1.outcome_eur - bet1_1.income_eur) +
                                                 (bet1_2.outcome_eur - bet1_2.income_eur) +
                                                 (bet2_1.outcome_eur - bet2_1.income_eur), 2),
                    'profit': '%.2f' % round(
                        (bet1_1.outcome_eur - bet1_1.income_eur) * program_rate / 100 +
                        (bet1_2.outcome_eur - bet1_2.income_eur) * program_rate / 100 +
                        (bet2_1.outcome_eur - bet2_1.income_eur) * program_rate / 100, 2
                    ),
                    'clicks': 7,
                    'unique_clicks': 5,
                },
                {
                    'date': datetime_to_date_repr(long_ago),
                    'signups': 1,
                    'first_deposits_count': 1,
                    'first_deposits_amount': '%.2f' % round(payment3_1.amount_eur, 2),
                    'deposits_count': 2,
                    'deposits_amount': '%.2f' % round(payment3_1.amount_eur +
                                                      payment2_4.amount_eur, 2),
                    'net_gaming': '%.2f' % round((bet3_1.outcome_eur - bet3_1.income_eur) +
                                                 (bet1_4.outcome_eur - bet1_4.income_eur), 2),
                    'profit': '%.2f' % round(
                        (bet3_1.outcome_eur - bet3_1.income_eur) * program_rate / 100 +
                        (bet1_4.outcome_eur - bet1_4.income_eur) * program_rate / 100, 2
                    ),
                    'clicks': 10,
                    'unique_clicks': 8,
                },
                {
                    'date': datetime_to_date_repr(now),
                    'signups': 0,
                    'first_deposits_count': 0,
                    'first_deposits_amount': '0.00',
                    'deposits_count': 0,
                    'deposits_amount': '0.00',
                    'net_gaming': '0.00',
                    'profit': '0.00',
                    'clicks': 6,
                    'unique_clicks': 6,
                },
            ],
        }
        self.assertEqual(response_staff_3_days_ordered.status_code, HTTP_200_OK)
        self.assertEqual(response_staff_3_days_ordered.data, expected_data)
        self.assertEqual(response_staff_3_days_ordered_from_cache.data, expected_data)

        expected_data = {
            'count': 4,
            'totals': {
                'signups': 3,
                'first_deposits_count': 3,
                'first_deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                        payment2_1.amount_eur +
                                                        payment3_1.amount_eur, 2),
                'deposits_count': 6,
                'deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                  payment1_2.amount_eur +
                                                  payment2_1.amount_eur +
                                                  payment2_2.amount_eur +
                                                  payment3_1.amount_eur +
                                                  payment2_4.amount_eur, 2),
                'net_gaming': '%.2f' % round(
                    (bet1_1.outcome_eur - bet1_1.income_eur) +
                    (bet1_2.outcome_eur - bet1_2.income_eur) +
                    (bet2_1.outcome_eur - bet2_1.income_eur) +
                    (bet3_1.outcome_eur - bet3_1.income_eur) +
                    (bet1_4.outcome_eur - bet1_4.income_eur), 2
                ),
                'profit': '%.2f' % round(
                    (bet1_1.outcome_eur - bet1_1.income_eur) * program_rate / 100 +
                    (bet1_2.outcome_eur - bet1_2.income_eur) * program_rate / 100 +
                    (bet2_1.outcome_eur - bet2_1.income_eur) * program_rate / 100 +
                    (bet3_1.outcome_eur - bet3_1.income_eur) * program_rate / 100 +
                    (bet1_4.outcome_eur - bet1_4.income_eur) * program_rate / 100, 2
                ),  # TODO в зависимости от программы, по разному считается profit, формула будет браться из ProgramType
                'clicks': 20,
                'unique_clicks': 16,
            },
            'results': [
                {
                    'date': datetime_to_date_repr(now),
                    'signups': 0,
                    'first_deposits_count': 0,
                    'first_deposits_amount': '0.00',
                    'deposits_count': 0,
                    'deposits_amount': '0.00',
                    'net_gaming': '0.00',
                    'profit': '0.00',
                    'clicks': 6,
                    'unique_clicks': 6,
                },
                {
                    'date': datetime_to_date_repr(long_ago),
                    'signups': 1,
                    'first_deposits_count': 1,
                    'first_deposits_amount': '%.2f' % round(payment3_1.amount_eur, 2),
                    'deposits_count': 2,
                    'deposits_amount': '%.2f' % round(payment3_1.amount_eur +
                                                      payment2_4.amount_eur, 2),
                    'net_gaming': '%.2f' % round((bet3_1.outcome_eur - bet3_1.income_eur) +
                                                 (bet1_4.outcome_eur - bet1_4.income_eur), 2),
                    'profit': '%.2f' % round(
                        (bet3_1.outcome_eur - bet3_1.income_eur) * program_rate / 100 +
                        (bet1_4.outcome_eur - bet1_4.income_eur) * program_rate / 100, 2
                    ),
                    'clicks': 10,
                    'unique_clicks': 8,
                },
                {
                    'date': datetime_to_date_repr(long_long_ago),
                    'signups': 2,
                    'first_deposits_count': 2,
                    'first_deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                            payment2_1.amount_eur, 2),
                    'deposits_count': 4,
                    'deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                      payment1_2.amount_eur +
                                                      payment2_1.amount_eur +
                                                      payment2_2.amount_eur, 2),
                    'net_gaming': '%.2f' % round(
                        (bet1_1.outcome_eur - bet1_1.income_eur) +
                        (bet1_2.outcome_eur - bet1_2.income_eur) +
                        (bet2_1.outcome_eur - bet2_1.income_eur), 2
                    ),
                    'profit': '%.2f' % round(
                        (bet1_1.outcome_eur - bet1_1.income_eur) * program_rate / 100 +
                        (bet1_2.outcome_eur - bet1_2.income_eur) * program_rate / 100 +
                        (bet2_1.outcome_eur - bet2_1.income_eur) * program_rate / 100, 2
                    ),  # TODO в зависимости от программы, по разному считается profit, формула будет браться из ProgramType
                    'clicks': 4,
                    'unique_clicks': 2,
                },
                {
                    'date': datetime_to_date_repr(long_long_long_ago),
                    'signups': 0,
                    'first_deposits_count': 0,
                    'first_deposits_amount': '0.00',
                    'deposits_count': 0,
                    'deposits_amount': '0.00',
                    'net_gaming': '0.00',
                    'profit': '0.00',
                    'clicks': 0,
                    'unique_clicks': 0,
                },
            ],
        }
        self.assertEqual(response_partner_4_days.status_code, HTTP_200_OK)
        self.assertEqual(response_partner_4_days.data, expected_data)
        self.assertEqual(response_partner_4_days_from_cache.data, expected_data)

        expected_data = {
            'count': 4,
            'totals': {
                'signups': 2,
                'first_deposits_count': 2,
                'first_deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                        payment2_1.amount_eur, 2),
                'deposits_count': 5,
                'deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                  payment1_2.amount_eur +
                                                  payment2_1.amount_eur +
                                                  payment2_2.amount_eur +
                                                  payment2_4.amount_eur, 2),
                'net_gaming': '%.2f' % round(
                    (bet1_1.outcome_eur - bet1_1.income_eur) +
                    (bet1_2.outcome_eur - bet1_2.income_eur) +
                    (bet2_1.outcome_eur - bet2_1.income_eur) +
                    (bet1_4.outcome_eur - bet1_4.income_eur), 2
                ),
                'profit': '%.2f' % round(
                    (bet1_1.outcome_eur - bet1_1.income_eur) * program_rate / 100 +
                    (bet1_2.outcome_eur - bet1_2.income_eur) * program_rate / 100 +
                    (bet2_1.outcome_eur - bet2_1.income_eur) * program_rate / 100 +
                    (bet1_4.outcome_eur - bet1_4.income_eur) * program_rate / 100, 2
                ),  # TODO в зависимости от программы, по разному считается profit, формула будет браться из ProgramType
                'clicks': 10,
                'unique_clicks': 8,
            },
            'results': [
                {
                    'date': datetime_to_date_repr(now),
                    'signups': 0,
                    'first_deposits_count': 0,
                    'first_deposits_amount': '0.00',
                    'deposits_count': 0,
                    'deposits_amount': '0.00',
                    'net_gaming': '0.00',
                    'profit': '0.00',
                    'clicks': 3,
                    'unique_clicks': 3,
                },
                {
                    'date': datetime_to_date_repr(long_ago),
                    'signups': 0,
                    'first_deposits_count': 0,
                    'first_deposits_amount': '0.00',
                    'deposits_count': 1,
                    'deposits_amount': '%.2f' % round(payment2_4.amount_eur, 2),
                    'net_gaming': '%.2f' % round((bet1_4.outcome_eur - bet1_4.income_eur), 2),
                    'profit': '%.2f' % round(
                        (bet1_4.outcome_eur - bet1_4.income_eur) * program_rate / 100, 2
                    ),
                    'clicks': 5,
                    'unique_clicks': 4,
                },
                {
                    'date': datetime_to_date_repr(long_long_ago),
                    'signups': 2,
                    'first_deposits_count': 2,
                    'first_deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                            payment2_1.amount_eur, 2),
                    'deposits_count': 4,
                    'deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                      payment1_2.amount_eur +
                                                      payment2_1.amount_eur +
                                                      payment2_2.amount_eur, 2),
                    'net_gaming': '%.2f' % round(
                        (bet1_1.outcome_eur - bet1_1.income_eur) +
                        (bet1_2.outcome_eur - bet1_2.income_eur) +
                        (bet2_1.outcome_eur - bet2_1.income_eur), 2
                    ),
                    'profit': '%.2f' % round(
                        (bet1_1.outcome_eur - bet1_1.income_eur) * program_rate / 100 +
                        (bet1_2.outcome_eur - bet1_2.income_eur) * program_rate / 100 +
                        (bet2_1.outcome_eur - bet2_1.income_eur) * program_rate / 100, 2
                    ),  # TODO в зависимости от программы, по разному считается profit, формула будет браться из ProgramType
                    'clicks': 2,
                    'unique_clicks': 1,
                },
                {
                    'date': datetime_to_date_repr(long_long_long_ago),
                    'signups': 0,
                    'first_deposits_count': 0,
                    'first_deposits_amount': '0.00',
                    'deposits_count': 0,
                    'deposits_amount': '0.00',
                    'net_gaming': '0.00',
                    'profit': '0.00',
                    'clicks': 0,
                    'unique_clicks': 0,
                },
            ],
        }
        self.assertEqual(response_partner_4_days_filtered.status_code, HTTP_200_OK)
        self.assertEqual(response_partner_4_days_filtered.data, expected_data)
        self.assertEqual(response_partner_4_days_filtered_from_cache.data, expected_data)

    @tag('local')
    def test_list_months(self):
        """
        Проверка что валидности сагрегированных по месяцам значений.
        В нулевой месяц происходит действия для стороннего partner0:
            * Регистрация 1 игрока
            * 1 первый депозит
            * 1 ставка

        В первый месяц происходит:
            * Регистрация 2-х игроков.
            * Каждый игрок создает 3 платежа из которых 2 валидны и 1 является первым депозитом.
            * 1-й игрок создает 3 ставки из которых 2 валидны.
            * 2-й игрок создает 3 ставки из которых 1 валидна.

        Во второй месяц происходит:
            * регистрация 1 нового игрока
            * новый игрок создает 1 первый депозит
            * старый игрок создает 1 депозит
            * новый игрок создает проигранную ставку
            * старый игрок создает выйгранную ставку

        Проверяем корректность:
            * Админ запросил репорты за все дни и получил информацию по всем партнерам и промо
            * Партнер запросил репорты за все дни и получил только информацию о своих promo
        """
        redis_client = RedisClient()
        redis_client.flushall()

        url = reverse('public:report-list-months')
        date1_1 = datetime.datetime(2018, 7, 7, 7, 7, 7)
        date2_1 = datetime.datetime(2018, 8, 8, 8, 8, 8)
        date2_2 = datetime.datetime(2018, 8, 18, 18, 18, 18)
        date3_1 = datetime.datetime(2018, 9, 9, 9, 9, 9)
        date3_2 = datetime.datetime(2018, 9, 19, 19, 19, 19)
        date4_1 = datetime.datetime(2018, 10, 10, 10, 10, 10)

        partner0 = PartnerFactory()
        promo0 = PromoFactory(partner=partner0)
        program_rate0 = promo0.program.rate
        program_rate = 60
        program = ProgramFactory(rate=program_rate)
        promo1 = PromoFactory(program=program, partner=self.partner)
        promo2 = PromoFactory(program=program, partner=self.partner)

        with ClickhouseInfrastructure(
                partners=[partner0],
                timestamp_range=[date1_1, date2_1],
        ) as clickhouse_client0, ClickhouseInfrastructure(
            partners=[self.partner],
            timestamp_range=[date2_1, date3_1],
        ) as clickhouse_client1, ClickhouseInfrastructure(
            partners=[self.partner],
            timestamp_range=[date3_1, date4_1],
        ) as clickhouse_client2:
            with freeze_time(date1_1):
                player0 = PlayerFactory(promo=promo0)
                payment0_1 = PaymentFactory(player=player0, status=PaymentStatus.SUCCESS,
                                            currency=Currency.USD, direction=PaymentDirection.INCOMING, is_first=True)
                bet0_1 = BetFactory(player=player0, status=BetStatus.ACCEPTED, state=BetState.WIN,
                                    currency=Currency.USD)

            with freeze_time(date2_1):
                player1 = PlayerFactory(promo=promo1)

                payment1_1 = PaymentFactory(player=player1, status=PaymentStatus.SUCCESS,
                                            currency=Currency.USD, direction=PaymentDirection.INCOMING, is_first=True)
                payment1_2 = PaymentFactory(player=player1, status=PaymentStatus.SUCCESS,
                                            currency=Currency.USD, direction=PaymentDirection.INCOMING)
                payment1_3 = PaymentFactory(player=player1, status=PaymentStatus.SUCCESS,
                                            currency=Currency.USD, direction=PaymentDirection.OUTGOING)

                bet1_1 = BetFactory(player=player1, status=BetStatus.ACCEPTED, state=BetState.WIN,
                                    currency=Currency.USD)
                bet1_2 = BetFactory(player=player1, status=BetStatus.ACCEPTED, state=BetState.LOSE,
                                    currency=Currency.USD)
                bet1_3 = BetFactory(player=player1, status=BetStatus.CANCELED, state=BetState.WIN,
                                    currency=Currency.USD)

            with freeze_time(date2_2):
                player2 = PlayerFactory(promo=promo1)

                payment2_1 = PaymentFactory(player=player2, status=PaymentStatus.SUCCESS,
                                            currency=Currency.GBP, direction=PaymentDirection.INCOMING, is_first=True)
                payment2_2 = PaymentFactory(player=player2, status=PaymentStatus.SUCCESS,
                                            currency=Currency.GBP, direction=PaymentDirection.INCOMING)
                payment2_3 = PaymentFactory(player=player2, status=PaymentStatus.CANCELED,
                                            currency=Currency.GBP, direction=PaymentDirection.INCOMING)

                bet2_1 = BetFactory(player=player2, status=BetStatus.ACCEPTED, state=BetState.WIN,
                                    currency=Currency.GBP)
                bet2_2 = BetFactory(player=player2, status=BetStatus.ACCEPTED, state=BetState.WIN,
                                    currency=Currency.GBP, is_freebet=True)
                bet2_3 = BetFactory(player=player2, status=BetStatus.ACCEPTED, state=BetState.UNKNOWN,
                                    currency=Currency.GBP)

            with freeze_time(date3_1):
                player3 = PlayerFactory(promo=promo2)
                payment3_1 = PaymentFactory(player=player3, status=PaymentStatus.SUCCESS,
                                            currency=Currency.RUB, direction=PaymentDirection.INCOMING, is_first=True)
                bet3_1 = BetFactory(player=player3, status=BetStatus.ACCEPTED, state=BetState.LOSE,
                                    currency=Currency.RUB)

            with freeze_time(date3_2):
                payment2_4 = PaymentFactory(player=player2, status=PaymentStatus.SUCCESS,
                                            currency=Currency.RUB, direction=PaymentDirection.INCOMING)
                bet1_4 = BetFactory(player=player1, status=BetStatus.ACCEPTED, state=BetState.WIN,
                                    currency=Currency.RUB)

            response_staff_4_months = self.client.get(
                url,
                data={
                    'date__gte': date1_1.strftime(DATE_FORMAT),
                    'date__lte': date4_1.strftime(DATE_FORMAT),
                },
                HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token
            )
            response_staff_4_months_from_cache = self.client.get(
                url,
                data={
                    'date__gte': date1_1.strftime(DATE_FORMAT),
                    'date__lte': date4_1.strftime(DATE_FORMAT),
                },
                HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token
            )
            response_staff_3_months_ordered = self.client.get(
                url,
                data={
                    'date__gte': date2_1.strftime(DATE_FORMAT),
                    'date__lte': date4_1.strftime(DATE_FORMAT),
                    'ordering': '-signups',
                },
                HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token
            )
            response_staff_3_months_ordered_from_cache = self.client.get(
                url,
                data={
                    'date__gte': date2_1.strftime(DATE_FORMAT),
                    'date__lte': date4_1.strftime(DATE_FORMAT),
                    'ordering': '-signups',
                },
                HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token
            )
            response_partner_4_months = self.client.get(
                url,
                data={
                    'date__gte': date1_1.strftime(DATE_FORMAT),
                    'date__lte': date4_1.strftime(DATE_FORMAT),
                },
                HTTP_AUTHORIZATION='JWT %s' % self.partner_jwt_token
            )
            response_partner_4_months_from_cache = self.client.get(
                url,
                data={
                    'date__gte': date1_1.strftime(DATE_FORMAT),
                    'date__lte': date4_1.strftime(DATE_FORMAT),
                },
                HTTP_AUTHORIZATION='JWT %s' % self.partner_jwt_token
            )
            response_partner_4_months_filtered = self.client.get(
                url,
                data={
                    'date__gte': date1_1.strftime(DATE_FORMAT),
                    'date__lte': date4_1.strftime(DATE_FORMAT),
                    'promo': promo1.uuid
                },
                HTTP_AUTHORIZATION='JWT %s' % self.partner_jwt_token
            )
            response_partner_4_months_filtered_from_cache = self.client.get(
                url,
                data={
                    'date__gte': date1_1.strftime(DATE_FORMAT),
                    'date__lte': date4_1.strftime(DATE_FORMAT),
                    'promo': promo1.uuid
                },
                HTTP_AUTHORIZATION='JWT %s' % self.partner_jwt_token
            )

        expected_data = {
            'count': 4,
            'totals': {
                'signups': 4,
                'first_deposits_count': 4,
                'first_deposits_amount': '%.2f' % round(payment0_1.amount_eur +
                                                        payment1_1.amount_eur +
                                                        payment2_1.amount_eur +
                                                        payment3_1.amount_eur, 2),
                'deposits_count': 7,
                'deposits_amount': '%.2f' % round(payment0_1.amount_eur +
                                                  payment1_1.amount_eur +
                                                  payment1_2.amount_eur +
                                                  payment2_1.amount_eur +
                                                  payment2_2.amount_eur +
                                                  payment3_1.amount_eur +
                                                  payment2_4.amount_eur, 2),
                'net_gaming': '%.2f' % round(
                    (bet0_1.outcome_eur - bet0_1.income_eur) +
                    (bet1_1.outcome_eur - bet1_1.income_eur) +
                    (bet1_2.outcome_eur - bet1_2.income_eur) +
                    (bet2_1.outcome_eur - bet2_1.income_eur) +
                    (bet3_1.outcome_eur - bet3_1.income_eur) +
                    (bet1_4.outcome_eur - bet1_4.income_eur), 2
                ),
                'profit': '%.2f' % round(
                    (bet0_1.outcome_eur - bet0_1.income_eur) * program_rate0 / 100 +
                    (bet1_1.outcome_eur - bet1_1.income_eur) * program_rate / 100 +
                    (bet1_2.outcome_eur - bet1_2.income_eur) * program_rate / 100 +
                    (bet2_1.outcome_eur - bet2_1.income_eur) * program_rate / 100 +
                    (bet3_1.outcome_eur - bet3_1.income_eur) * program_rate / 100 +
                    (bet1_4.outcome_eur - bet1_4.income_eur) * program_rate / 100, 2
                ),  # TODO в зависимости от программы, по разному считается profit, формула будет браться из ProgramType
                'clicks': 25,
                'unique_clicks': 20,
            },
            'results': [
                {
                    'month': datetime_to_month_repr(date4_1),
                    'signups': 0,
                    'first_deposits_count': 0,
                    'first_deposits_amount': '0.00',
                    'deposits_count': 0,
                    'deposits_amount': '0.00',
                    'net_gaming': '0.00',
                    'profit': '0.00',
                    'clicks': 10,
                    'unique_clicks': 8,
                },
                {
                    'month': datetime_to_month_repr(date3_1),
                    'signups': 1,
                    'first_deposits_count': 1,
                    'first_deposits_amount': '%.2f' % round(payment3_1.amount_eur, 2),
                    'deposits_count': 2,
                    'deposits_amount': '%.2f' % round(payment3_1.amount_eur +
                                                      payment2_4.amount_eur, 2),
                    'net_gaming': '%.2f' % round((bet3_1.outcome_eur - bet3_1.income_eur) +
                                                 (bet1_4.outcome_eur - bet1_4.income_eur), 2),
                    'profit': '%.2f' % round(
                        (bet3_1.outcome_eur - bet3_1.income_eur) * program_rate / 100 +
                        (bet1_4.outcome_eur - bet1_4.income_eur) * program_rate / 100, 2
                    ),
                    'clicks': 10,
                    'unique_clicks': 8,
                },
                {
                    'month': datetime_to_month_repr(date2_1),
                    'signups': 2,
                    'first_deposits_count': 2,
                    'first_deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                            payment2_1.amount_eur, 2),
                    'deposits_count': 4,
                    'deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                      payment1_2.amount_eur +
                                                      payment2_1.amount_eur +
                                                      payment2_2.amount_eur, 2),
                    'net_gaming': '%.2f' % round((bet1_1.outcome_eur - bet1_1.income_eur) +
                                                 (bet1_2.outcome_eur - bet1_2.income_eur) +
                                                 (bet2_1.outcome_eur - bet2_1.income_eur), 2),
                    'profit': '%.2f' % round(
                        (bet1_1.outcome_eur - bet1_1.income_eur) * program_rate / 100 +
                        (bet1_2.outcome_eur - bet1_2.income_eur) * program_rate / 100 +
                        (bet2_1.outcome_eur - bet2_1.income_eur) * program_rate / 100, 2),
                    'clicks': 5,
                    'unique_clicks': 4,
                },
                {
                    'month': datetime_to_month_repr(date1_1),
                    'signups': 1,
                    'first_deposits_count': 1,
                    'first_deposits_amount': '%.2f' % round(payment0_1.amount_eur, 2),
                    'deposits_count': 1,
                    'deposits_amount': '%.2f' % round(payment0_1.amount_eur, 2),
                    'net_gaming': '%.2f' % round((bet0_1.outcome_eur - bet0_1.income_eur), 2),
                    'profit': '%.2f' % round((bet0_1.outcome_eur - bet0_1.income_eur) * program_rate0 / 100, 2),
                    'clicks': 0,
                    'unique_clicks': 0,
                },
            ],
        }

        self.assertEqual(response_staff_4_months.status_code, HTTP_200_OK)
        self.assertEqual(response_staff_4_months.data, expected_data)
        self.assertEqual(response_staff_4_months_from_cache.data, expected_data)

        expected_data = {
            'count': 3,
            'totals': {
                'signups': 3,
                'first_deposits_count': 3,
                'first_deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                        payment2_1.amount_eur +
                                                        payment3_1.amount_eur, 2),
                'deposits_count': 6,
                'deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                  payment1_2.amount_eur +
                                                  payment2_1.amount_eur +
                                                  payment2_2.amount_eur +
                                                  payment3_1.amount_eur +
                                                  payment2_4.amount_eur, 2),
                'net_gaming': '%.2f' % round(
                    (bet1_1.outcome_eur - bet1_1.income_eur) +
                    (bet1_2.outcome_eur - bet1_2.income_eur) +
                    (bet2_1.outcome_eur - bet2_1.income_eur) +
                    (bet3_1.outcome_eur - bet3_1.income_eur) +
                    (bet1_4.outcome_eur - bet1_4.income_eur), 2
                ),
                'profit': '%.2f' % round(
                    (bet1_1.outcome_eur - bet1_1.income_eur) * program_rate / 100 +
                    (bet1_2.outcome_eur - bet1_2.income_eur) * program_rate / 100 +
                    (bet2_1.outcome_eur - bet2_1.income_eur) * program_rate / 100 +
                    (bet3_1.outcome_eur - bet3_1.income_eur) * program_rate / 100 +
                    (bet1_4.outcome_eur - bet1_4.income_eur) * program_rate / 100, 2
                ),  # TODO в зависимости от программы, по разному считается profit, формула будет браться из ProgramType
                'clicks': 25,
                'unique_clicks': 20,
            },
            'results': [
                {
                    'month': datetime_to_month_repr(date2_1),
                    'signups': 2,
                    'first_deposits_count': 2,
                    'first_deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                            payment2_1.amount_eur, 2),
                    'deposits_count': 4,
                    'deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                      payment1_2.amount_eur +
                                                      payment2_1.amount_eur +
                                                      payment2_2.amount_eur, 2),
                    'net_gaming': '%.2f' % round((bet1_1.outcome_eur - bet1_1.income_eur) +
                                                 (bet1_2.outcome_eur - bet1_2.income_eur) +
                                                 (bet2_1.outcome_eur - bet2_1.income_eur), 2),
                    'profit': '%.2f' % round(
                        (bet1_1.outcome_eur - bet1_1.income_eur) * program_rate / 100 +
                        (bet1_2.outcome_eur - bet1_2.income_eur) * program_rate / 100 +
                        (bet2_1.outcome_eur - bet2_1.income_eur) * program_rate / 100, 2
                    ),
                    'clicks': 5,
                    'unique_clicks': 4,
                },
                {
                    'month': datetime_to_month_repr(date3_1),
                    'signups': 1,
                    'first_deposits_count': 1,
                    'first_deposits_amount': '%.2f' % round(payment3_1.amount_eur, 2),
                    'deposits_count': 2,
                    'deposits_amount': '%.2f' % round(payment3_1.amount_eur +
                                                      payment2_4.amount_eur, 2),
                    'net_gaming': '%.2f' % round((bet3_1.outcome_eur - bet3_1.income_eur) +
                                                 (bet1_4.outcome_eur - bet1_4.income_eur), 2),
                    'profit': '%.2f' % round(
                        (bet3_1.outcome_eur - bet3_1.income_eur) * program_rate / 100 +
                        (bet1_4.outcome_eur - bet1_4.income_eur) * program_rate / 100, 2
                    ),
                    'clicks': 10,
                    'unique_clicks': 8,
                },
                {
                    'month': datetime_to_month_repr(date4_1),
                    'signups': 0,
                    'first_deposits_count': 0,
                    'first_deposits_amount': '0.00',
                    'deposits_count': 0,
                    'deposits_amount': '0.00',
                    'net_gaming': '0.00',
                    'profit': '0.00',
                    'clicks': 10,
                    'unique_clicks': 8,
                },
            ],
        }
        self.assertEqual(response_staff_3_months_ordered.status_code, HTTP_200_OK)
        self.assertEqual(response_staff_3_months_ordered.data, expected_data)
        self.assertEqual(response_staff_3_months_ordered_from_cache.data, expected_data)

        expected_data = {
            'count': 4,
            'totals': {
                'signups': 3,
                'first_deposits_count': 3,
                'first_deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                        payment2_1.amount_eur +
                                                        payment3_1.amount_eur, 2),
                'deposits_count': 6,
                'deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                  payment1_2.amount_eur +
                                                  payment2_1.amount_eur +
                                                  payment2_2.amount_eur +
                                                  payment3_1.amount_eur +
                                                  payment2_4.amount_eur, 2),
                'net_gaming': '%.2f' % round(
                    (bet1_1.outcome_eur - bet1_1.income_eur) +
                    (bet1_2.outcome_eur - bet1_2.income_eur) +
                    (bet2_1.outcome_eur - bet2_1.income_eur) +
                    (bet3_1.outcome_eur - bet3_1.income_eur) +
                    (bet1_4.outcome_eur - bet1_4.income_eur), 2
                ),
                'profit': '%.2f' % round(
                    (bet1_1.outcome_eur - bet1_1.income_eur) * program_rate / 100 +
                    (bet1_2.outcome_eur - bet1_2.income_eur) * program_rate / 100 +
                    (bet2_1.outcome_eur - bet2_1.income_eur) * program_rate / 100 +
                    (bet3_1.outcome_eur - bet3_1.income_eur) * program_rate / 100 +
                    (bet1_4.outcome_eur - bet1_4.income_eur) * program_rate / 100, 2
                ),  # TODO в зависимости от программы, по разному считается profit, формула будет браться из ProgramType
                'clicks': 20,
                'unique_clicks': 16,
            },
            'results': [
                {
                    'month': datetime_to_month_repr(date4_1),
                    'signups': 0,
                    'first_deposits_count': 0,
                    'first_deposits_amount': '0.00',
                    'deposits_count': 0,
                    'deposits_amount': '0.00',
                    'net_gaming': '0.00',
                    'profit': '0.00',
                    'clicks': 10,
                    'unique_clicks': 8,
                },
                {
                    'month': datetime_to_month_repr(date3_1),
                    'signups': 1,
                    'first_deposits_count': 1,
                    'first_deposits_amount': '%.2f' % round(payment3_1.amount_eur, 2),
                    'deposits_count': 2,
                    'deposits_amount': '%.2f' % round(payment3_1.amount_eur +
                                                      payment2_4.amount_eur, 2),
                    'net_gaming': '%.2f' % round((bet3_1.outcome_eur - bet3_1.income_eur) +
                                                 (bet1_4.outcome_eur - bet1_4.income_eur), 2),
                    'profit': '%.2f' % round(
                        (bet3_1.outcome_eur - bet3_1.income_eur) * program_rate / 100 +
                        (bet1_4.outcome_eur - bet1_4.income_eur) * program_rate / 100, 2
                    ),
                    'clicks': 10,
                    'unique_clicks': 8,
                },
                {
                    'month': datetime_to_month_repr(date2_1),
                    'signups': 2,
                    'first_deposits_count': 2,
                    'first_deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                            payment2_1.amount_eur, 2),
                    'deposits_count': 4,
                    'deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                      payment1_2.amount_eur +
                                                      payment2_1.amount_eur +
                                                      payment2_2.amount_eur, 2),
                    'net_gaming': '%.2f' % round(
                        (bet1_1.outcome_eur - bet1_1.income_eur) +
                        (bet1_2.outcome_eur - bet1_2.income_eur) +
                        (bet2_1.outcome_eur - bet2_1.income_eur), 2
                    ),
                    'profit': '%.2f' % round(
                        (bet1_1.outcome_eur - bet1_1.income_eur) * program_rate / 100 +
                        (bet1_2.outcome_eur - bet1_2.income_eur) * program_rate / 100 +
                        (bet2_1.outcome_eur - bet2_1.income_eur) * program_rate / 100, 2
                    ),  # TODO в зависимости от программы, по разному считается profit, формула будет браться из ProgramType
                    'clicks': 0,
                    'unique_clicks': 0,
                },
                {
                    'month': datetime_to_month_repr(date1_1),
                    'signups': 0,
                    'first_deposits_count': 0,
                    'first_deposits_amount': '0.00',
                    'deposits_count': 0,
                    'deposits_amount': '0.00',
                    'net_gaming': '0.00',
                    'profit': '0.00',
                    'clicks': 0,
                    'unique_clicks': 0,
                },
            ],
        }
        self.assertEqual(response_partner_4_months.status_code, HTTP_200_OK)
        self.assertEqual(response_partner_4_months.data, expected_data)
        self.assertEqual(response_partner_4_months_from_cache.data, expected_data)

        expected_data = {
            'count': 4,
            'totals': {
                'signups': 2,
                'first_deposits_count': 2,
                'first_deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                        payment2_1.amount_eur, 2),
                'deposits_count': 5,
                'deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                  payment1_2.amount_eur +
                                                  payment2_1.amount_eur +
                                                  payment2_2.amount_eur +
                                                  payment2_4.amount_eur, 2),
                'net_gaming': '%.2f' % round(
                    (bet1_1.outcome_eur - bet1_1.income_eur) +
                    (bet1_2.outcome_eur - bet1_2.income_eur) +
                    (bet2_1.outcome_eur - bet2_1.income_eur) +
                    (bet1_4.outcome_eur - bet1_4.income_eur), 2
                ),
                'profit': '%.2f' % round(
                    (bet1_1.outcome_eur - bet1_1.income_eur) * program_rate / 100 +
                    (bet1_2.outcome_eur - bet1_2.income_eur) * program_rate / 100 +
                    (bet2_1.outcome_eur - bet2_1.income_eur) * program_rate / 100 +
                    (bet1_4.outcome_eur - bet1_4.income_eur) * program_rate / 100, 2
                ),  # TODO в зависимости от программы, по разному считается profit, формула будет браться из ProgramType
                'clicks': 10,
                'unique_clicks': 8,
            },
            'results': [
                {
                    'month': datetime_to_month_repr(date4_1),
                    'signups': 0,
                    'first_deposits_count': 0,
                    'first_deposits_amount': '0.00',
                    'deposits_count': 0,
                    'deposits_amount': '0.00',
                    'net_gaming': '0.00',
                    'profit': '0.00',
                    'clicks': 5,
                    'unique_clicks': 4,
                },
                {
                    'month': datetime_to_month_repr(date3_1),
                    'signups': 0,
                    'first_deposits_count': 0,
                    'first_deposits_amount': '0.00',
                    'deposits_count': 1,
                    'deposits_amount': '%.2f' % round(payment2_4.amount_eur, 2),
                    'net_gaming': '%.2f' % round((bet1_4.outcome_eur - bet1_4.income_eur), 2),
                    'profit': '%.2f' % round(
                        (bet1_4.outcome_eur - bet1_4.income_eur) * program_rate / 100, 2
                    ),
                    'clicks': 5,
                    'unique_clicks': 4,
                },
                {
                    'month': datetime_to_month_repr(date2_1),
                    'signups': 2,
                    'first_deposits_count': 2,
                    'first_deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                            payment2_1.amount_eur, 2),
                    'deposits_count': 4,
                    'deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                      payment1_2.amount_eur +
                                                      payment2_1.amount_eur +
                                                      payment2_2.amount_eur, 2),
                    'net_gaming': '%.2f' % round(
                        (bet1_1.outcome_eur - bet1_1.income_eur) +
                        (bet1_2.outcome_eur - bet1_2.income_eur) +
                        (bet2_1.outcome_eur - bet2_1.income_eur), 2
                    ),
                    'profit': '%.2f' % round(
                        (bet1_1.outcome_eur - bet1_1.income_eur) * program_rate / 100 +
                        (bet1_2.outcome_eur - bet1_2.income_eur) * program_rate / 100 +
                        (bet2_1.outcome_eur - bet2_1.income_eur) * program_rate / 100, 2
                    ),  # TODO в зависимости от программы, по разному считается profit, формула будет браться из ProgramType
                    'clicks': 0,
                    'unique_clicks': 0,
                },
                {
                    'month': datetime_to_month_repr(date1_1),
                    'signups': 0,
                    'first_deposits_count': 0,
                    'first_deposits_amount': '0.00',
                    'deposits_count': 0,
                    'deposits_amount': '0.00',
                    'net_gaming': '0.00',
                    'profit': '0.00',
                    'clicks': 0,
                    'unique_clicks': 0,
                },
            ],
        }
        self.assertEqual(response_partner_4_months_filtered.status_code, HTTP_200_OK)
        self.assertEqual(response_partner_4_months_filtered.data, expected_data)
        self.assertEqual(response_partner_4_months_filtered_from_cache.data, expected_data)

    @tag('local')
    def test_list_promos(self):
        """
        Проверка что валидности сагрегированных по promo значений.
        В нулевой день происходит действия для стороннего partner0:
            * Регистрация 1 игрока
            * 1 первый депозит
            * 1 ставка

        В первый день происходит:
            * Регистрация 2-х игроков.
            * Каждый игрок создает 3 платежа из которых 2 валидны и 1 является первым депозитом.
            * 1-й игрок создает 3 ставки из которых 2 валидны.
            * 2-й игрок создает 3 ставки из которых 1 валидна.

        Во второй день происходит:
            * регистрация 1 нового игрока
            * новый игрок создает 1 первый депозит
            * старый игрок создает 1 депозит
            * новый игрок создает проигранную ставку
            * старый игрок создает выйгранную ставку

        Проверяем корректность:
            * Админ запросил репорты за все дни и получил информацию по всем партнерам и промо
            * Партнер запросил репорты за все дни и получил только информацию о своих promo
        """
        redis_client = RedisClient()
        redis_client.flushall()

        url = reverse('public:report-list-promos')
        long_long_long_ago = datetime.datetime(2018, 8, 7, 8, 8, 8)
        long_long_ago = datetime.datetime(2018, 8, 8, 8, 8, 8)
        long_ago = datetime.datetime(2018, 8, 9, 8, 8, 8)
        now = datetime.datetime(2018, 8, 10, 8, 8, 8)

        partner0 = PartnerFactory()
        promo0 = PromoFactory(partner=partner0, name='charlie')
        program_rate0 = promo0.program.rate
        program_rate = 60
        program = ProgramFactory(rate=program_rate)
        promo1 = PromoFactory(program=program, partner=self.partner, name='betta')
        promo2 = PromoFactory(program=program, partner=self.partner, name='alpha')

        with ClickhouseInfrastructure(
                partners=[partner0],
                timestamp_range=[long_long_long_ago, long_long_ago],
        ) as clickhouse_client0, ClickhouseInfrastructure(
            partners=[self.partner],
            timestamp_range=[long_long_ago, long_ago],
        ) as clickhouse_client1, ClickhouseInfrastructure(
            partners=[self.partner],
            timestamp_range=[long_ago, now],
        ) as clickhouse_client2:
            with freeze_time(long_long_long_ago):
                player0 = PlayerFactory(promo=promo0)
                payment0_1 = PaymentFactory(player=player0, status=PaymentStatus.SUCCESS,
                                            currency=Currency.USD, direction=PaymentDirection.INCOMING, is_first=True)
                bet0_1 = BetFactory(player=player0, status=BetStatus.ACCEPTED, state=BetState.WIN,
                                    currency=Currency.USD)

            with freeze_time(long_long_ago):
                player1 = PlayerFactory(promo=promo1)
                player2 = PlayerFactory(promo=promo1)

                payment1_1 = PaymentFactory(player=player1, status=PaymentStatus.SUCCESS,
                                            currency=Currency.USD, direction=PaymentDirection.INCOMING, is_first=True)
                payment1_2 = PaymentFactory(player=player1, status=PaymentStatus.SUCCESS,
                                            currency=Currency.USD, direction=PaymentDirection.INCOMING)
                payment1_3 = PaymentFactory(player=player1, status=PaymentStatus.SUCCESS,
                                            currency=Currency.USD, direction=PaymentDirection.OUTGOING)
                payment2_1 = PaymentFactory(player=player2, status=PaymentStatus.SUCCESS,
                                            currency=Currency.GBP, direction=PaymentDirection.INCOMING, is_first=True)
                payment2_2 = PaymentFactory(player=player2, status=PaymentStatus.SUCCESS,
                                            currency=Currency.GBP, direction=PaymentDirection.INCOMING)
                payment2_3 = PaymentFactory(player=player2, status=PaymentStatus.CANCELED,
                                            currency=Currency.GBP, direction=PaymentDirection.INCOMING)

                bet1_1 = BetFactory(player=player1, status=BetStatus.ACCEPTED, state=BetState.WIN,
                                    currency=Currency.USD)
                bet1_2 = BetFactory(player=player1, status=BetStatus.ACCEPTED, state=BetState.LOSE,
                                    currency=Currency.USD)
                bet1_3 = BetFactory(player=player1, status=BetStatus.CANCELED, state=BetState.WIN,
                                    currency=Currency.USD)
                bet2_1 = BetFactory(player=player2, status=BetStatus.ACCEPTED, state=BetState.WIN,
                                    currency=Currency.GBP)
                bet2_2 = BetFactory(player=player2, status=BetStatus.ACCEPTED, state=BetState.WIN,
                                    currency=Currency.GBP, is_freebet=True)
                bet2_3 = BetFactory(player=player2, status=BetStatus.ACCEPTED, state=BetState.UNKNOWN,
                                    currency=Currency.GBP)

            with freeze_time(long_ago):
                player3 = PlayerFactory(promo=promo2)
                payment3_1 = PaymentFactory(player=player3, status=PaymentStatus.SUCCESS,
                                            currency=Currency.RUB, direction=PaymentDirection.INCOMING, is_first=True)
                payment2_4 = PaymentFactory(player=player2, status=PaymentStatus.SUCCESS,
                                            currency=Currency.RUB, direction=PaymentDirection.INCOMING)

                bet3_1 = BetFactory(player=player3, status=BetStatus.ACCEPTED, state=BetState.LOSE,
                                    currency=Currency.RUB)
                bet1_4 = BetFactory(player=player1, status=BetStatus.ACCEPTED, state=BetState.WIN,
                                    currency=Currency.RUB)

            response_staff_4_days = self.client.get(
                url,
                data={
                    'date__gte': long_long_long_ago.strftime(DATE_FORMAT),
                    'date__lte': now.strftime(DATE_FORMAT),
                },
                HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token
            )
            response_staff_4_days_from_cache = self.client.get(
                url,
                data={
                    'date__gte': long_long_long_ago.strftime(DATE_FORMAT),
                    'date__lte': now.strftime(DATE_FORMAT),
                },
                HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token
            )
            response_staff_3_days_ordered = self.client.get(
                url,
                data={
                    'date__gte': long_long_ago.strftime(DATE_FORMAT),
                    'date__lte': now.strftime(DATE_FORMAT),
                    'ordering': '-promo.name',
                },
                HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token
            )
            response_staff_3_days_ordered_from_cache = self.client.get(
                url,
                data={
                    'date__gte': long_long_ago.strftime(DATE_FORMAT),
                    'date__lte': now.strftime(DATE_FORMAT),
                    'ordering': '-promo.name',
                },
                HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token
            )
            response_partner_4_days = self.client.get(
                url,
                data={
                    'date__gte': long_long_long_ago.strftime(DATE_FORMAT),
                    'date__lte': now.strftime(DATE_FORMAT),
                },
                HTTP_AUTHORIZATION='JWT %s' % self.partner_jwt_token
            )
            response_partner_4_days_from_cache = self.client.get(
                url,
                data={
                    'date__gte': long_long_long_ago.strftime(DATE_FORMAT),
                    'date__lte': now.strftime(DATE_FORMAT),
                },
                HTTP_AUTHORIZATION='JWT %s' % self.partner_jwt_token
            )
            response_partner_4_days_filtered = self.client.get(
                url,
                data={
                    'date__gte': long_long_long_ago.strftime(DATE_FORMAT),
                    'date__lte': now.strftime(DATE_FORMAT),
                    'promo': promo1.uuid
                },
                HTTP_AUTHORIZATION='JWT %s' % self.partner_jwt_token
            )
            response_partner_4_days_filtered_from_cache = self.client.get(
                url,
                data={
                    'date__gte': long_long_long_ago.strftime(DATE_FORMAT),
                    'date__lte': now.strftime(DATE_FORMAT),
                    'promo': promo1.uuid
                },
                HTTP_AUTHORIZATION='JWT %s' % self.partner_jwt_token
            )

        expected_data = {
            'count': 4,
            'totals': {
                'signups': 4,
                'first_deposits_count': 4,
                'first_deposits_amount': '%.2f' % round(payment0_1.amount_eur +
                                                        payment1_1.amount_eur +
                                                        payment2_1.amount_eur +
                                                        payment3_1.amount_eur, 2),
                'deposits_count': 7,
                'deposits_amount': '%.2f' % round(payment0_1.amount_eur +
                                                  payment1_1.amount_eur +
                                                  payment1_2.amount_eur +
                                                  payment2_1.amount_eur +
                                                  payment2_2.amount_eur +
                                                  payment3_1.amount_eur +
                                                  payment2_4.amount_eur, 2),
                'net_gaming': '%.2f' % round(
                    (bet0_1.outcome_eur - bet0_1.income_eur) +
                    (bet1_1.outcome_eur - bet1_1.income_eur) +
                    (bet1_2.outcome_eur - bet1_2.income_eur) +
                    (bet2_1.outcome_eur - bet2_1.income_eur) +
                    (bet3_1.outcome_eur - bet3_1.income_eur) +
                    (bet1_4.outcome_eur - bet1_4.income_eur), 2
                ),
                'profit': '%.2f' % round(
                    (bet0_1.outcome_eur - bet0_1.income_eur) * program_rate0 / 100 +
                    (bet1_1.outcome_eur - bet1_1.income_eur) * program_rate / 100 +
                    (bet1_2.outcome_eur - bet1_2.income_eur) * program_rate / 100 +
                    (bet2_1.outcome_eur - bet2_1.income_eur) * program_rate / 100 +
                    (bet3_1.outcome_eur - bet3_1.income_eur) * program_rate / 100 +
                    (bet1_4.outcome_eur - bet1_4.income_eur) * program_rate / 100, 2
                ),  # TODO в зависимости от программы, по разному считается profit, формула будет браться из ProgramType
                'clicks': 25,
                'unique_clicks': 20,
            },
            'results': [
                {
                    'promo': 'alpha',
                    'signups': 1,
                    'first_deposits_count': 1,
                    'first_deposits_amount': '%.2f' % round(payment3_1.amount_eur, 2),
                    'deposits_count': 1,
                    'deposits_amount': '%.2f' % round(payment3_1.amount_eur, 2),
                    'net_gaming': '%.2f' % round((bet3_1.outcome_eur - bet3_1.income_eur), 2),
                    'profit': '%.2f' % round(
                        (bet3_1.outcome_eur - bet3_1.income_eur) * program_rate / 100, 2
                    ),
                    'clicks': 10,
                    'unique_clicks': 8,
                },
                {
                    'promo': 'betta',
                    'signups': 2,
                    'first_deposits_count': 2,
                    'first_deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                            payment2_1.amount_eur, 2),
                    'deposits_count': 5,
                    'deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                      payment1_2.amount_eur +
                                                      payment2_1.amount_eur +
                                                      payment2_2.amount_eur +
                                                      payment2_4.amount_eur, 2),
                    'net_gaming': '%.2f' % round((bet1_1.outcome_eur - bet1_1.income_eur) +
                                                 (bet1_2.outcome_eur - bet1_2.income_eur) +
                                                 (bet2_1.outcome_eur - bet2_1.income_eur) +
                                                 (bet1_4.outcome_eur - bet1_4.income_eur), 2),
                    'profit': '%.2f' % round(
                        (bet1_1.outcome_eur - bet1_1.income_eur) * program_rate / 100 +
                        (bet1_2.outcome_eur - bet1_2.income_eur) * program_rate / 100 +
                        (bet2_1.outcome_eur - bet2_1.income_eur) * program_rate / 100 +
                        (bet1_4.outcome_eur - bet1_4.income_eur) * program_rate / 100, 2
                    ),
                    'clicks': 10,
                    'unique_clicks': 8,
                },
                {
                    'promo': 'charlie',
                    'signups': 1,
                    'first_deposits_count': 1,
                    'first_deposits_amount': '%.2f' % round(payment0_1.amount_eur, 2),
                    'deposits_count': 1,
                    'deposits_amount': '%.2f' % round(payment0_1.amount_eur, 2),
                    'net_gaming': '%.2f' % round((bet0_1.outcome_eur - bet0_1.income_eur), 2),
                    'profit': '%.2f' % round(
                        (bet0_1.outcome_eur - bet0_1.income_eur) * program_rate0 / 100, 2),
                    'clicks': 5,
                    'unique_clicks': 4,
                },
                {
                    'promo': 'cyberbet',
                    'signups': 0,
                    'first_deposits_count': 0,
                    'first_deposits_amount': '0.00',
                    'deposits_count': 0,
                    'deposits_amount': '0.00',
                    'net_gaming': '0.00',
                    'profit': '0.00',
                    'clicks': 0,
                    'unique_clicks': 0,
                },
            ],
        }

        self.assertEqual(response_staff_4_days.status_code, HTTP_200_OK)
        self.assertEqual(response_staff_4_days.data, expected_data)
        self.assertEqual(response_staff_4_days_from_cache.data, expected_data)

        expected_data = {
            'count': 4,
            'totals': {
                'signups': 3,
                'first_deposits_count': 3,
                'first_deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                        payment2_1.amount_eur +
                                                        payment3_1.amount_eur, 2),
                'deposits_count': 6,
                'deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                  payment1_2.amount_eur +
                                                  payment2_1.amount_eur +
                                                  payment2_2.amount_eur +
                                                  payment3_1.amount_eur +
                                                  payment2_4.amount_eur, 2),
                'net_gaming': '%.2f' % round(
                    (bet1_1.outcome_eur - bet1_1.income_eur) +
                    (bet1_2.outcome_eur - bet1_2.income_eur) +
                    (bet2_1.outcome_eur - bet2_1.income_eur) +
                    (bet3_1.outcome_eur - bet3_1.income_eur) +
                    (bet1_4.outcome_eur - bet1_4.income_eur), 2
                ),
                'profit': '%.2f' % round(
                    (bet1_1.outcome_eur - bet1_1.income_eur) * program_rate / 100 +
                    (bet1_2.outcome_eur - bet1_2.income_eur) * program_rate / 100 +
                    (bet2_1.outcome_eur - bet2_1.income_eur) * program_rate / 100 +
                    (bet3_1.outcome_eur - bet3_1.income_eur) * program_rate / 100 +
                    (bet1_4.outcome_eur - bet1_4.income_eur) * program_rate / 100, 2
                ),  # TODO в зависимости от программы, по разному считается profit, формула будет браться из ProgramType
                'clicks': 23,
                'unique_clicks': 19,
            },
            'results': [
                {
                    'promo': 'cyberbet',
                    'signups': 0,
                    'first_deposits_count': 0,
                    'first_deposits_amount': '0.00',
                    'deposits_count': 0,
                    'deposits_amount': '0.00',
                    'net_gaming': '0.00',
                    'profit': '0.00',
                    'clicks': 0,
                    'unique_clicks': 0,
                },
                {
                    'promo': 'charlie',
                    'signups': 0,
                    'first_deposits_count': 0,
                    'first_deposits_amount': '0.00',
                    'deposits_count': 0,
                    'deposits_amount': '0.00',
                    'net_gaming': '0.00',
                    'profit': '0.00',
                    'clicks': 3,          # клики происходили позже получения данных из NATS клиента
                    'unique_clicks': 3,
                },
                {
                    'promo': 'betta',
                    'signups': 2,
                    'first_deposits_count': 2,
                    'first_deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                            payment2_1.amount_eur, 2),
                    'deposits_count': 5,
                    'deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                      payment1_2.amount_eur +
                                                      payment2_1.amount_eur +
                                                      payment2_2.amount_eur +
                                                      payment2_4.amount_eur, 2),
                    'net_gaming': '%.2f' % round((bet1_1.outcome_eur - bet1_1.income_eur) +
                                                 (bet1_2.outcome_eur - bet1_2.income_eur) +
                                                 (bet2_1.outcome_eur - bet2_1.income_eur) +
                                                 (bet1_4.outcome_eur - bet1_4.income_eur), 2),
                    'profit': '%.2f' % round(
                        (bet1_1.outcome_eur - bet1_1.income_eur) * program_rate / 100 +
                        (bet1_2.outcome_eur - bet1_2.income_eur) * program_rate / 100 +
                        (bet2_1.outcome_eur - bet2_1.income_eur) * program_rate / 100 +
                        (bet1_4.outcome_eur - bet1_4.income_eur) * program_rate / 100, 2
                    ),
                    'clicks': 10,
                    'unique_clicks': 8,
                },
                {
                    'promo': 'alpha',
                    'signups': 1,
                    'first_deposits_count': 1,
                    'first_deposits_amount': '%.2f' % round(payment3_1.amount_eur, 2),
                    'deposits_count': 1,
                    'deposits_amount': '%.2f' % round(payment3_1.amount_eur, 2),
                    'net_gaming': '%.2f' % round((bet3_1.outcome_eur - bet3_1.income_eur), 2),
                    'profit': '%.2f' % round(
                        (bet3_1.outcome_eur - bet3_1.income_eur) * program_rate / 100, 2
                    ),
                    'clicks': 10,
                    'unique_clicks': 8,
                },
            ],
        }
        self.assertEqual(response_staff_3_days_ordered.status_code, HTTP_200_OK)
        self.assertEqual(response_staff_3_days_ordered.data, expected_data)
        self.assertEqual(response_staff_3_days_ordered_from_cache.data, expected_data)

        expected_data = {
            'count': 2,
            'totals': {
                'signups': 3,
                'first_deposits_count': 3,
                'first_deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                        payment2_1.amount_eur +
                                                        payment3_1.amount_eur, 2),
                'deposits_count': 6,
                'deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                  payment1_2.amount_eur +
                                                  payment2_1.amount_eur +
                                                  payment2_2.amount_eur +
                                                  payment3_1.amount_eur +
                                                  payment2_4.amount_eur, 2),
                'net_gaming': '%.2f' % round(
                    (bet1_1.outcome_eur - bet1_1.income_eur) +
                    (bet1_2.outcome_eur - bet1_2.income_eur) +
                    (bet2_1.outcome_eur - bet2_1.income_eur) +
                    (bet3_1.outcome_eur - bet3_1.income_eur) +
                    (bet1_4.outcome_eur - bet1_4.income_eur), 2
                ),
                'profit': '%.2f' % round(
                    (bet1_1.outcome_eur - bet1_1.income_eur) * program_rate / 100 +
                    (bet1_2.outcome_eur - bet1_2.income_eur) * program_rate / 100 +
                    (bet2_1.outcome_eur - bet2_1.income_eur) * program_rate / 100 +
                    (bet3_1.outcome_eur - bet3_1.income_eur) * program_rate / 100 +
                    (bet1_4.outcome_eur - bet1_4.income_eur) * program_rate / 100, 2
                ),  # TODO в зависимости от программы, по разному считается profit, формула будет браться из ProgramType
                'clicks': 20,
                'unique_clicks': 16,
            },
            'results': [
                {
                    'promo': 'alpha',
                    'signups': 1,
                    'first_deposits_count': 1,
                    'first_deposits_amount': '%.2f' % round(payment3_1.amount_eur, 2),
                    'deposits_count': 1,
                    'deposits_amount': '%.2f' % round(payment3_1.amount_eur, 2),
                    'net_gaming': '%.2f' % round((bet3_1.outcome_eur - bet3_1.income_eur), 2),
                    'profit': '%.2f' % round(
                        (bet3_1.outcome_eur - bet3_1.income_eur) * program_rate / 100, 2
                    ),
                    'clicks': 10,
                    'unique_clicks': 8,
                },
                {
                    'promo': 'betta',
                    'signups': 2,
                    'first_deposits_count': 2,
                    'first_deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                            payment2_1.amount_eur, 2),
                    'deposits_count': 5,
                    'deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                      payment1_2.amount_eur +
                                                      payment2_1.amount_eur +
                                                      payment2_2.amount_eur +
                                                      payment2_4.amount_eur, 2),
                    'net_gaming': '%.2f' % round((bet1_1.outcome_eur - bet1_1.income_eur) +
                                                 (bet1_2.outcome_eur - bet1_2.income_eur) +
                                                 (bet2_1.outcome_eur - bet2_1.income_eur) +
                                                 (bet1_4.outcome_eur - bet1_4.income_eur), 2),
                    'profit': '%.2f' % round(
                        (bet1_1.outcome_eur - bet1_1.income_eur) * program_rate / 100 +
                        (bet1_2.outcome_eur - bet1_2.income_eur) * program_rate / 100 +
                        (bet2_1.outcome_eur - bet2_1.income_eur) * program_rate / 100 +
                        (bet1_4.outcome_eur - bet1_4.income_eur) * program_rate / 100, 2
                    ),
                    'clicks': 10,
                    'unique_clicks': 8,
                },
            ],
        }
        self.assertEqual(response_partner_4_days.status_code, HTTP_200_OK)
        self.assertEqual(response_partner_4_days.data, expected_data)
        self.assertEqual(response_partner_4_days_from_cache.data, expected_data)

        expected_data = {
            'count': 1,
            'totals': {
                'signups': 2,
                'first_deposits_count': 2,
                'first_deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                        payment2_1.amount_eur, 2),
                'deposits_count': 5,
                'deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                  payment1_2.amount_eur +
                                                  payment2_1.amount_eur +
                                                  payment2_2.amount_eur +
                                                  payment2_4.amount_eur, 2),
                'net_gaming': '%.2f' % round(
                    (bet1_1.outcome_eur - bet1_1.income_eur) +
                    (bet1_2.outcome_eur - bet1_2.income_eur) +
                    (bet2_1.outcome_eur - bet2_1.income_eur) +
                    (bet1_4.outcome_eur - bet1_4.income_eur), 2
                ),
                'profit': '%.2f' % round(
                    (bet1_1.outcome_eur - bet1_1.income_eur) * program_rate / 100 +
                    (bet1_2.outcome_eur - bet1_2.income_eur) * program_rate / 100 +
                    (bet2_1.outcome_eur - bet2_1.income_eur) * program_rate / 100 +
                    (bet1_4.outcome_eur - bet1_4.income_eur) * program_rate / 100, 2
                ),  # TODO в зависимости от программы, по разному считается profit, формула будет браться из ProgramType
                'clicks': 10,
                'unique_clicks': 8,
            },
            'results': [
                {
                    'promo': 'betta',
                    'signups': 2,
                    'first_deposits_count': 2,
                    'first_deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                            payment2_1.amount_eur, 2),
                    'deposits_count': 5,
                    'deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                                      payment1_2.amount_eur +
                                                      payment2_1.amount_eur +
                                                      payment2_2.amount_eur +
                                                      payment2_4.amount_eur, 2),
                    'net_gaming': '%.2f' % round((bet1_1.outcome_eur - bet1_1.income_eur) +
                                                 (bet1_2.outcome_eur - bet1_2.income_eur) +
                                                 (bet2_1.outcome_eur - bet2_1.income_eur) +
                                                 (bet1_4.outcome_eur - bet1_4.income_eur), 2),
                    'profit': '%.2f' % round(
                        (bet1_1.outcome_eur - bet1_1.income_eur) * program_rate / 100 +
                        (bet1_2.outcome_eur - bet1_2.income_eur) * program_rate / 100 +
                        (bet2_1.outcome_eur - bet2_1.income_eur) * program_rate / 100 +
                        (bet1_4.outcome_eur - bet1_4.income_eur) * program_rate / 100, 2
                    ),
                    'clicks': 10,
                    'unique_clicks': 8,
                },
            ],
        }
        self.assertEqual(response_partner_4_days_filtered.status_code, HTTP_200_OK)
        self.assertEqual(response_partner_4_days_filtered.data, expected_data)
        self.assertEqual(response_partner_4_days_filtered_from_cache.data, expected_data)


class DashboardTesCase(AffiliateAPITestCase):
    @tag('local')
    def test_ok(self):
        """
        Проверка валидности значений dashboard за текущий месяц.
        В нулевой месяц происходит действия для стороннего partner0:
            * Регистрация 1 игрока
            * 1 первый депозит
            * 1 ставка

        В текущий месяц происходит:
        в первый день:
            * Регистрация 2-х игроков.
            * Каждый игрок создает 3 платежа из которых 2 валидны и 1 является первым депозитом.
            * 1-й игрок создает 3 ставки из которых 2 валидны.
            * 2-й игрок создает 3 ставки из которых 1 валидна.

        во второй день:
            * регистрация 1 нового игрока
            * новый игрок создает 1 первый депозит
            * старый игрок создает 1 депозит
            * новый игрок создает проигранную ставку
            * старый игрок создает выйгранную ставку

        """
        redis_client = RedisClient()
        redis_client.flushall()

        url = reverse('public:dashboard')
        date1_1 = datetime.datetime(2018, 7, 7, 7, 7, 7)
        date2_1 = datetime.datetime(2018, 8, 8, 8, 8, 8)
        date2_2 = datetime.datetime(2018, 8, 18, 18, 18, 18)

        partner0 = PartnerFactory()
        promo0 = PromoFactory(partner=partner0)
        program_rate0 = promo0.program.rate
        program_rate = 60
        program = ProgramFactory(rate=program_rate)
        promo1 = PromoFactory(program=program, partner=self.partner)
        promo2 = PromoFactory(program=program, partner=self.partner)

        with freeze_time(date1_1):
            player0 = PlayerFactory(promo=promo0)
            payment0_1 = PaymentFactory(player=player0, status=PaymentStatus.SUCCESS,
                                        currency=Currency.USD, direction=PaymentDirection.INCOMING, is_first=True)
            bet0_1 = BetFactory(player=player0, status=BetStatus.ACCEPTED, state=BetState.WIN,
                                currency=Currency.USD)

        with freeze_time(date2_1):
            player1 = PlayerFactory(promo=promo1)

            payment1_1 = PaymentFactory(player=player1, status=PaymentStatus.SUCCESS,
                                        currency=Currency.USD, direction=PaymentDirection.INCOMING, is_first=True)
            payment1_2 = PaymentFactory(player=player1, status=PaymentStatus.SUCCESS,
                                        currency=Currency.USD, direction=PaymentDirection.INCOMING)
            payment1_3 = PaymentFactory(player=player1, status=PaymentStatus.SUCCESS,
                                        currency=Currency.USD, direction=PaymentDirection.OUTGOING)
            payment0_2 = PaymentFactory(player=player0, status=PaymentStatus.SUCCESS,
                                        currency=Currency.USD, direction=PaymentDirection.INCOMING)

            bet1_1 = BetFactory(player=player1, status=BetStatus.ACCEPTED, state=BetState.WIN,
                                currency=Currency.USD)
            bet1_2 = BetFactory(player=player1, status=BetStatus.ACCEPTED, state=BetState.LOSE,
                                currency=Currency.USD)
            bet1_3 = BetFactory(player=player1, status=BetStatus.CANCELED, state=BetState.WIN,
                                currency=Currency.USD)

        with freeze_time(date2_2):
            player2 = PlayerFactory(promo=promo1)
            player00 = PlayerFactory(promo=promo0)

            payment2_1 = PaymentFactory(player=player2, status=PaymentStatus.SUCCESS,
                                        currency=Currency.GBP, direction=PaymentDirection.INCOMING, is_first=True)
            payment2_2 = PaymentFactory(player=player2, status=PaymentStatus.SUCCESS,
                                        currency=Currency.GBP, direction=PaymentDirection.INCOMING)
            payment2_3 = PaymentFactory(player=player2, status=PaymentStatus.CANCELED,
                                        currency=Currency.GBP, direction=PaymentDirection.INCOMING)

            bet2_1 = BetFactory(player=player2, status=BetStatus.ACCEPTED, state=BetState.WIN,
                                currency=Currency.GBP)
            bet2_2 = BetFactory(player=player2, status=BetStatus.ACCEPTED, state=BetState.WIN,
                                currency=Currency.GBP, is_freebet=True)
            bet2_3 = BetFactory(player=player2, status=BetStatus.ACCEPTED, state=BetState.UNKNOWN,
                                currency=Currency.GBP)
            bet0_2 = BetFactory(player=player00, status=BetStatus.ACCEPTED, state=BetState.WIN,
                                currency=Currency.USD)

            response_staff = self.client.get(url, HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token)
            response_partner = self.client.get(url, HTTP_AUTHORIZATION='JWT %s' % self.partner_jwt_token)

        expected_data = {
            'signups': 3,
            'deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                              payment1_2.amount_eur +
                                              payment2_1.amount_eur +
                                              payment2_2.amount_eur +
                                              payment0_2.amount_eur, 2),
            'net_gaming': '%.2f' % round((bet1_1.outcome_eur - bet1_1.income_eur) +
                                         (bet1_2.outcome_eur - bet1_2.income_eur) +
                                         (bet2_1.outcome_eur - bet2_1.income_eur) +
                                         (bet0_2.outcome_eur - bet0_2.income_eur), 2),
            'profit': '%.2f' % round(
                (bet1_1.outcome_eur - bet1_1.income_eur) * program_rate / 100 +
                (bet1_2.outcome_eur - bet1_2.income_eur) * program_rate / 100 +
                (bet2_1.outcome_eur - bet2_1.income_eur) * program_rate / 100 +
                (bet0_2.outcome_eur - bet0_2.income_eur) * program_rate0 / 100, 2),
        }
        self.assertEqual(response_staff.status_code, HTTP_200_OK)
        self.assertEqual(response_staff.data, expected_data)

        expected_data = {
            'signups': 2,
            'deposits_amount': '%.2f' % round(payment1_1.amount_eur +
                                              payment1_2.amount_eur +
                                              payment2_1.amount_eur +
                                              payment2_2.amount_eur, 2),
            'net_gaming': '%.2f' % round((bet1_1.outcome_eur - bet1_1.income_eur) +
                                         (bet1_2.outcome_eur - bet1_2.income_eur) +
                                         (bet2_1.outcome_eur - bet2_1.income_eur), 2),
            'profit': '%.2f' % round(
                (bet1_1.outcome_eur - bet1_1.income_eur) * program_rate / 100 +
                (bet1_2.outcome_eur - bet1_2.income_eur) * program_rate / 100 +
                (bet2_1.outcome_eur - bet2_1.income_eur) * program_rate / 100, 2),
        }
        self.assertEqual(response_partner.status_code, HTTP_200_OK)
        self.assertEqual(response_partner.data, expected_data)

    def test_zero_values(self):
        """
        Проверка что если нет данных в ответе 0 или 0.00 а не null.
        """
        redis_client = RedisClient()
        redis_client.flushall()

        url = reverse('public:dashboard')

        long_ago = datetime.datetime(2018, 7, 7, 7, 7, 7)
        now = datetime.datetime(2018, 7, 8, 8, 8, 8)

        response = self.client.get(url, HTTP_AUTHORIZATION='JWT %s' % self.partner_jwt_token)

        expected_data = {
            "signups": 0,
            "deposits_amount": '0.00',
            "net_gaming": '0.00',
            "profit": '0.00'
        }
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertEqual(response.data, expected_data)
