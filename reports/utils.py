from base.consts import DATE_FORMAT, VERBOSE_MONTH_FORMAT


def datetime_to_date_repr(dt):
    return dt.strftime(DATE_FORMAT)


def datetime_to_month_repr(dt):
    return dt.strftime(VERBOSE_MONTH_FORMAT)
