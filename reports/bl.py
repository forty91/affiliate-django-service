import hashlib
from collections import namedtuple
from datetime import timedelta, datetime
from decimal import Decimal

import pytz
from django.db.models import Count, Sum
from django.db.models.functions import TruncDay, TruncMonth

from base.consts import DATE_FORMAT, MONTH_FORMAT, TTL_HOUR
from base.utils.clickhouse import ClickhouseClient
from base.utils.datetime import dayrange, monthrange, datetime_now
from base.utils.redis import RedisClient
from bets.models import Bet
from payments.models import Payment
from players.models import Player
from payments.consts import Status as PaymentStatus, Direction as PaymentDirection
from bets.consts import Status as BetStatus, State as BetState
from promos.consts import TrafficActionType
from promos.models import Promo
from traffic.bl import GetTraffic
from traffic.consts import DATA_DELIMITER


class CreateReports:
    def __init__(self, is_staff, request, validated_data):
        self.is_staff = is_staff
        self.user = request.user
        self.validated_data = validated_data
        self.redis_client = RedisClient()
        self.clickhouse_client = ClickhouseClient()
        self.promos = self.get_promos()
        self.reports_type = None
        self.traffic_client = GetTraffic(
            user=self.user,
            promos=self.promos,
            clickhouse_client=self.clickhouse_client,
            is_staff=self.is_staff
        )

    def get_promos(self):
        promos = Promo.objects.none()

        if self.is_staff is True:
            promos = Promo.objects.all()

        if self.is_staff is False:
            promos = Promo.objects.filter(partner__user=self.user)

        if 'promo' in self.validated_data:
            promos = promos.filter(pk=self.validated_data['promo'].uuid)

        return promos

    def generate_redis_key(self, date_gte, date_lte):
        promos_uuids = ':'.join([promo.uuid for promo in self.promos.order_by('name')])
        promos_uuids_hex_hashed = hashlib.sha256(b'%s' % promos_uuids.encode('utf-8')).hexdigest()
        date_gte_formated = date_gte.strftime(DATE_FORMAT)
        date_lte_formated = date_lte.strftime(DATE_FORMAT)
        key = f'reports:{self.reports_type}:{promos_uuids_hex_hashed}:{date_gte_formated}:{date_lte_formated}'
        return key

    def get_reports_from_redis(self, key):
        raise NotImplementedError("Please Implement this method")

    def get_reports_from_postgres_clickhouse(self, date_gte, date_lte, key):
        raise NotImplementedError("Please Implement this method")

    def get_reports(self, date_gte, date_lte):
        key = self.generate_redis_key(date_gte, date_lte)
        if self.redis_client.exists(key):
            reports = self.get_reports_from_redis(key)
        else:
            reports = self.get_reports_from_postgres_clickhouse(date_gte, date_lte, key)

        return reports

    def run(self):
        date_gte = self.validated_data['date__gte']
        date_lte = self.validated_data['date__lte']

        reports = self.get_reports(date_gte, date_lte)
        return reports


class CreateDaysReports(CreateReports):
    def __init__(self, is_staff, request, validated_data):
        super().__init__(is_staff, request, validated_data)
        self.reports_type = 'days'
        self.Report = namedtuple(
            'Report',
            ['date', 'signups', 'first_deposits_count', 'first_deposits_amount',
             'deposits_count', 'deposits_amount', 'net_gaming', 'profit', 'clicks', 'unique_clicks']
        )

    def set_reports_to_redis(self, reports, key):
        for report in reports:
            self.redis_client.rpush(
                key,
                DATA_DELIMITER.join([
                    report.date.strftime(DATE_FORMAT),
                    str(report.signups),
                    str(report.first_deposits_count),
                    str(report.first_deposits_amount),
                    str(report.deposits_count),
                    str(report.deposits_amount),
                    str(report.net_gaming),
                    str(report.profit),
                    str(report.clicks),
                    str(report.unique_clicks),
                ])
            )
        self.redis_client.expire(key, TTL_HOUR)

    def get_reports_from_redis(self, key):
        reports = []

        redis_values = self.redis_client.lrange(key, 0, -1)
        for value in redis_values:
            day, signups, first_deposits_count, first_deposits_amount, \
            deposits_count, deposits_amount, net_gaming, profit, \
            clicks, unique_clicks = value.decode('unicode_escape').split(DATA_DELIMITER)

            reports.append(
                    self.Report(
                        date=datetime.strptime(day, DATE_FORMAT).date(),
                        signups=int(signups),
                        first_deposits_count=int(first_deposits_count),
                        first_deposits_amount=Decimal(first_deposits_amount),
                        deposits_count=int(deposits_count),
                        deposits_amount=Decimal(deposits_amount),
                        net_gaming=Decimal(net_gaming),
                        profit=Decimal(profit),
                        clicks=int(clicks),
                        unique_clicks=int(unique_clicks)
                    )
                )
        return reports

    def get_reports_from_postgres_clickhouse(self, date_gte, date_lte, redis_key):
        datetime_gte = datetime.combine(date_gte, datetime.min.time(), tzinfo=pytz.utc)
        datetime_lte = datetime.combine(date_lte, datetime.max.time(), tzinfo=pytz.utc)

        days = list(dayrange(date_gte, date_lte))

        players = Player.objects.filter(
            promo__in=self.promos,
            created_at__gte=datetime_gte, created_at__lte=datetime_lte
        )
        players_aggregated = players.annotate(
            date=TruncDay('created_at')
        ).values('date').annotate(
            count=Count('id'),
        )

        deposits = Payment.objects.filter(
            player__promo__in=self.promos, status=PaymentStatus.SUCCESS, direction=PaymentDirection.INCOMING,
            created_at__gte=datetime_gte, created_at__lte=datetime_lte
        )
        deposits_aggregated = deposits.annotate(
            date=TruncDay('created_at')
        ).values('date').annotate(
            count=Count('id'),
            amount_eur=Sum('amount_eur')
        )

        first_deposits = deposits.filter(is_first=True)
        first_deposits_aggregated = first_deposits.annotate(
            date=TruncDay('created_at')
        ).values('date').annotate(
            count=Count('id'),
            amount_eur=Sum('amount_eur')
        )

        bets = Bet.objects.filter(
            player__promo__in=self.promos, is_freebet=False,
            status=BetStatus.ACCEPTED, state__in=(BetState.WIN, BetState.LOSE),
            created_at__gte=datetime_gte, created_at__lte=datetime_lte
        )
        bets_aggregated = bets.annotate(
            date=TruncDay('created_at')
        ).values('date').annotate(
            income_eur=Sum('income_eur'),
            outcome_eur=Sum('outcome_eur'),
            net_gaming=Sum('net_gaming'),
            profit=Sum('profit')
        )

        traffic = self.traffic_client.get_days_traffic(
            days=days,
            actions=(TrafficActionType.CLICK,)
        )

        reports = []
        for day in days:
            date_players = players_aggregated.filter(date=day)
            date_players = list(date_players)[0] if date_players else {'count': 0}

            date_first_deposits = first_deposits_aggregated.filter(date=day)
            date_first_deposits = list(date_first_deposits)[0] if date_first_deposits \
                else {'count': 0, 'amount_eur': Decimal(0)}

            date_deposits = deposits_aggregated.filter(date=day)
            date_deposits = list(date_deposits)[0] if date_deposits \
                else {'count': 0, 'amount_eur': Decimal(0)}

            date_bets = bets_aggregated.filter(date=day)
            date_bets = list(date_bets)[0] if date_bets \
                else {'income_eur': Decimal(0), 'outcome_eur': Decimal(0),
                      'net_gaming': Decimal(0), 'profit': Decimal(0)}

            date_traffic = {'all': 0, 'unique': 0}
            for traffic_key in traffic:
                if day.strftime(DATE_FORMAT) in traffic_key:
                    date_traffic['all'] += traffic[traffic_key]['all']
                    date_traffic['unique'] += traffic[traffic_key]['unique']

            reports.append(
                self.Report(
                    date=day.date(),
                    signups=date_players['count'],
                    first_deposits_count=date_first_deposits['count'],
                    first_deposits_amount=date_first_deposits['amount_eur'],
                    deposits_count=date_deposits['count'],
                    deposits_amount=date_deposits['amount_eur'],
                    net_gaming=date_bets['net_gaming'],
                    profit=date_bets['profit'],
                    clicks=date_traffic['all'],
                    unique_clicks=date_traffic['unique']
                )
            )

        self.set_reports_to_redis(reports, redis_key)
        return reports


class CreateMonthsReports(CreateReports):
    def __init__(self, is_staff, request, validated_data):
        super().__init__(is_staff, request, validated_data)
        self.reports_type = 'months'
        self.Report = namedtuple(
            'Report',
            ['month', 'signups', 'first_deposits_count', 'first_deposits_amount',
             'deposits_count', 'deposits_amount', 'net_gaming', 'profit', 'clicks', 'unique_clicks']
        )

    def set_reports_to_redis(self, reports, key):
        for report in reports:
            self.redis_client.rpush(
                key,
                DATA_DELIMITER.join([
                    report.month.strftime(MONTH_FORMAT),
                    str(report.signups),
                    str(report.first_deposits_count),
                    str(report.first_deposits_amount),
                    str(report.deposits_count),
                    str(report.deposits_amount),
                    str(report.net_gaming),
                    str(report.profit),
                    str(report.clicks),
                    str(report.unique_clicks),
                ])
            )
        self.redis_client.expire(key, TTL_HOUR)

    def get_reports_from_redis(self, key):
        reports = []

        redis_values = self.redis_client.lrange(key, 0, -1)
        for value in redis_values:
            month, signups, first_deposits_count, first_deposits_amount, \
            deposits_count, deposits_amount, net_gaming, profit, \
            clicks, unique_clicks = value.decode('unicode_escape').split(DATA_DELIMITER)

            reports.append(
                    self.Report(
                        month=datetime.strptime(month, MONTH_FORMAT).date(),
                        signups=int(signups),
                        first_deposits_count=int(first_deposits_count),
                        first_deposits_amount=Decimal(first_deposits_amount),
                        deposits_count=int(deposits_count),
                        deposits_amount=Decimal(deposits_amount),
                        net_gaming=Decimal(net_gaming),
                        profit=Decimal(profit),
                        clicks=int(clicks),
                        unique_clicks=int(unique_clicks)
                    )
                )
        return reports

    def get_reports_from_postgres_clickhouse(self, date_gte, date_lte, redis_key):
        datetime_gte = datetime.combine(date_gte, datetime.min.time(), tzinfo=pytz.utc)
        datetime_lte = datetime.combine(date_lte, datetime.max.time(), tzinfo=pytz.utc)

        months = list(monthrange(date_gte, date_lte))

        players = Player.objects.filter(
            promo__in=self.promos,
            created_at__gte=datetime_gte, created_at__lte=datetime_lte
        )
        players_aggregated = players.annotate(
            month=TruncMonth('created_at')
        ).values('month').annotate(
            count=Count('id'),
        )

        deposits = Payment.objects.filter(
            player__promo__in=self.promos, status=PaymentStatus.SUCCESS, direction=PaymentDirection.INCOMING,
            created_at__gte=datetime_gte, created_at__lte=datetime_lte
        )
        deposits_aggregated = deposits.annotate(
            month=TruncMonth('created_at')
        ).values('month').annotate(
            count=Count('id'),
            amount_eur=Sum('amount_eur')
        )

        first_deposits = deposits.filter(is_first=True)
        first_deposits_aggregated = first_deposits.annotate(
            month=TruncMonth('created_at')
        ).values('month').annotate(
            count=Count('id'),
            amount_eur=Sum('amount_eur')
        )

        bets = Bet.objects.filter(
            player__promo__in=self.promos, is_freebet=False,
            status=BetStatus.ACCEPTED, state__in=(BetState.WIN, BetState.LOSE),
            created_at__gte=datetime_gte, created_at__lte=datetime_lte
        )
        bets_aggregated = bets.annotate(
            month=TruncMonth('created_at')
        ).values('month').annotate(
            income_eur=Sum('income_eur'),
            outcome_eur=Sum('outcome_eur'),
            net_gaming=Sum('net_gaming'),
            profit=Sum('profit')
        )

        traffic = self.traffic_client.get_months_traffic(
            months=months,
            actions=(TrafficActionType.CLICK,)
        )

        reports = []
        for month in months:
            month_players = players_aggregated.filter(month=month)
            month_players = list(month_players)[0] if month_players else {'count': 0}

            month_first_deposits = first_deposits_aggregated.filter(month=month)
            month_first_deposits = list(month_first_deposits)[0] if month_first_deposits \
                else {'count': 0, 'amount_eur': Decimal(0)}

            month_deposits = deposits_aggregated.filter(month=month)
            month_deposits = list(month_deposits)[0] if month_deposits \
                else {'count': 0, 'amount_eur': Decimal(0)}

            month_bets = bets_aggregated.filter(month=month)
            month_bets = list(month_bets)[0] if month_bets \
                else {'income_eur': Decimal(0), 'outcome_eur': Decimal(0),
                      'net_gaming': Decimal(0), 'profit': Decimal(0)}

            month_traffic = {'all': 0, 'unique': 0}
            for traffic_key in traffic:
                if month.strftime(MONTH_FORMAT) in traffic_key:
                    month_traffic['all'] += traffic[traffic_key]['all']
                    month_traffic['unique'] += traffic[traffic_key]['unique']

            reports.append(
                self.Report(
                    month=month.date(),
                    signups=month_players['count'],
                    first_deposits_count=month_first_deposits['count'],
                    first_deposits_amount=month_first_deposits['amount_eur'],
                    deposits_count=month_deposits['count'],
                    deposits_amount=month_deposits['amount_eur'],
                    net_gaming=month_bets['net_gaming'],
                    profit=month_bets['profit'],
                    clicks=month_traffic['all'],
                    unique_clicks=month_traffic['unique']
                )
            )

        self.set_reports_to_redis(reports, redis_key)
        return reports


class CreatePromosReports(CreateReports):
    def __init__(self, is_staff, request, validated_data):
        super().__init__(is_staff, request, validated_data)
        self.reports_type = 'promos'
        self.Report = namedtuple(
            'Report',
            ['promo', 'signups', 'first_deposits_count', 'first_deposits_amount',
             'deposits_count', 'deposits_amount', 'net_gaming', 'profit', 'clicks', 'unique_clicks']
        )

    def set_reports_to_redis(self, reports, key):
        for report in reports:
            self.redis_client.rpush(
                key,
                DATA_DELIMITER.join([
                    report.promo.uuid,
                    str(report.signups),
                    str(report.first_deposits_count),
                    str(report.first_deposits_amount),
                    str(report.deposits_count),
                    str(report.deposits_amount),
                    str(report.net_gaming),
                    str(report.profit),
                    str(report.clicks),
                    str(report.unique_clicks),
                ])
            )
        self.redis_client.expire(key, TTL_HOUR)

    def get_reports_from_redis(self, key):
        reports = []

        redis_values = self.redis_client.lrange(key, 0, -1)
        for value in redis_values:
            promo_uuid, signups, first_deposits_count, first_deposits_amount, \
            deposits_count, deposits_amount, net_gaming, profit, \
            clicks, unique_clicks = value.decode('unicode_escape').split(DATA_DELIMITER)

            reports.append(
                self.Report(
                    promo=Promo.objects.get(pk=promo_uuid),
                    signups=int(signups),
                    first_deposits_count=int(first_deposits_count),
                    first_deposits_amount=Decimal(first_deposits_amount),
                    deposits_count=int(deposits_count),
                    deposits_amount=Decimal(deposits_amount),
                    net_gaming=Decimal(net_gaming),
                    profit=Decimal(profit),
                    clicks=int(clicks),
                    unique_clicks=int(unique_clicks)
                )
            )
        return reports

    def get_reports_from_postgres_clickhouse(self, date_gte, date_lte, redis_key):
        datetime_gte = datetime.combine(date_gte, datetime.min.time(), tzinfo=pytz.utc)
        datetime_lte = datetime.combine(date_lte, datetime.max.time(), tzinfo=pytz.utc)

        players = Player.objects.filter(
            promo__in=self.promos,
            created_at__gte=datetime_gte, created_at__lte=datetime_lte
        )
        players_aggregated = players.values('promo').annotate(
            count=Count('id'),
        )

        deposits = Payment.objects.filter(
            player__promo__in=self.promos, status=PaymentStatus.SUCCESS, direction=PaymentDirection.INCOMING,
            created_at__gte=datetime_gte, created_at__lte=datetime_lte
        )
        deposits_aggregated = deposits.values('player__promo').annotate(
            count=Count('id'),
            amount_eur=Sum('amount_eur')
        )

        first_deposits = deposits.filter(is_first=True)
        first_deposits_aggregated = first_deposits.values('player__promo').annotate(
            count=Count('id'),
            amount_eur=Sum('amount_eur')
        )

        bets = Bet.objects.filter(
            player__promo__in=self.promos, is_freebet=False,
            status=BetStatus.ACCEPTED, state__in=(BetState.WIN, BetState.LOSE),
            created_at__gte=datetime_gte, created_at__lte=datetime_lte
        )
        bets_aggregated = bets.values('player__promo').annotate(
            income_eur=Sum('income_eur'),
            outcome_eur=Sum('outcome_eur'),
            net_gaming=Sum('net_gaming'),
            profit=Sum('profit')
        )

        traffic = self.traffic_client.get_promos_traffic(
            datetime_gte=datetime_gte,
            datetime_lte=datetime_lte,
            actions=(TrafficActionType.CLICK,)
        )

        reports = []
        for promo in self.promos:
            promo_players = players_aggregated.filter(promo=promo)
            promo_players = list(promo_players)[0] if promo_players else {'count': 0}

            promo_first_deposits = first_deposits_aggregated.filter(player__promo=promo)
            promo_first_deposits = list(promo_first_deposits)[0] if promo_first_deposits \
                else {'count': 0, 'amount_eur': Decimal(0)}

            promo_deposits = deposits_aggregated.filter(player__promo=promo)
            promo_deposits = list(promo_deposits)[0] if promo_deposits \
                else {'count': 0, 'amount_eur': Decimal(0)}

            promo_bets = bets_aggregated.filter(player__promo=promo)
            promo_bets = list(promo_bets)[0] if promo_bets \
                else {'income_eur': Decimal(0), 'outcome_eur': Decimal(0),
                      'net_gaming': Decimal(0), 'profit': Decimal(0)}

            promo_traffic = {'all': 0, 'unique': 0}
            for traffic_key in traffic:
                if promo.uuid in traffic_key:
                    promo_traffic['all'] += traffic[traffic_key]['all']
                    promo_traffic['unique'] += traffic[traffic_key]['unique']

            reports.append(
                self.Report(
                    promo=promo,
                    signups=promo_players['count'],
                    first_deposits_count=promo_first_deposits['count'],
                    first_deposits_amount=promo_first_deposits['amount_eur'],
                    deposits_count=promo_deposits['count'],
                    deposits_amount=promo_deposits['amount_eur'],
                    net_gaming=promo_bets['net_gaming'],
                    profit=promo_bets['profit'],
                    clicks=promo_traffic['all'],
                    unique_clicks=promo_traffic['unique']
                )
            )

        self.set_reports_to_redis(reports, redis_key)
        return reports


class CreateDashboard:
    def __init__(self, is_staff, request):
        self.is_staff = is_staff
        self.user = request.user

    def get_promos(self):
        promos = Promo.objects.none()

        if self.is_staff is True:
            promos = Promo.objects.all()

        if self.is_staff is False:
            promos = Promo.objects.filter(partner__user=self.user)

        return promos

    def run(self):
        now = datetime_now()
        today = now.date()
        gte = datetime.combine(today.replace(day=1), datetime.min.time(), tzinfo=pytz.utc)
        promos = self.get_promos()

        players = Player.objects.filter(
            promo__in=promos,
            created_at__gte=gte
        )
        players_aggregated = players.aggregate(signups=Count('id'))
        deposits = Payment.objects.filter(
            player__promo__in=promos, status=PaymentStatus.SUCCESS, direction=PaymentDirection.INCOMING,
            created_at__gte=gte
        )
        deposits_aggregated = deposits.aggregate(amount=Sum('amount_eur'))
        bets = Bet.objects.filter(
            player__promo__in=promos, is_freebet=False,
            status=BetStatus.ACCEPTED, state__in=(BetState.WIN, BetState.LOSE),
            created_at__gte=gte
        )
        bets_aggregated = bets.aggregate(net_gaming=Sum('net_gaming'), profit=Sum('profit'))

        dashboard = namedtuple('dashboard', ['signups', 'deposits_amount', 'net_gaming', 'profit'])(
            signups=players_aggregated['signups'],
            deposits_amount=deposits_aggregated['amount'] or Decimal(0),
            net_gaming=bets_aggregated['net_gaming'] or Decimal(0),
            profit=bets_aggregated['profit'] or Decimal(0)
        )

        return dashboard
