from collections import OrderedDict, namedtuple

from rest_framework.response import Response

from api_public.pagination import AffiliatePagination
from reports.rest.serializers import ReportTotalsSerializer


class ReportTotalsPegination(AffiliatePagination):
    def paginate_queryset(self, reports, request, view=None):
        totals_instance = namedtuple(
            'totals_instance',
            ['signups', 'first_deposits_count', 'first_deposits_amount', 'deposits_count', 'deposits_amount',
             'net_gaming', 'profit', 'clicks', 'unique_clicks']
        )(
            signups=sum(r.signups for r in reports),
            first_deposits_count=sum(r.first_deposits_count for r in reports),
            first_deposits_amount=sum(r.first_deposits_amount for r in reports),
            deposits_count=sum(r.deposits_count for r in reports),
            deposits_amount=sum(r.deposits_amount for r in reports),
            net_gaming=sum(r.net_gaming for r in reports),
            profit=sum(r.profit for r in reports),
            clicks=sum(r.clicks for r in reports),
            unique_clicks=sum(r.unique_clicks for r in reports),
        )
        self.totals = ReportTotalsSerializer(instance=totals_instance).data
        self.count = len(reports)
        self.limit = self.get_limit(request)
        if self.limit is None:
            return None

        self.offset = self.get_offset(request)
        self.request = request

        if self.count == 0 or self.offset > self.count:
            return []
        return reports[self.offset:self.offset + self.limit]

    def get_paginated_response(self, data):
        return Response(OrderedDict([
            ('count', self.count),
            ('results', data),
            ('totals', self.totals)
        ]))
