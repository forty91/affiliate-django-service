from drf_yasg.utils import swagger_auto_schema
from rest_framework.filters import OrderingFilter
from django_filters import rest_framework as filters
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK

from api_public.permissions import IsPartner, staff_permission
from base.filters import CustomObjectsOrderingFilter
from base.views import AffiliateAPIView
from permissions.consts import Block, Action
from reports.bl import CreateDaysReports, CreateMonthsReports, CreatePromosReports, CreateDashboard
from reports.rest.pagination import ReportTotalsPegination
from reports.rest.serializers import ReportParamsSerilizer, ReportDaySerializer, ReportMonthSerializer, \
    ReportPromoSerializer, DashboardSerializer


class ReportDaysView(AffiliateAPIView):
    """
    Список данных из Player, Bet, Payment аггрерированный по дням.
    """
    permission_classes = (IsPartner, staff_permission(Block.REPORTS, Action.READ))
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter)

    ordering_fields = (
        'date', 'signups', 'first_deposits_count', 'first_deposits_amount',
        'deposits_count', 'deposits_amount', 'net_gaming', 'profit', 'clicks', 'unique_clicks'
    )
    ordering = ('-date', )

    @swagger_auto_schema(query_serializer=ReportParamsSerilizer,
                         responses={200: ReportDaySerializer})
    def get(self, request):
        serializer = ReportParamsSerilizer(data=request.query_params)
        serializer.is_valid(raise_exception=True)

        reports = CreateDaysReports(self.is_staff, request, serializer.validated_data).run()
        reports = CustomObjectsOrderingFilter().filter(request, reports, self)

        paginator = ReportTotalsPegination()
        page = paginator.paginate_queryset(reports, self.request, view=self)
        serializer = ReportDaySerializer(page, many=True)

        return paginator.get_paginated_response(serializer.data)


class ReportMonthsView(AffiliateAPIView):
    """
    Список данных из Player, Bet, Payment аггрерированный по месяцам.
    """
    permission_classes = (IsPartner, staff_permission(Block.REPORTS, Action.READ))
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter)

    ordering_fields = (
        'month', 'signups', 'first_deposits_count', 'first_deposits_amount',
        'deposits_count', 'deposits_amount', 'net_gaming', 'profit', 'clicks', 'unique_clicks'
    )
    ordering = ('-month', )

    @swagger_auto_schema(query_serializer=ReportParamsSerilizer,
                         responses={200: ReportMonthSerializer})
    def get(self, request):
        serializer = ReportParamsSerilizer(data=request.query_params)
        serializer.is_valid(raise_exception=True)

        reports = CreateMonthsReports(self.is_staff, request, serializer.validated_data).run()
        reports = CustomObjectsOrderingFilter().filter(request, reports, self)

        paginator = ReportTotalsPegination()
        page = paginator.paginate_queryset(reports, self.request, view=self)
        serializer = ReportMonthSerializer(page, many=True)

        return paginator.get_paginated_response(serializer.data)


class ReportPromosView(AffiliateAPIView):
    """
    Список данных из Player, Bet, Payment аггрерированный по promo.
    """
    permission_classes = (IsPartner, staff_permission(Block.REPORTS, Action.READ))
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter)

    ordering_fields = (
        'promo.name', 'signups', 'first_deposits_count', 'first_deposits_amount',
        'deposits_count', 'deposits_amount', 'net_gaming', 'profit', 'clicks', 'unique_clicks'
    )
    ordering = ('promo.name', )

    @swagger_auto_schema(query_serializer=ReportParamsSerilizer,
                         responses={200: ReportPromoSerializer})
    def get(self, request):
        serializer = ReportParamsSerilizer(data=request.query_params)
        serializer.is_valid(raise_exception=True)

        reports = CreatePromosReports(self.is_staff, request, serializer.validated_data).run()
        reports = CustomObjectsOrderingFilter().filter(request, reports, self)

        paginator = ReportTotalsPegination()
        page = paginator.paginate_queryset(reports, self.request, view=self)
        serializer = ReportPromoSerializer(page, many=True)

        return paginator.get_paginated_response(serializer.data)


class DashboardView(AffiliateAPIView):
    """
    Endpoint for dashboard.
    """
    permission_classes = (IsPartner, staff_permission(Block.REPORTS, Action.READ))

    @swagger_auto_schema(responses={HTTP_200_OK: DashboardSerializer})
    def get(self, request):
        dashboard = CreateDashboard(self.is_staff, request).run()
        return Response(data=DashboardSerializer(instance=dashboard).data, status=HTTP_200_OK)
