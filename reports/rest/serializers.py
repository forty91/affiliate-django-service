from datetime import datetime

from rest_framework import serializers

from base.consts import DATE_FORMAT
from base.help_texts import REPORT_DATE, REPORT_SIGNUPS, REPORT_FIRST_DEPOSITS_COUNT, REPORT_FIRST_DEPOSITS_AMOUNT, \
    REPORT_NET_GAMING, REPORT_PROFIT, REPORT_CLICKS, REPORT_UNIQUE_CLICKS, REPORT_MONTH, REPORT_PROMO_NAME, \
    REPORT_DEPOSITS_COUNT, REPORT_DEPOSITS_AMOUNT, PROMO_EXACT, DATE_LTE, DATE_GTE, DASHBOARD_SIGNUPS, \
    DASHBOARD_DEPOSITS_AMOUNT, DASHBOARD_NET_GAMING, DASHBOARD_PROFIT
from base.utils.datetime import datetime_now
from promos.models import Promo
from reports.utils import datetime_to_month_repr, datetime_to_date_repr


class ReportTotalsSerializer(serializers.Serializer):
    signups = serializers.IntegerField(help_text=REPORT_SIGNUPS)
    first_deposits_count = serializers.IntegerField(help_text=REPORT_FIRST_DEPOSITS_COUNT)
    first_deposits_amount = serializers.DecimalField(max_digits=9, decimal_places=2,
                                                     help_text=REPORT_FIRST_DEPOSITS_AMOUNT)
    deposits_count = serializers.IntegerField(help_text=REPORT_DEPOSITS_COUNT)
    deposits_amount = serializers.DecimalField(max_digits=9, decimal_places=2, help_text=REPORT_DEPOSITS_AMOUNT)
    net_gaming = serializers.DecimalField(max_digits=9, decimal_places=2, help_text=REPORT_NET_GAMING)
    profit = serializers.DecimalField(max_digits=9, decimal_places=2, help_text=REPORT_PROFIT)
    clicks = serializers.IntegerField(help_text=REPORT_CLICKS)
    unique_clicks = serializers.IntegerField(help_text=REPORT_UNIQUE_CLICKS)


class ReportDaySerializer(serializers.Serializer):
    date = serializers.SerializerMethodField(help_text=REPORT_DATE)
    signups = serializers.IntegerField(help_text=REPORT_SIGNUPS)
    first_deposits_count = serializers.IntegerField(help_text=REPORT_FIRST_DEPOSITS_COUNT)
    first_deposits_amount = serializers.DecimalField(max_digits=9, decimal_places=2,
                                                     help_text=REPORT_FIRST_DEPOSITS_AMOUNT)
    deposits_count = serializers.IntegerField(help_text=REPORT_DEPOSITS_COUNT)
    deposits_amount = serializers.DecimalField(max_digits=9, decimal_places=2, help_text=REPORT_DEPOSITS_AMOUNT)
    net_gaming = serializers.DecimalField(max_digits=9, decimal_places=2, help_text=REPORT_NET_GAMING)
    profit = serializers.DecimalField(max_digits=9, decimal_places=2, help_text=REPORT_PROFIT)
    clicks = serializers.IntegerField(help_text=REPORT_CLICKS)
    unique_clicks = serializers.IntegerField(help_text=REPORT_UNIQUE_CLICKS)

    def get_date(self, obj):
        return datetime_to_date_repr(obj.date)


class ReportMonthSerializer(serializers.Serializer):
    month = serializers.SerializerMethodField(help_text=REPORT_MONTH)
    signups = serializers.IntegerField(help_text=REPORT_SIGNUPS)
    first_deposits_count = serializers.IntegerField(help_text=REPORT_FIRST_DEPOSITS_COUNT)
    first_deposits_amount = serializers.DecimalField(max_digits=9, decimal_places=2,
                                                     help_text=REPORT_FIRST_DEPOSITS_AMOUNT)
    deposits_count = serializers.IntegerField(help_text=REPORT_DEPOSITS_COUNT)
    deposits_amount = serializers.DecimalField(max_digits=9, decimal_places=2, help_text=REPORT_DEPOSITS_AMOUNT)
    net_gaming = serializers.DecimalField(max_digits=9, decimal_places=2, help_text=REPORT_NET_GAMING)
    profit = serializers.DecimalField(max_digits=9, decimal_places=2, help_text=REPORT_PROFIT)
    clicks = serializers.IntegerField(help_text=REPORT_CLICKS)
    unique_clicks = serializers.IntegerField(help_text=REPORT_UNIQUE_CLICKS)

    def get_month(self, obj):
        return datetime_to_month_repr(obj.month)


class ReportPromoSerializer(serializers.Serializer):
    promo = serializers.SerializerMethodField(help_text=REPORT_PROMO_NAME)
    signups = serializers.IntegerField(help_text=REPORT_SIGNUPS)
    first_deposits_count = serializers.IntegerField(help_text=REPORT_FIRST_DEPOSITS_COUNT)
    first_deposits_amount = serializers.DecimalField(max_digits=9, decimal_places=2,
                                                     help_text=REPORT_FIRST_DEPOSITS_AMOUNT)
    deposits_count = serializers.IntegerField(help_text=REPORT_DEPOSITS_COUNT)
    deposits_amount = serializers.DecimalField(max_digits=9, decimal_places=2, help_text=REPORT_DEPOSITS_AMOUNT)
    net_gaming = serializers.DecimalField(max_digits=9, decimal_places=2, help_text=REPORT_NET_GAMING)
    profit = serializers.DecimalField(max_digits=9, decimal_places=2, help_text=REPORT_PROFIT)
    clicks = serializers.IntegerField(help_text=REPORT_CLICKS)
    unique_clicks = serializers.IntegerField(help_text=REPORT_UNIQUE_CLICKS)

    def get_promo(self, obj):
        return obj.promo.name


class ReportParamsSerilizer(serializers.Serializer):
    date__lte = serializers.DateField(format=DATE_FORMAT, required=True, help_text=DATE_LTE)
    date__gte = serializers.DateField(format=DATE_FORMAT, required=True, help_text=DATE_GTE)
    promo = serializers.PrimaryKeyRelatedField(queryset=Promo.objects.all(), required=False, help_text=PROMO_EXACT)

    def validate(self, attrs):
        date__gte = attrs['date__gte']
        date__lte = attrs['date__lte']

        if date__gte > date__lte:
            msg = 'Parameter date__gte should be lower or equal date__lte.'
            raise serializers.ValidationError(msg)

        today = datetime_now().date()
        if date__gte > today:
            msg = 'Parameter date__gte should be lower or equal today.'
            raise serializers.ValidationError(msg)

        if date__lte > today:
            msg = 'Parameter date__lte should be lower or equal today.'
            raise serializers.ValidationError(msg)

        return attrs


class DashboardSerializer(serializers.Serializer):
    signups = serializers.IntegerField(help_text=DASHBOARD_SIGNUPS)
    deposits_amount = serializers.DecimalField(max_digits=9, decimal_places=2, help_text=DASHBOARD_DEPOSITS_AMOUNT)
    net_gaming = serializers.DecimalField(max_digits=9, decimal_places=2, help_text=DASHBOARD_NET_GAMING)
    profit = serializers.DecimalField(max_digits=9, decimal_places=2, help_text=DASHBOARD_PROFIT)
