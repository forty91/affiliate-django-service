from base.utils.enumerate import BaseEnumerate

REPORT_COLLECTION = 'reports'


class ReportAction(BaseEnumerate):
    CREATE = 'create'
