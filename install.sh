#!/bin/sh

############################################################################################
#
#           Скрипт собственно производит установку всех зависимостей
#                       для запуска приложения в контейнере
#
############################################################################################

set -ex

# Install system dependencies
#
apk update

apk add --no-cache \
    python3 \
    python3-dev \
    curl \
    ca-certificates \
    zlib \
    tzdata \
    openssl \
    libstdc++ \
    postgresql-libs

# Install system dependencies, del it
#
apk add --no-cache --virtual .build-deps \
    alpine-sdk \
    gcc \
    musl-dev \
    make \
    zlib-dev \
    curl-dev \
    postgresql-dev

# Create symlinks
#
cd /usr/bin && \
    ln -sf easy_install-3.6 easy_install && \
    ln -sf idle3.6 idle && \
    ln -sf pydoc3.6 pydoc && \
    ln -sf python3.6 python && \
    ln -sf python3.6-config python-config && \
    ln -sf pip3.6 pip


# Install python dependencies
#
pip install --no-cache --upgrade pip setuptools && \
    pip install --no-cache -r /app/requirements.txt

# Update certificates
#
update-ca-certificates

# Cleanup
#
apk del .build-deps; \
    rm -rf /var/cache/apk/* && \
    rm -rf /root/.cache/* && \
    rm -rf ~/.cache/*
