import os

from base.utils.enumerate import BaseEnumerate

REPORT_COLLECTION = 'reports'


class ReportAction(BaseEnumerate):
    CREATE = 'create'


DATA_DELIMITER = '<=>'
TABLE_NAME_PREFIX = 'partner_traffic_'
MERGE_TABLE_NAME = "merge(currentDatabase(), '^{prefix}*')".format(prefix=TABLE_NAME_PREFIX)

QUERIES_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../traffic/queries')