import os

from base.utils.template import render_template
from traffic.consts import QUERIES_DIR, TABLE_NAME_PREFIX


def get_partner_table_name(partner):
    return '{prefix}{partner_uuid}'.format(prefix=TABLE_NAME_PREFIX, partner_uuid=str(partner.id).replace('-', '_'))


def create_partner_table_if_not_exists(clickhouse_client, partner):
    table_name = get_partner_table_name(partner)
    create_table_request = render_template(
        os.path.join(QUERIES_DIR, 'create_table.sql'),
        table_name=get_partner_table_name(partner)
    )
    clickhouse_client.execute(create_table_request)
    return table_name
