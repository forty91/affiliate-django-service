SELECT promo_id, all_count, unique_count, action
FROM(
  SELECT PromoId AS promo_id, count(UserToken) as all_count, uniq(UserToken) as unique_count,
  Action as action
  FROM {{ table_name }}
  WHERE Action IN ({{ formatted_actions }})
  AND PromoId IN ({{ formatted_promos }})
  AND CreatedAt >= toDateTime('{{ datetime_gte }}')
  AND CreatedAt <= toDateTime('{{ datetime_lte }}')
  GROUP BY promo_id, action
)
