SELECT promo_id, all_count, unique_count, date, action
FROM(
  SELECT PromoId AS promo_id, count(UserToken) as all_count, uniq(UserToken) as unique_count,
  toDate(CreatedAt) as date, Action as action
  FROM {{ table_name }}
  WHERE Action IN ({{ formatted_actions }})
  AND PromoId IN ({{ formatted_promos }})
  AND toStartOfDay(CreatedAt) IN ({{ formatted_days }})
  GROUP BY promo_id, date, action
)