CREATE TABLE IF NOT EXISTS {{table_name}}
(
    PromoId UUID,
    LandingId Nullable(UUID),
    Action Enum8('download' = 1, 'click' = 2),
    UserIp String,
    UserAgent String,
    UserToken UUID,
    CreatedAt DateTime
) ENGINE = MergeTree()
ORDER BY (CreatedAt, PromoId, UserToken)