SELECT promo_id, all_count, unique_count, month, action
FROM(
  SELECT PromoId AS promo_id, count(UserToken) as all_count, uniq(UserToken) as unique_count,
  toStartOfMonth(CreatedAt) as month, Action as action
  FROM {{ table_name }}
  WHERE Action IN ({{ formatted_actions }})
  AND PromoId IN ({{ formatted_promos }})
  AND toStartOfMonth(CreatedAt) IN ({{ formatted_months }})
  GROUP BY promo_id, month, action
)