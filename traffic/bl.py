import logging
import os

from clickhouse_driver.errors import Error
from rest_framework.status import HTTP_503_SERVICE_UNAVAILABLE

from base.consts import DATETIME_FORMAT, DATE_FORMAT, MONTH_FORMAT
from base.exceptions import affiliate_api_exception
from base.utils.clickhouse import ClickhouseClient
from base.utils.template import render_template
from traffic.consts import QUERIES_DIR, MERGE_TABLE_NAME, DATA_DELIMITER
from traffic.utils import create_partner_table_if_not_exists
from users.models import Partner


class GetTraffic:
    def __init__(self, user, promos, clickhouse_client=None, is_staff=False):
        self.promos = promos
        self.clickhouse_client = clickhouse_client or ClickhouseClient()

        if is_staff is True:
            self.table_name = MERGE_TABLE_NAME
        else:
            partner = Partner.objects.get(user=user)
            self.table_name = create_partner_table_if_not_exists(self.clickhouse_client, partner)

    def execute_query(self, query):
        while True:
            try:
                response = self.clickhouse_client.execute(query)
                break
            # TODO: райзится в BufferedSocketReader при большом больших выборках по дням,
            # разобраться в кишках clickhouse_driver и пофиксить грязный хак c while
            except EOFError as e:
                logging.error('Clickhouse error: Too much requests per second.')
                clickhouse_client = ClickhouseClient()
                continue
            except Error as e:
                logging.error('Clickhouse error:\n%s', str(e))
                raise affiliate_api_exception(
                    status=HTTP_503_SERVICE_UNAVAILABLE,
                    detail='Traffic service unavailable for a while.',
                    code='service unavailable'
                )
        return response

    def get_days_traffic(self, days=tuple(), actions=tuple()):
        if not self.promos:
            return {}

        formatted_days = ','.join(["toDateTime('%s')" % day.strftime(DATETIME_FORMAT) for day in days])
        formatted_promos = ','.join("'%s'" % promo.pk for promo in self.promos)
        formatted_actions = ','.join("'%s'" % action for action in actions)
        query = render_template(
            template_path=os.path.join(QUERIES_DIR, 'select_days_traffic.sql'),
            table_name=self.table_name,
            formatted_days=formatted_days,
            formatted_promos=formatted_promos,
            formatted_actions=formatted_actions,
        )

        response = self.execute_query(query)

        result = {}
        for promo_id, all_count, unique_count, date, action in response:
            key = DATA_DELIMITER.join([str(promo_id), date.strftime(DATE_FORMAT), action])
            result[key] = {'all': all_count, 'unique': unique_count}

        return result

    def get_months_traffic(self, months=tuple(), actions=tuple()):
        if not self.promos:
            return {}

        formatted_months = ','.join(["toDate('%s')" % month.strftime(DATE_FORMAT) for month in months])
        formatted_promos = ','.join("'%s'" % promo.pk for promo in self.promos)
        formatted_actions = ','.join("'%s'" % action for action in actions)
        query = render_template(
            template_path=os.path.join(QUERIES_DIR, 'select_months_traffic.sql'),
            table_name=self.table_name,
            formatted_months=formatted_months,
            formatted_promos=formatted_promos,
            formatted_actions=formatted_actions,
        )

        response = self.execute_query(query)

        result = {}
        for promo_id, all_count, unique_count, month, action in response:
            key = DATA_DELIMITER.join([str(promo_id), month.strftime(MONTH_FORMAT), action])
            result[key] = {'all': all_count, 'unique': unique_count}

        return result

    def get_promos_traffic(self, datetime_gte, datetime_lte, actions=tuple()):
        if not self.promos:
            return {}

        formatted_promos = ','.join("'%s'" % promo.pk for promo in self.promos)
        formatted_actions = ','.join("'%s'" % action for action in actions)
        query = render_template(
            template_path=os.path.join(QUERIES_DIR, 'select_promos_traffic.sql'),
            table_name=self.table_name,
            datetime_gte=datetime_gte.strftime(DATETIME_FORMAT),
            datetime_lte=datetime_lte.strftime(DATETIME_FORMAT),
            formatted_promos=formatted_promos,
            formatted_actions=formatted_actions,
        )

        response = self.execute_query(query)

        result = {}
        for promo_id, all_count, unique_count, action in response:
            key = DATA_DELIMITER.join([str(promo_id), action])
            result[key] = {'all': all_count, 'unique': unique_count}

        return result
