from base.utils.enumerate import BaseEnumerate


class Currency(BaseEnumerate):
    EUR = 'eur'
    USD = 'usd'
    GBP = 'gbp'
    RUB = 'rub'


DATETIME_FORMAT = '%Y-%m-%dT%H:%M:%S'
UTC_DATETIME_FORMAT = '%Y-%m-%dT%H:%M:%SZ'
DATE_FORMAT = '%Y-%m-%d'
MONTH_FORMAT = '%Y-%m'
VERBOSE_MONTH_FORMAT = '%B-%Y'


TTL_DAY = 86400
TTL_HOUR = 3600
