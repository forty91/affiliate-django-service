from operator import attrgetter

from rest_framework.filters import OrderingFilter


class CustomObjectsOrderingFilter(OrderingFilter):
    """
    Отвечает за сортировку кастомных, не queryset итерируемых объектов,
    например списка namedtuple или объектов некоторого класса.
    """

    def filter(self, request, entities, view):
        ordering = self.get_ordering(request, entities, view)
        reverse = False

        if ordering:
            order = ordering[0]
            if order[0] == '-':
                order = order[1:]
                reverse = True
            entities = sorted(entities, key=lambda enity: attrgetter(order)(enity), reverse=reverse)

        return entities
