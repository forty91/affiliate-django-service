class UpdateEntity:
    def __init__(self, entity, validated_data):
        self.entity = entity
        self.validated_data = validated_data

    def run(self):
        for (key, value) in self.validated_data.items():
            setattr(self.entity, key, value)

        self.entity.save()
        return self.entity


class DestroyEntity:
    def __init__(self, entity):
        self.entity = entity

    def run(self):
        self.entity.delete()
