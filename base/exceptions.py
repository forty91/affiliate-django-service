from rest_framework import status
from rest_framework.exceptions import APIException, _get_error_details


class AffiliateAPIExceptionBase(APIException):
    status_code = status.HTTP_503_SERVICE_UNAVAILABLE
    default_detail = 'Service unavailable for a while.'
    default_code = 'service unavailable'


def affiliate_api_exception(status, detail, code):
    """
    Фабричная функция. Возвращает пользовательский класс для ошибки.
    """

    class AffiliateAPIException(AffiliateAPIExceptionBase):
        status_code = status
        default_detail = detail
        default_code = code

    return AffiliateAPIException
