from rest_framework.mixins import ListModelMixin, RetrieveModelMixin, DestroyModelMixin
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet


class AffiliateViewSet(GenericViewSet):
    queryset_user_lookup_expr = None
    queryset_user_filter = None

    def get_permissions(self):
        """
        Возвращает permission_classes зависящие от action, на основе переменной permission_classes_by_action,
        если отсутствует ключ то возвращаются описанные в permission_classes.
        """
        try:
            return [permission() for permission in self.permission_classes_by_action[self.action]]
        except KeyError:
            return [permission() for permission in self.permission_classes]

    def get_queryset(self):
        """
        Список возвращаемых сущностей меняется в зависимости от permission_classes_by_action.
        Пользователи у которых permission_class с 0 индексом, могут только смотреть сущности связанные с ними.
        Остальные пользователи могут смотреть все сущности.
        """
        queryset = super().get_queryset()
        list_permission_classes = self.permission_classes_by_action[self.action]

        if len(list_permission_classes) > 1:
            if list_permission_classes[0]().has_permission(self.request, self):
                queryset = queryset.filter(**self.queryset_user_filter)

        return queryset

    def check_permissions(self, request):
        """
        Должен удовлетворять хотябы одной permission из списка permission_classes_by_action[self.action]
        """
        if not any([permission.has_permission(request, self)
                    for permission in self.get_permissions()]):
            self.permission_denied(request, None)


class AffiliateListModelMixin(ListModelMixin):
    def list(self, request, *args, **kwargs):
        self.queryset_user_filter = {self.queryset_user_lookup_expr: self.request.user}
        return super().list(self, request, *args, **kwargs)


class AffiliateRetrieveModelMixin(RetrieveModelMixin):
    def retrieve(self, request, *args, **kwargs):
        self.queryset_user_filter = {self.queryset_user_lookup_expr: self.request.user}
        return super().retrieve(self, request, *args, **kwargs)


class AffiliateDestroyModelMixin(DestroyModelMixin):
    def destroy(self, request, *args, **kwargs):
        self.queryset_user_filter = {self.queryset_user_lookup_expr: self.request.user}
        return super().destroy(self, request, *args, **kwargs)


class AffiliateAPIView(APIView):
    def check_permissions(self, request):
        list_permission_classes = self.get_permissions()
        self.is_staff = None

        if not any([permission.has_permission(request, self)
                    for permission in list_permission_classes]):
            self.permission_denied(request, None)

        if len(list_permission_classes) > 1:
            if list_permission_classes[0].has_permission(request, self):
                self.is_staff = False
            if list_permission_classes[1].has_permission(request, self):
                self.is_staff = True
