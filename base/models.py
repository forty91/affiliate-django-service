import uuid

from django.db import models


class AffiliateModel(models.Model):
    """
    Базовый класс для всех моделей приложения.
    id - уникальный идентификатор сущности в формате uuid4.
    """

    class Meta:
        abstract = True

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    @property
    def uuid(self):
        return str(self.id)

    @property
    def entity_type(self):
        return self.__class__.__name__.lower()

    # def save(self, *args, **kwargs):
    #     override_created_at = kwargs.pop('override_created_at', False)
    #     override_modified_at = kwargs.pop('override_modified_at', False)
    #
    #     if hasattr(self, 'created_at') and not override_created_at:
    #         if not self.id:
    #             self.created_at = datetime_now()
    #
    #     if hasattr(self, 'modified_at') and not override_modified_at:
    #         self.modified_at = datetime_now()
    #
    #     super().save(*args, **kwargs)

    def __str__(self, **kwargs):
        """
        Метод определяет отображение объекта. В качестве kwargs могут
        быть переданы дополнительные аргументы,
        которые будут добавлены в конец строки с отображением.
        :param kwargs: дополнительные аргументы для отображения
        :return: String
        """
        custom_string = ""
        for k, v in kwargs.items():
            custom_string = "{}, {}:{}".format(custom_string, k, v)
        return "<uuid:{}, {},{}>".format(self.id, self.__class__, custom_string)
