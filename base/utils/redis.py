import redis

from django.conf import settings


REDIS_HOST = settings.REDIS['HOST']
REDIS_PORT = settings.REDIS['PORT']
REDIS_DB = settings.REDIS['DB']
REDIS_PASSWORD = settings.REDIS['PASSWORD']


class RedisClient(redis.Redis):
    def __init__(self, host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB, password=REDIS_PASSWORD):
        self.values_count = {}
        super().__init__(host=host, port=port, db=db, password=password)
