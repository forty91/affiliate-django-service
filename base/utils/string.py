import random
import string


def random_string(length):
    """
    Генерирует случайную строку указанной длины
    """
    return ''.join([random.choice(string.ascii_letters + string.digits) for n in range(length)])


def random_len_string(min_len, max_len):
    """
    Генерирует случайную строку cлучайной длины в пределах значений
    """
    return ''.join(
        [
            random.choice(string.ascii_letters + string.digits)
            for n in range(random.randrange(min_len, max_len))
        ]
    )


def random_phone_number():
    """
    Генерирует случайный телефонный номер.
    """
    return '+%s' % ''.join(
        [
            random.choice(string.digits)
            for n in range(random.randrange(10, 20))
        ]
    )
