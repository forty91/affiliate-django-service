import logging
import boto3
from botocore.exceptions import ClientError
from django.conf import settings
from rest_framework.status import HTTP_503_SERVICE_UNAVAILABLE

from base.exceptions import affiliate_api_exception

CHARSET = "UTF-8"
SENDER = "team@cyber.bet"


class SendEmail:
    def __init__(self,
                 region_name=settings.AWS_REGION,
                 aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
                 aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY):
        self.client = boto3.client(
            'ses',
            region_name=region_name,
            aws_access_key_id=aws_access_key_id,
            aws_secret_access_key=aws_secret_access_key
        )

    def run(self, recipient, subject, body, sender=SENDER):
        try:
            response = self.client.send_email(
                Destination={
                    'ToAddresses': [
                        recipient,
                    ],
                },
                Message={
                    'Subject': {
                        'Charset': CHARSET,
                        'Data': subject,
                    },
                    'Body': {
                        'Html': {
                            'Charset': CHARSET,
                            'Data': body,
                        },
                    },
                },
                Source=sender,
            )
        except ClientError as e:
            logging.error('Sending email error: %s' % e.response['Error'])
            raise affiliate_api_exception(status=HTTP_503_SERVICE_UNAVAILABLE,
                                          detail='Email service unavailable for a while.',
                                          code='service unavailable')
        else:
            logging.info('Sent confirmation email: %s' % response['MessageId'])
