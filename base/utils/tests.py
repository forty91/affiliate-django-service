import uuid
from datetime import timedelta

from PIL import Image
from django.core.files.temp import NamedTemporaryFile

from base.utils.clickhouse import ClickhouseClient
from promos.tests.factories import PromoFactory
from traffic.utils import create_partner_table_if_not_exists, get_partner_table_name


class ClickhouseInfrastructure:
    """
    Подготавливает инфраструктуру в clickhouse.
    Если create_promos==True для каждого partner создается 2 promo.
    Для каждого promo у каждого партнера создаются записи в clickhouse:
        * 2 записи вне timestamp range, 1 уникалная загрузка, 1 уникальный клик
        * 7 записей, 3 загрузки (2 уникальных) 4 клика (3 уникальных)
    """
    def __init__(self, partners, timestamp_range, create_promos=False):
        self.clickhouse_client = ClickhouseClient()
        self.partners = partners
        self.timestamp__gte = timestamp_range[0]
        self.timestamp__lte = timestamp_range[1]
        self.create_promos = create_promos

    def __enter__(self):
        if self.create_promos:
            for partner in self.partners:
                PromoFactory(partner=partner)
                PromoFactory(partner=partner)

        landing_id = uuid.uuid4()

        for partner in self.partners:
            create_partner_table_if_not_exists(self.clickhouse_client, partner)
            partner_table_name = get_partner_table_name(partner)

            for promo in partner.promos.all():
                user_token1 = uuid.uuid4()
                user_token2 = uuid.uuid4()
                user_token3 = uuid.uuid4()
                self.clickhouse_client.execute(
                    "INSERT INTO {table_name} VALUES".format(table_name=partner_table_name),
                    [
                        # 2 запроса вне timestamp range, 1 уникалная загрузка, 1 уникальный клик
                        [promo.id, None, 'download', '1.1.1.1', 'some useragent1', uuid.uuid4(),
                         self.timestamp__gte - timedelta(seconds=100)],
                        [promo.id, landing_id, 'click', '1.1.1.1', 'some useragent1', uuid.uuid4(),
                         self.timestamp__lte + timedelta(seconds=100)],

                        # 7 запросов, 2 уникальных загрузки, 3 уникальных клика
                        [promo.id, None, 'download', '1.1.1.1', 'some useragent1', user_token1,
                         self.timestamp__gte + timedelta(seconds=100)],
                        [promo.id, landing_id, 'click', '1.1.1.1', 'some useragent1', user_token1,
                         self.timestamp__lte - timedelta(seconds=100)],
                        [promo.id, None, 'download', '1.1.1.1', 'some useragent1', user_token1,
                         self.timestamp__gte + timedelta(hours=10)],
                        [promo.id, landing_id, 'click', '2.2.2.2', 'some useragent2', user_token2,
                         self.timestamp__lte - timedelta(hours=10)],
                        [promo.id, landing_id, 'click', '2.2.2.2', 'some useragent2', user_token2,
                         self.timestamp__lte - timedelta(hours=10)],
                        [promo.id, None, 'download', '3.3.3.3', 'some useragent3', user_token3,
                         self.timestamp__gte],
                        [promo.id, landing_id, 'click', '3.3.3.3', 'some useragent3', user_token3,
                         self.timestamp__lte],
                    ]
                )
        return self.clickhouse_client

    def __exit__(self, *args):
        for partner in self.partners:
            partner_clickhouse_table = get_partner_table_name(partner)
            tables = [t[0] for t in self.clickhouse_client.execute('SHOW TABLES')]
            if partner_clickhouse_table in tables:
                self.clickhouse_client.execute('DROP TABLE {table_name}'.format(table_name=partner_clickhouse_table))


def generate_tmp_image():
    image = Image.new('RGB', (1, 1))
    file = NamedTemporaryFile(suffix='.png')
    image.save(file)
    return file
