import psycopg2

from django.conf import settings


DB_SETTINGS = settings.DATABASES['default']


def select(table, condition):
    """
    Использовать если появилась необходимость сделать выборку вобход django ORM
    """
    conn = psycopg2.connect("dbname={dbname} "
                            "user={user} "
                            "password={password} "
                            "host={host} "
                            "port={port}".format(dbname=DB_SETTINGS['NAME'],
                                                 user=DB_SETTINGS['USER'],
                                                 password=DB_SETTINGS['PASSWORD'],
                                                 host=DB_SETTINGS['HOST'],
                                                 port=DB_SETTINGS['PORT']))
    cur = conn.cursor()

    cur.execute(f"SELECT * FROM {table} {condition};")
    rows = cur.fetchall()

    cur.close()
    conn.close()

    return rows
