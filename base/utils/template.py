from jinja2 import Template


def render_template(template_path, **context):
    with open(template_path) as f:
        template_file = f.read()
    template = Template(template_file)
    return template.render(context)
