import logging
from concurrent.futures import ThreadPoolExecutor, as_completed

import requests


class ThreadPoolRequests:
    """
    Позволяет посылать множество паралельных http запросов
    """
    def __init__(self, max_workers=10):
        self.pool = ThreadPoolExecutor(max_workers=max_workers)
        self.futures = []

    def get(self, url):
        self.futures.append(self.pool.submit(requests.get, url))

        for future in as_completed(self.futures):
            response = future.result()
            logging.info('Knock request on url:\n%s\nresponse: %s\n%s', url, response.status_code, response.content)
            self.futures.remove(future)
