from django.conf import settings
from django.apps import apps


def get_default_partner_uuid():
    Partner = apps.get_model("users", "Partner")
    default_partner = Partner.objects.get(user__username=settings.DEFAULT_PARTNER_NAME)
    return str(default_partner.pk)


def get_default_program_uuid():
    ProgramType = apps.get_model("projects", "ProgramType")
    Project = apps.get_model("projects", "Project")
    Program = apps.get_model("projects", "Program")

    default_program_type = ProgramType.objects.default()
    default_project = Project.objects.default()
    program, is_created = Program.objects.get_or_create(
        type=default_program_type,
        project=default_project,
        rate=settings.DEFAULT_PROGRAM_RATE,
        amount=0.0
    )
    return str(program.pk)


def get_default_landing_uuid():
    Landing = apps.get_model("promos", "Landing")

    landing = Landing.objects.get(name=settings.DEFAULT_LANDING_NAME)
    return str(landing.pk)


def get_default_promo_uuid():
    Promo = apps.get_model("promos", "Promo")

    promo = Promo.objects.get(name=settings.DEFAULT_PROMO_NAME)
    return str(promo.pk)
