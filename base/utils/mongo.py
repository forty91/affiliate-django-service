import pymongo

from django.conf import settings

from base.utils.datetime import datetime_now

MONGO_HOST = settings.MONGO['HOST']
MONGO_PORT = settings.MONGO['PORT']
MONGO_DB = settings.MONGO['DB']
MONGO_USER = settings.MONGO['USER']
MONGO_PASSWORD = settings.MONGO['PASSWORD']
MONGO_REPLICASET = settings.MONGO['REPLICASET']


class MongoClient:
    def __init__(self, host=MONGO_HOST, port=MONGO_PORT, db=MONGO_DB, user=MONGO_USER, password=MONGO_PASSWORD,
                 replicaset=MONGO_REPLICASET):
        self.client = pymongo.MongoClient(
            settings.MONGO_CONNECTION_TEMPLATE.format(
                user=user, password=password, host=host, port=port, db=db, replicaset=replicaset
            )
        )
        self.db = self.client[db]

    def insert(self, collection, data):
        collection = self.db[collection]
        if isinstance(data, list):
            collection.insert_many(data)
        else:
            collection.insert_one(data)

    def get(self, collection, data):
        collection = self.db[collection]
        result = collection.find_one(data)

        return result

    def filter(self, collection, data=None, sort=None):
        collection = self.db[collection]
        result = collection.find(data or {})
        if sort:
            result = result.sort(sort)

        return result

    def count(self, collection, filter=None):
        result = collection.count_documents(filter or {})

        return result

    def create_indexes(self, collection, indexes):
        collection = self.db[collection]
        collection.create_indexes(indexes)

    def index_information(self, collection):
        collection = self.db[collection]
        return collection.index_information()


class MongoLogging(MongoClient):
    def log(self, collection, action, data):
        data = dict(**data, **{'action': action, 'timestamp': datetime_now()})
        self.insert(collection, data)

        if 'timestamp_-1' not in self.index_information(collection):
            self.create_indexes(collection, [pymongo.IndexModel([('timestamp', pymongo.DESCENDING)])])
