from rest_framework_jwt.utils import jwt_payload_handler

from base.consts import UTC_DATETIME_FORMAT


def affiliate_jwt_payload_handler(user):
    payload = jwt_payload_handler(user)

    if hasattr(user, 'partner_profile'):
        payload['id'] = user.partner_profile.uuid
    elif hasattr(user, 'staff_profile'):
        payload['id'] = user.staff_profile.uuid

    payload['joined_at'] = user.date_joined.strftime(UTC_DATETIME_FORMAT)

    return payload
