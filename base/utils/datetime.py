from datetime import datetime, timedelta

import pytz
from dateutil import tz, parser, rrule, relativedelta
from dateutil.relativedelta import relativedelta

UTC_TIMEZONE = tz.gettz("UTC")
RIGA_TIMEZONE = tz.gettz('Europe/Riga')


EPOCH_DATETIME = datetime(1970, 1, 1, 0, 0, 0, tzinfo=UTC_TIMEZONE)


def datetime_now(timezone=UTC_TIMEZONE, truncate_ms=False):
    dt = datetime.now(tz=timezone)
    if truncate_ms:
        dt = dt.replace(microsecond=0)

    return dt


def timestamp_to_datetime(seconds, nanoseconds=0, timezone=UTC_TIMEZONE):
    microseconds = int(nanoseconds / 1000)
    timestamp = float(seconds) + float(microseconds) * 0.00001
    return datetime.fromtimestamp(timestamp, tz=timezone)


def datetime_to_timestamp(dt, truncate_ms=False):
    ts = dt.timestamp()
    if truncate_ms:
        ts = int(ts)

    return ts


def datetime_to_iso(dt, tz=RIGA_TIMEZONE, truncate_ms=False):
    """
    :type dt: datetime.datetime
    :rtype: str
    """
    if dt is None:
        return None

    if truncate_ms:
        dt = dt.replace(microsecond=0)

    if dt.tzinfo is None:
        dt = dt.replace(tzinfo=UTC_TIMEZONE)

    value = dt.astimezone(tz).replace(tzinfo=UTC_TIMEZONE).isoformat()
    if value.endswith('+00:00'):
        value = value[:-6]
    return value


def utc_datetime_to_iso(dt, truncate_ms=False):
    """
    :type dt: datetime.datetime
    :rtype: str
    """
    if dt is None:
        return None

    if truncate_ms:
        dt = dt.replace(microsecond=0)

    if dt.tzinfo is None:
        dt = dt.replace(tzinfo=UTC_TIMEZONE)

    value = dt.astimezone(UTC_TIMEZONE).isoformat()
    if value.endswith('+00:00'):
        value = value[:-6] + 'Z'
    return value


def iso_to_datetime(iso_dt, tz=UTC_TIMEZONE):
    return parser.parse(iso_dt).replace(tzinfo=tz)


def utc_datetime(*args, **kwargs):
    return datetime(*args, tzinfo=UTC_TIMEZONE, **kwargs)


def datetime_in_range(start, end, x):
    """Return true if x is in the range [start, end]"""
    if start <= end:
        return start <= x <= end
    else:
        return start <= x or x <= end


def dayrange(start_date, end_date):
    """
    Возвращает даты виде datetime начала дня между двумя датами включительно.
    """
    end_date += timedelta(days=1)
    for day in range(int((end_date - start_date).days)):
        day = start_date + timedelta(days=day)
        day = datetime.combine(day, datetime.min.time(), tzinfo=pytz.utc)
        yield day


def monthrange(start_date, end_date):
    """
    Возвращает масяцы в виде datetime начала дня первого числа между двумя датами включительно.
    """
    for month in rrule.rrule(rrule.MONTHLY, dtstart=start_date + relativedelta(day=1), until=end_date):
        month = datetime.combine(month + relativedelta(day=1), datetime.min.time(), tzinfo=pytz.utc)
        yield month
