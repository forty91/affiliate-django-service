import json
import grpc
from copy import deepcopy
from datetime import datetime, date
from cyberbet.protobuf.currency import currency_pb2_grpc, currency_pb2
from google.protobuf import timestamp_pb2, json_format
from django.conf import settings
from constance import config

from base.consts import Currency, DATE_FORMAT
from base.utils.datetime import datetime_to_timestamp, datetime_now


CONSTANCE_CURRENCY_RATES = 'CURRENCY_RATES'


def get_currency(currency=Currency.EUR, timestamp=None):
    """
    Получение курсов валют в сыром виде.
    """
    if timestamp is None:
        timestamp = datetime_to_timestamp(datetime_now())

    channel = grpc.insecure_channel(settings.CURRENCY_SERVICE_URL)
    stub = currency_pb2_grpc.CurrencyServiceStub(channel)

    rate_request = currency_pb2.RateRequest(base=currency_pb2.Code.Value(currency.upper()),
                                            timestamp=timestamp_pb2.Timestamp(seconds=int(timestamp)))
    rate_response = stub.Rate(rate_request)
    return json.loads(json_format.MessageToJson(rate_response))


def get_currency_rates(currency=Currency.EUR, timestamp=None):
    """
    Получение курсов валют в формате
    {<валюта>: <курс по отношению к выбранной валюте>, <другая_валюта>: <курс по отношению к выбранной валюте>...}
    """
    currency_response = get_currency(currency=currency, timestamp=timestamp)

    currency_rates = {
        row.get('currency', currency).lower(): float(row['rate'])
        for row in currency_response['rates']
    }

    return currency_rates


def aggregate_to_one_currency(values_dict, currency=Currency.EUR, timestamp=None):
    """
    Приведение словаря с данными вида {<валюта>: <число>, <другая_валюта>: <число>...} к одной выбранной валюте.
    Доступные ключи для словаря содержатся в перечислении Currency.
    """
    other_currencies = deepcopy(Currency.keys)
    other_currencies.remove(currency)
    currency_rates = get_currency_rates(currency=currency, timestamp=timestamp)

    value = values_dict.get(currency, 0.0)
    for curr in other_currencies:
        value += values_dict[curr] / currency_rates[curr]

    return round(value, 4)


def get_today_currency_rates(currency=Currency.EUR):
    """
    Получение курсов валют за текущий день из constance
    чтобы не выполнять постоянно запросы к currency-service.
    Не уверен что это хорошее решение, но лучшего места для хранения не придумал.
    """
    currency_rates_on_date = getattr(config, CONSTANCE_CURRENCY_RATES, None)
    if currency_rates_on_date:
        currency_rates_on_date = json.loads(currency_rates_on_date)
        if datetime.strptime(currency_rates_on_date['date'], DATE_FORMAT).date() >= date.today():
            return currency_rates_on_date['rates']

    new_currency_rates_on_date = {
        'rates': get_currency_rates(currency=currency),
        'date': date.today().strftime(DATE_FORMAT)
    }
    setattr(config, CONSTANCE_CURRENCY_RATES, json.dumps(new_currency_rates_on_date))
    return new_currency_rates_on_date['rates']
