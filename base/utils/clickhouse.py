import clickhouse_driver

from django.conf import settings

CLICKHOUSE_HOST = settings.CLICKHOUSE['HOST']
CLICKHOUSE_PORT = settings.CLICKHOUSE['PORT']
CLICKHOUSE_DB = settings.CLICKHOUSE['DB']
CLICKHOUSE_USER = settings.CLICKHOUSE['USER']
CLICKHOUSE_PASSWORD = settings.CLICKHOUSE['PASSWORD']
SEND_RECEIVE_TIMEOUT = 100


class ClickhouseClient(clickhouse_driver.Client):
    def __init__(self, host=CLICKHOUSE_HOST, port=CLICKHOUSE_PORT, database=CLICKHOUSE_DB,
                 user=CLICKHOUSE_USER, password=CLICKHOUSE_PASSWORD,
                 send_receive_timeout=SEND_RECEIVE_TIMEOUT):
        super().__init__(host=host, port=port, database=database, user=user, password=password,
                         send_receive_timeout=send_receive_timeout)
