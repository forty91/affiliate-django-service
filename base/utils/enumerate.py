class EnumerateMetaClass(type):
    """
    Метакласс для перечисляемых сущностей
    """
    def __new__(cls, name, bases, dct):
        keys = sorted([key for key in dct.keys() if key.isupper() and not key.startswith('_')])
        l_keys = [key.lower() for key in keys]
        choices = [(l_key, dct[key]) for key, l_key in zip(keys, l_keys)]
        reversed_choices = [(dct[key], l_key) for key, l_key in zip(keys, l_keys)]
        dct.update(
            dict(zip(keys, l_keys)),
            choices=choices, _choices_dict=dict(choices), _reversed_choices_dict=dict(reversed_choices), keys=l_keys
        )
        return super().__new__(cls, name, bases, dct)


class BaseEnumerate(metaclass=EnumerateMetaClass):
    """
    ВНИМАНИЕ! Содержит порцию магии метаклассов.

    Базовый класс для перечисляемых сущностей.
    Служит для упрощения работы с константами и автоматического создания choices из представленных данных.

    Имя атрибута задается в верхнем регистре и содержит человекочитаемое обозначение данного атрибута.
    В дальнейшем имя атрибута преобразуется в нижний регистр и используется как идентификатор.
    Идентификатор присваивается атрибуту, человекочитаемые названия кэшируются в переменную choices,
    которую можно использовать в моделях либо в форме.

    Пример работы:
    ```
    # объявление класса
    class ExampleModelTypes(BaseEnumerate):
        FOO = 'Title for foo'
        BAR = 'Title for bar'

    # далее все выражения вернут True
    ExampleModelTypes.FOO == 'foo'

    ExampleModelTypes.BAR == 'bar'

    ExampleModelTypes.choices == (
            ('foo', 'Title for foo'),
            ('bar', 'Title for bar'),
        )

    ExampleModelTypes.label(ExampleModelTypes.FOO) == 'Title for foo'
    ```
    """

    @classmethod
    def label(cls, var):
        return cls._choices_dict.get(var)

    @classmethod
    def key(cls, var):
        return cls._reversed_choices_dict.get(var)
