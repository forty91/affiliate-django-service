from django.db.models import F, Sum, Case, When, Value, Q, FloatField
from django.db.models.functions import Coalesce
from rest_framework import serializers

from base.consts import Currency


def aggregate_currencies_sum(queryset, field):
    """
    Производит агергацию по полю содержащему значения для множества типов валют.
    :param queryset: QuerySet: выборка по которой делаем агрегацию
    :param field: имя поля, содержащее значение в валюте
    :return: dict: вида {<валюта>: <сумма значений>, <другая_валюта>: <сумма значений>...}
    """
    result = queryset.aggregate(
        **{
            cur: Coalesce(Sum(Case(
                When(Q(currency=cur),
                     then=F(field)), output_field=FloatField(),
            )), Value(0.0))
            for cur in Currency.keys
        }
    )
    return result


def get_object_or_reject(queryset, uuid):
    """
    В случае отсутсвия сущности с указанным uuid'ом
    отреджектить запрос пользователя.
    """
    model = queryset.model
    try:
        return queryset.get(pk=uuid)
    except model.DoesNotExist:
        message = 'The entity {model} not exist with the id {id}'.format(model=model, id=uuid)
        raise serializers.ValidationError(message)
