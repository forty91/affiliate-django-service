import json

from rest_framework.test import APITestCase
from django.conf import settings
from django.urls import reverse

from users.tests.factories import PartnerFactory, USER_FACTORY_PASSWORD


class AffiliateAPITestCase(APITestCase):
    def setUp(self):
        self.partner = PartnerFactory()
        token_resp = self.client.post(
            reverse('public:authenticate'),
            {
                'username': self.partner.user.email,
                'password': USER_FACTORY_PASSWORD
            },
            format='json'
        )
        self.partner_jwt_token = json.loads(token_resp.content.decode('utf-8'))['token']

        token_resp = self.client.post(
            reverse('public:authenticate'),
            {
                'username': settings.DEFAULT_STAFF_NAME,
                'password': settings.DEFAULT_STAFF_PASSWORD
            },
            format='json'
        )
        self.staff_jwt_token = json.loads(token_resp.content.decode('utf-8'))['token']
