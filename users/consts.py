import os

from base.utils.enumerate import BaseEnumerate


class Status(BaseEnumerate):
    ACTIVE = 'active'
    INACTIVE = 'inactive'
    CONFIRMED = 'confirmed'
    BLOCKED = 'blocked'
    DELETED = 'deleted'


class MessengerType(BaseEnumerate):
    TELEGRAM = 'telegram'
    WHATSAPP = 'whatsapp'
    SKYPE = 'skype'
    VIBER = 'viber'


AUTHENTICATE_COLLECTION = 'authentications'
PARTNER_COLLECTION = 'partners'
STAFF_COLLECTION = 'staffs'
CONFIRMATION_COLLECTION = 'confirmations'


class AuthenticateAction(BaseEnumerate):
    LOGIN = 'login'
    LOGOUT = 'logout'


class PartnerAction(BaseEnumerate):
    CREATE = 'create'
    UPDATE = 'update'


class StaffAction(BaseEnumerate):
    CREATE = 'create'
    UPDATE = 'update'


class ConfirmationAction(BaseEnumerate):
    ACCESS_TOKEN = 'access_token'


TEMPLATES_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../users/templates')
