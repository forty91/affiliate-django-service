from django_filters import rest_framework as filters

from base.help_texts import USERNAME_ICONTAINS, EMAIL_ICONTAINS, USER_STATUS_EXACT
from users.models import Partner, Staff


class PartnerFilter(filters.FilterSet):
    username = filters.CharFilter(field_name='username', method='filter_username', help_text=USERNAME_ICONTAINS)
    email = filters.CharFilter(field_name='email', method='filter_email', help_text=EMAIL_ICONTAINS)
    status = filters.CharFilter(field_name='status', help_text=USER_STATUS_EXACT)

    class Meta:
        model = Partner
        fields = ['username', 'email', 'status']

    def filter_username(self, queryset, name, value):
        return queryset.filter(user__username__icontains=value)

    def filter_email(self, queryset, name, value):
        return queryset.filter(user__email__icontains=value)


class StaffFilter(filters.FilterSet):
    username = filters.CharFilter(field_name='username', method='filter_username', help_text=USERNAME_ICONTAINS)
    email = filters.CharFilter(field_name='email', method='filter_email', help_text=EMAIL_ICONTAINS)
    status = filters.CharFilter(field_name='status', lookup_expr='exact', help_text=USER_STATUS_EXACT)

    class Meta:
        model = Staff
        fields = ['username', 'email', 'status']

    def filter_username(self, queryset, name, value):
        return queryset.filter(user__username__icontains=value)

    def filter_email(self, queryset, name, value):
        return queryset.filter(user__email__icontains=value)
