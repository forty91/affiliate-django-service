from django.contrib.auth import authenticate
from drf_queryfields import QueryFieldsMixin
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer, Serializer, CharField, SerializerMethodField, EmailField, \
    ChoiceField
from rest_framework.validators import UniqueValidator
from rest_framework_jwt.serializers import jwt_payload_handler, jwt_encode_handler

from base.help_texts import USERNAME, SITE, PASSWORD, JWT_TOKEN, USERNAME_OR_EMAIL, USER_STATUS, FIRST_NAME, \
    LAST_NAME, PHONE, CONFIRMATION_TOKEN
from permissions.serializers import RoleSerializer
from users.consts import Status
from users.models import Partner, Staff, User, Messenger, ConfirmationToken


class MessengerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Messenger
        fields = ('id', 'username', 'type')


class MessengerCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Messenger
        fields = ('username', 'type')


class AuthenticateSerializer(Serializer):
    username = CharField(help_text=USERNAME_OR_EMAIL)
    password = CharField(help_text=PASSWORD)

    def validate(self, attrs):
        credentials = {
            'username': attrs['username'],
            'password': attrs['password']
        }

        user = authenticate(**credentials)

        if user:
            if hasattr(user, 'partner_profile'):
                status = user.partner_profile.status
                if status != Status.ACTIVE:
                    msg = f'Unable to login with status: {status}.'
                    raise serializers.ValidationError(msg)

            if hasattr(user, 'staff_profile'):
                status = user.staff_profile.status
                if status != Status.ACTIVE:
                    msg = f'Unable to login with status: {status}.'
                    raise serializers.ValidationError(msg)

            payload = jwt_payload_handler(user)
            return {
                'token': jwt_encode_handler(payload),
                'user': user
            }

        else:
            msg = 'Unable to log in with provided credentials.'
            raise serializers.ValidationError(msg)


class ConfirmationTokenSerializer(Serializer):
    token = CharField(help_text=CONFIRMATION_TOKEN)

    def validate(self, attrs):
        token_qs = ConfirmationToken.objects.filter(token=attrs['token'])
        if token_qs.exists():
            token = token_qs.first()
            user = token.user
            if hasattr(user, 'partner_profile'):
                partner = user.partner_profile
                return {
                    'token': token,
                    'partner': partner
                }
            else:
                raise serializers.ValidationError(
                    'Only partners available for confirmation via confirmation token.'
                )
        else:
            raise serializers.ValidationError({
                    'token': ['Incorrect confirmation token.'],
                })


class ConfirmationSendEmailSerializer(Serializer):
    partner = serializers.PrimaryKeyRelatedField(queryset=Partner.objects.filter(status=Status.INACTIVE))


class AuthenticationTokenSerializer(Serializer):
    token = CharField(help_text=JWT_TOKEN)


class PartnerSerializer(ModelSerializer):
    username = SerializerMethodField(help_text=USERNAME)
    first_name = SerializerMethodField(help_text=FIRST_NAME)
    last_name = SerializerMethodField(help_text=LAST_NAME)
    messenger = MessengerSerializer()

    def get_username(self, obj):
        return obj.user.username

    def get_email(self, obj):
        return obj.user.email

    def get_first_name(self, obj):
        return obj.user.first_name

    def get_last_name(self, obj):
        return obj.user.last_name

    class Meta:
        model = Partner
        fields = ('id', 'username', 'phone', 'messenger', 'first_name', 'last_name', 'site', 'status')


class PartnerCreateSerializer(QueryFieldsMixin, Serializer):
    username = EmailField(
        help_text=USERNAME,
        validators=[
            UniqueValidator(
                queryset=User.objects.all(),
                message='Username already used.',
                lookup='iexact'
            )
        ]
    )
    phone = CharField(
        required=False, max_length=20, help_text=PHONE,
        validators=[
            # validate_phone_number,
            # UniqueValidator(
            #     queryset=Partner.objects.all(),
            #     message='Phone already used.'
            # )
        ]
    )
    messenger = MessengerCreateSerializer(required=False)
    first_name = CharField(
        required=False, max_length=30, help_text=FIRST_NAME,
        validators=[]
    )
    last_name = CharField(
        required=False, max_length=150, help_text=LAST_NAME,
        validators=[]
    )
    password = CharField(max_length=50, help_text=PASSWORD)
    site = CharField(max_length=150, required=False, help_text=SITE)


class PartnerUpdateSerializer(Serializer):
    username = CharField(
        max_length=150, required=False, help_text=USERNAME,
        validators=[
            UniqueValidator(
                queryset=User.objects.all(),
                message='Username already used.',
                lookup='iexact'
            )
        ]
    )
    phone = CharField(
        max_length=20, required=False, help_text=PHONE,
        validators=[
            # validate_phone_number,
            # UniqueValidator(
            #     queryset=Partner.objects.all(),
            #     message='Phone already used.'
            # )
        ]
    )
    messenger = MessengerCreateSerializer(required=False)
    first_name = CharField(
        max_length=30, required=False, help_text=FIRST_NAME,
        validators=[]
    )
    last_name = CharField(
        max_length=150, required=False, help_text=LAST_NAME,
        validators=[]
    )
    password = CharField(max_length=50, required=False, help_text=PASSWORD)
    site = CharField(max_length=150, required=False, help_text=SITE)


class PartnerUpdateStatusSerializer(Serializer):
    status = ChoiceField(choices=Status.choices, required=True, allow_null=False, help_text=USER_STATUS)


class StaffSerializer(ModelSerializer):
    username = SerializerMethodField(help_text=USERNAME)
    role = RoleSerializer()

    def get_username(self, obj):
        return obj.user.username

    class Meta:
        model = Staff
        fields = ('id', 'username', 'role', 'status')


class StaffCreateSerializer(QueryFieldsMixin, ModelSerializer):
    username = EmailField(
        help_text=USERNAME,
        validators=[
            UniqueValidator(
                queryset=User.objects.all(),
                message='Username already used.',
                lookup='iexact'
            )
        ]
    )
    password = CharField(max_length=50, help_text=PASSWORD)

    class Meta:
        model = Staff
        fields = ('username', 'password')


class StaffUpdateSerializer(Serializer):
    username = EmailField(
        required=False, help_text=USERNAME,
        validators=[
            UniqueValidator(
                queryset=User.objects.all(),
                message='Username already used.',
                lookup='iexact'
            )
        ]
    )
    password = CharField(max_length=50, required=False, help_text=PASSWORD)
    status = ChoiceField(choices=Status.choices, required=False, allow_null=False, help_text=USER_STATUS)
