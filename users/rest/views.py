from django.http import JsonResponse
from drf_yasg.utils import swagger_auto_schema
from rest_framework.decorators import action
from rest_framework.filters import OrderingFilter
from django_filters import rest_framework as filters
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_201_CREATED

from api_public.pagination import AffiliatePagination
from api_public.permissions import IsPartner, staff_permission, IsStaff
from base.views import AffiliateViewSet, AffiliateListModelMixin, AffiliateRetrieveModelMixin
from permissions.consts import Block, Action
from users.models import Partner, Staff
from users.bl import CreatePartner, CreateStaff, UpdatePartner, UpdateStatusPartner, UpdateStaff, Authenticate, Logout, \
    ConfirmationConfirm, SendConfirmationTokenByEmail
from users.rest.filters import PartnerFilter, StaffFilter
from users.rest.serializers import PartnerSerializer, StaffSerializer, PartnerCreateSerializer, StaffCreateSerializer, \
    AuthenticateSerializer, AuthenticationTokenSerializer, PartnerUpdateSerializer, PartnerUpdateStatusSerializer, \
    StaffUpdateSerializer, ConfirmationTokenSerializer, ConfirmationSendEmailSerializer


class AuthenticateView(APIView):
    """
    Endpoint for partner/staff authentication.
    """
    authentication_classes = ()
    permission_classes = (AllowAny,)

    @swagger_auto_schema(request_body=AuthenticateSerializer,
                         responses={200: AuthenticationTokenSerializer})
    def post(self, request):
        serializer = AuthenticateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        response = Authenticate(request, serializer.validated_data).run()
        return response


class ConfirmationSendEmailView(APIView):
    """
    Endpoint for send email with confirmation token.
    """
    permission_classes = (AllowAny,)

    @swagger_auto_schema(request_body=ConfirmationSendEmailSerializer,
                         responses={200: '{"status": "sent"}'})
    def post(self, request):
        serializer = ConfirmationSendEmailSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        SendConfirmationTokenByEmail(serializer.validated_data).run()
        return JsonResponse(data={'status': 'sent'}, status=HTTP_200_OK)


class ConfirmationConfirmView(APIView):
    """
    Endpoint for confirmation partner via confirmation token.
    """
    permission_classes = (AllowAny,)

    @swagger_auto_schema(request_body=ConfirmationTokenSerializer,
                         responses={200: PartnerSerializer})
    def post(self, request):
        serializer = ConfirmationTokenSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        partner = ConfirmationConfirm(serializer.validated_data).run()
        return Response(data=PartnerSerializer(instance=partner).data,
                        status=HTTP_200_OK)


class LogoutView(APIView):
    """
    Endpoint for partner/staff logout.
    """
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        Logout(request).run()
        return Response(status=HTTP_200_OK)


class PartnerViewSet(AffiliateViewSet, AffiliateListModelMixin, AffiliateRetrieveModelMixin):
    """
    list:
    For staff user return a list of all the existing partners.
    For partner user return a list with single current partner.

    retrieve:
    Return a single partner. Partner must exists in list for current user.

    create:
    Create a new partner. Allowed without Authorisation.

    update:
    Update an existed partner. Partner must exists in list for current user.

    status:
    Update status for existed partner. Allow for users with staff permissions only.
    """
    serializer_class = PartnerSerializer
    queryset = Partner.objects.all()
    queryset_user_lookup_expr = 'user'
    pagination_class = AffiliatePagination
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter)
    filter_class = PartnerFilter
    ordering_fields = ('user__username', )
    ordering = ('user__username', )

    permission_classes_by_action = {
        'create': [AllowAny],
        'list': [IsPartner, staff_permission(Block.PARTNERS, Action.READ)],
        'retrieve': [IsPartner, staff_permission(Block.PARTNERS, Action.READ)],
        'update': [IsPartner, staff_permission(Block.PARTNERS, Action.UPDATE)],
        'status': [staff_permission(Block.PARTNERS, Action.UPDATE)],
    }

    @swagger_auto_schema(request_body=PartnerCreateSerializer,
                         responses={HTTP_201_CREATED: PartnerSerializer})
    def create(self, request):
        serializer = PartnerCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        partner = CreatePartner(serializer.validated_data).run()
        return Response(data=PartnerSerializer(instance=partner).data,
                        status=HTTP_201_CREATED)

    @swagger_auto_schema(request_body=PartnerUpdateSerializer,
                         responses={HTTP_200_OK: PartnerSerializer})
    def update(self, request, *args, **kwargs):
        serializer = PartnerUpdateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        self.queryset_user_filter = {self.queryset_user_lookup_expr: self.request.user}

        partner = UpdatePartner(self.get_object(), serializer.validated_data).run()
        return Response(data=PartnerSerializer(instance=partner).data,
                        status=HTTP_200_OK)

    @swagger_auto_schema(request_body=PartnerUpdateStatusSerializer,
                         responses={HTTP_200_OK: PartnerSerializer})
    @action(methods=['put'], detail=True, url_path='status')
    def status(self, request, *args, **kwargs):
        serializer = PartnerUpdateStatusSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        partner = UpdateStatusPartner(self.get_object(), serializer.validated_data).run()
        return Response(data=PartnerSerializer(instance=partner).data,
                        status=HTTP_200_OK)


class StaffViewSet(AffiliateViewSet, AffiliateListModelMixin, AffiliateRetrieveModelMixin):
    """
    list:
    For staff user with special permissions return a list of all the existing staff users.
    For staff user without special permissions return a list with single current staff user.

    retrieve:
    Return a single staff. Staff must exists in list for current user.

    create:
    Create a new staff instance. Allowed only for staff user with special permissions.
    """
    permission_classes = (staff_permission(Block.STAFF, Action.READ),)
    serializer_class = StaffSerializer
    queryset = Staff.objects.all()
    queryset_user_lookup_expr = 'user'
    pagination_class = AffiliatePagination
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter)
    filter_class = StaffFilter
    ordering_fields = ('user__username', )
    ordering = ('user__username', )

    permission_classes_by_action = {
        'create': [staff_permission(Block.STAFF, Action.CREATE)],
        'list': [IsStaff, staff_permission(Block.STAFF, Action.READ)],
        'retrieve': [IsStaff, staff_permission(Block.STAFF, Action.READ)],
        'update': [staff_permission(Block.STAFF, Action.UPDATE)],
    }

    @swagger_auto_schema(request_body=StaffCreateSerializer,
                         responses={HTTP_201_CREATED: StaffSerializer})
    def create(self, request):
        serializer = StaffCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        staff = CreateStaff(serializer.validated_data).run()
        return Response(data=StaffSerializer(instance=staff).data,
                        status=HTTP_201_CREATED)

    @swagger_auto_schema(request_body=StaffUpdateSerializer,
                         responses={HTTP_200_OK: PartnerSerializer})
    def update(self, request, *args, **kwargs):
        serializer = StaffUpdateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        staff = UpdateStaff(self.get_object(), serializer.validated_data).run()
        return Response(data=StaffSerializer(instance=staff).data,
                        status=HTTP_200_OK)
