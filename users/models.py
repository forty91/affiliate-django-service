from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.conf import settings

from base.help_texts import USER_STATUS, SITE, PARTNER_HOLD, PHONE, MESSENGER_TYPE, MESSENGER_USERNAME
from base.models import AffiliateModel
from projects.models import Program
from users.consts import Status, MessengerType


class Messenger(AffiliateModel):
    type = models.SlugField(choices=MessengerType.choices, help_text=MESSENGER_TYPE)
    username = models.CharField(max_length=255, help_text=MESSENGER_USERNAME)


class UserManager(BaseUserManager):
    def get_by_natural_key(self, username):
        return self.get(username__iexact=username)


class User(AffiliateModel, AbstractUser):
    class Meta(AbstractUser.Meta):
        swappable = 'AUTH_USER_MODEL'

    objects = UserManager()


class Position(models.Model):
    name = models.CharField(max_length=255)


class Staff(AffiliateModel):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=False, parent_link=False,
                                related_name='staff_profile')
    role = models.ForeignKey(to='permissions.Role', on_delete=models.SET_NULL, blank=True, null=True,
                             related_name='users')
    status = models.SlugField(choices=Status.choices, default=Status.ACTIVE, help_text=USER_STATUS)
    position = models.ForeignKey(Position, on_delete=models.SET_NULL, null=True, default=None)


class BlackListedToken(models.Model):
    token = models.CharField(max_length=500)
    user = models.ForeignKey(User, related_name="black_list_tokens", on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ("token", "user")


class ConfirmationToken(models.Model):
    token = models.CharField(max_length=500)
    user = models.ForeignKey(User, related_name="confirmation_tokens", on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ("token", "user")


class PartnerManager(models.Manager):
    def default(self):
        return self.get(user__username=settings.DEFAULT_PARTNER_NAME)


class Partner(AffiliateModel):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=False, parent_link=False,
                                related_name='partner_profile')
    status = models.SlugField(choices=Status.choices, default=Status.INACTIVE, help_text=USER_STATUS)
    site = models.URLField(null=True, help_text=SITE)
    hold = models.PositiveSmallIntegerField(default=settings.DEFAULT_PARTNER_HOLD, help_text=PARTNER_HOLD)
    phone = models.CharField(max_length=20, null=True, help_text=PHONE)
    messenger = models.OneToOneField(Messenger, on_delete=models.SET_NULL, null=True, parent_link=False,
                                     related_name='partner_messenger')

    objects = PartnerManager()
