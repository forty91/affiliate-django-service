import hashlib
import os
from urllib.parse import urlencode
from datetime import datetime, date

from rest_framework.response import Response
from rest_framework import serializers
from rest_framework_jwt.settings import api_settings
from rest_framework_jwt.utils import jwt_response_payload_handler
from django.conf import settings

from base.bl import UpdateEntity
from base.utils.clickhouse import ClickhouseClient
from base.utils.email import SendEmail
from base.utils.mongo import MongoLogging
from base.utils.template import render_template
from permissions.models import Role
from traffic.utils import create_partner_table_if_not_exists
from users.consts import AUTHENTICATE_COLLECTION, AuthenticateAction, PartnerAction, PARTNER_COLLECTION, \
    STAFF_COLLECTION, StaffAction, Status, CONFIRMATION_COLLECTION, ConfirmationAction, TEMPLATES_DIR
from users.models import User, Partner, Staff, Messenger, BlackListedToken, ConfirmationToken
from users.rest.serializers import StaffSerializer, PartnerSerializer


class Authenticate:
    def __init__(self, request, validated_data):
        self.request = request
        self.validated_data = validated_data

    def run(self):
        user = self.validated_data['user']
        token = self.validated_data['token']
        response_data = jwt_response_payload_handler(token, user, self.request)
        response = Response(response_data)
        if api_settings.JWT_AUTH_COOKIE:
            expiration = (datetime.utcnow() + api_settings.JWT_EXPIRATION_DELTA)
            response.set_cookie(
                api_settings.JWT_AUTH_COOKIE,
                token,
                expires=expiration,
                httponly=True
            )

        MongoLogging().log(collection=AUTHENTICATE_COLLECTION, action=AuthenticateAction.LOGIN,
                           data={'username': user.username, 'token': token})

        return response


class ConfirmationConfirm:
    def __init__(self, validated_data):
        self.validated_data = validated_data

    def run(self):
        token = self.validated_data['token']
        partner = self.validated_data['partner']

        if partner.status == Status.CONFIRMED:
            token.delete()
            return partner

        if partner.status == Status.INACTIVE:
            partner.status = Status.CONFIRMED
            partner.save()

            MongoLogging().log(collection=CONFIRMATION_COLLECTION, action=ConfirmationAction.ACCESS_TOKEN,
                               data={'username': partner.user.username})

            token.delete()
            return partner

        else:
            raise serializers.ValidationError(
                f'Unable to confirm partner with status {partner.status} via confirmation token.'
            )


class SendConfirmationTokenByEmail:
    def __init__(self, validated_data):
        self.partner = validated_data['partner']

    def run(self):
        user = self.partner.user
        token = hashlib.sha256(b'%s' % user.uuid.encode('utf-8')).hexdigest()
        ConfirmationToken.objects.get_or_create(user=user, token=token)

        SendEmail().run(
            recipient=user.email,
            subject='Email confirmation.',
            body=render_template(
                template_path=os.path.join(TEMPLATES_DIR, 'confirmation.html'),
                link='%s?%s' % (settings.FRONTEND_SERVICE_URL, urlencode({'confirmation-token': token}))
            )
        )


class Logout:
    def __init__(self, request):
        self.request = request

    def run(self):
        user = self.request.user
        token = self.request.auth.decode("utf-8")
        BlackListedToken.objects.get_or_create(user=user, token=token)

        # удаляем из блек листа просроченные токены
        # TODO исполнять раз в день а не при каждом выходе
        BlackListedToken.objects.filter(timestamp__lt=datetime.combine(date.today(), datetime.min.time())).delete()

        if hasattr(user, 'staff_profile'):
            user_data = StaffSerializer(instance=user.staff_profile).data
        else:
            user_data = PartnerSerializer(instance=user.partner_profile).data

        MongoLogging().log(collection=AUTHENTICATE_COLLECTION, action=AuthenticateAction.LOGOUT,
                           data=user_data)


class CreateMessenger:
    def __init__(self, validated_data):
        self.validated_data = validated_data

    def run(self):
        messenger = Messenger.objects.create(
            type=self.validated_data['type'],
            username=self.validated_data['username']
        )

        return messenger


class UpdateMessenger(UpdateEntity):
    pass


class CreatePartner:
    def __init__(self, validated_data):
        self.validated_data = validated_data

    def run(self):
        user = User.objects.create(
            username=self.validated_data['username'],
            email=self.validated_data['username'],
            first_name=self.validated_data.get('first_name', ''),
            last_name=self.validated_data.get('last_name', ''),
        )
        user.set_password(self.validated_data['password'])
        user.save()

        partner = Partner.objects.create(user=user)
        partner.site = self.validated_data.get('site')
        partner.phone = self.validated_data.get('phone')

        if 'messenger' in self.validated_data:
            partner.messenger = CreateMessenger(self.validated_data['messenger']).run()

        partner.save()

        clickhouse_client = ClickhouseClient()
        create_partner_table_if_not_exists(clickhouse_client, partner)

        MongoLogging().log(collection=PARTNER_COLLECTION, action=PartnerAction.CREATE,
                           data=PartnerSerializer(instance=partner).data)

        return partner


class CreateStaff:
    def __init__(self, validated_data):
        self.validated_data = validated_data

    def run(self):
        user = User.objects.create(username=self.validated_data['username'],
                                   email=self.validated_data['username'],)
        user.set_password(self.validated_data['password'])
        user.save()

        # TODO сделать настройку прав и роли во входном сериалайзере а не дефолтную
        staff = Staff.objects.create(user=user, role=Role.objects.default())

        MongoLogging().log(collection=STAFF_COLLECTION, action=StaffAction.CREATE,
                           data=StaffSerializer(instance=staff).data)

        return staff


class UpdateStaff(UpdateEntity):
    def run(self):
        if 'password' in self.validated_data:
            password = self.validated_data.pop('password')
            self.entity.user.set_password(password)

        if 'username' in self.validated_data:
            self.entity.user.username = self.validated_data.pop('username')

        self.entity.user.save()

        staff = super().run()

        MongoLogging().log(collection=STAFF_COLLECTION, action=StaffAction.UPDATE,
                           data=StaffSerializer(instance=staff).data)

        return staff


class UpdatePartner(UpdateEntity):
    def run(self):
        if 'password' in self.validated_data:
            password = self.validated_data.pop('password')
            self.entity.user.set_password(password)

        if 'username' in self.validated_data:
            self.entity.user.username = self.validated_data.pop('username')
            self.entity.user.email = self.entity.user.username

        if 'first_name' in self.validated_data:
            self.entity.user.first_name = self.validated_data.pop('first_name')

        if 'last_name' in self.validated_data:
            self.entity.user.last_name = self.validated_data.pop('last_name')

        self.entity.user.save()

        if 'messenger' in self.validated_data:
            if self.entity.messenger:
                self.entity.messenger = UpdateMessenger(self.entity.messenger,
                                                        self.validated_data.pop('messenger')).run()
            else:
                self.entity.messenger = CreateMessenger(self.validated_data.pop('messenger')).run()

        partner = super().run()

        MongoLogging().log(collection=PARTNER_COLLECTION, action=PartnerAction.UPDATE,
                           data=PartnerSerializer(instance=partner).data)

        return partner


class UpdateStatusPartner(UpdateEntity):
    def run(self):
        if self.entity.status == Status.CONFIRMED and self.validated_data.get('status') == Status.ACTIVE:
            SendEmail().run(
                recipient=self.entity.user.email,
                subject='Email welcome.',
                body=render_template(
                    template_path=os.path.join(TEMPLATES_DIR, 'welcome.html')
                )
            )

        partner = super().run()

        MongoLogging().log(collection=PARTNER_COLLECTION, action=PartnerAction.UPDATE,
                           data=PartnerSerializer(instance=partner).data)

        return partner
