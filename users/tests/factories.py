import random

import factory
from django.contrib.auth.hashers import make_password
from factory import LazyAttribute
from django.conf import settings

from users.consts import Status, MessengerType
from users.models import User, Partner, Staff, Messenger, ConfirmationToken
from base.utils.string import random_len_string, random_phone_number

USER_FACTORY_PASSWORD = 'password'


class MessengerFactory(factory.django.DjangoModelFactory):
    type = LazyAttribute(lambda x: random.choice(MessengerType.keys))
    username = LazyAttribute(lambda x: random_len_string(10, 20))

    class Meta:
        model = Messenger


class UserFactory(factory.django.DjangoModelFactory):
    username = LazyAttribute(lambda x: '%s@cyber.bet' % random_len_string(6, 10))
    password = LazyAttribute(lambda x: make_password(USER_FACTORY_PASSWORD))
    first_name = LazyAttribute(lambda x: random_len_string(3, 15))
    last_name = LazyAttribute(lambda x: random_len_string(3, 15))
    email = LazyAttribute(lambda x: x.username)

    class Meta:
        model = User


class PartnerFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory(UserFactory)
    status = Status.ACTIVE
    site = LazyAttribute(lambda x: 'http://%s.com' % random_len_string(3, 10))
    phone = LazyAttribute(lambda x: random_phone_number())
    messenger = factory.SubFactory(MessengerFactory)
    hold = settings.DEFAULT_PARTNER_HOLD

    class Meta:
        model = Partner


class StaffFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory(UserFactory)
    status = Status.ACTIVE
    role = None
    position = None

    class Meta:
        model = Staff


class ConfirmationTokenFactory(factory.django.DjangoModelFactory):
    token = random_len_string(20, 30)
    user = factory.SubFactory(UserFactory)

    class Meta:
        model = ConfirmationToken
