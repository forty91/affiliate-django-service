import datetime
import json

from copy import deepcopy

from django.contrib.auth.hashers import make_password
from django.test import TestCase, tag
from django.urls import reverse
from freezegun import freeze_time
from rest_framework.test import APIRequestFactory

from base.utils.mongo import MongoLogging
from base.utils.string import random_len_string
from users.bl import CreatePartner, Authenticate, Logout, UpdatePartner, UpdateStatusPartner, CreateStaff, UpdateStaff
from users.consts import AuthenticateAction, AUTHENTICATE_COLLECTION, PARTNER_COLLECTION, PartnerAction, Status, \
    STAFF_COLLECTION, StaffAction
from users.rest.serializers import PartnerSerializer, AuthenticateSerializer, StaffSerializer
from users.tests.factories import PartnerFactory, StaffFactory, UserFactory


class AuthenticateLogoutTestCase(TestCase):
    @tag('local')
    def test_mongo_logging(self):
        username = random_len_string(3, 15)
        password = random_len_string(3, 15)

        user = UserFactory(username=username, password=make_password(password))
        partner = PartnerFactory(user=user)
        data = {
            "username": username,
            "password": password,
        }
        authenticate_url = reverse('public:authenticate')
        authenticate_request = APIRequestFactory().post(authenticate_url, data, format='json')
        authenticate_request.user = partner.user
        serializer = AuthenticateSerializer(data=json.loads(authenticate_request.body))
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data

        login_dt = datetime.datetime(2018, 10, 10, 10, 10, 10)
        with freeze_time(login_dt):
            Authenticate(authenticate_request, validated_data).run()

        logged_auth = MongoLogging().get(
            collection=AUTHENTICATE_COLLECTION,
            data={
                'token': validated_data['token'],
                'username': validated_data['user'].username
            }
        )
        logged_auth.pop('_id', None)

        self.assertEqual(login_dt, logged_auth['timestamp'])
        self.assertEqual(data['username'], logged_auth['username'])
        self.assertEqual(validated_data['token'], logged_auth['token'])

        logout_url = reverse('public:logout')
        logout_request = APIRequestFactory().post(logout_url)
        logout_request.user = partner.user
        logout_request.auth = validated_data['token'].encode("utf-8")

        logout_dt = datetime.datetime(2018, 10, 10, 11, 11, 11)
        with freeze_time(logout_dt):
            Logout(logout_request).run()

        logged_data = PartnerSerializer(instance=partner).data
        logged_logout = MongoLogging().get(collection=AUTHENTICATE_COLLECTION, data=logged_data)
        logged_logout.pop('_id', None)

        self.assertEqual(logged_logout, dict(**logged_data, **{'action': AuthenticateAction.LOGOUT, 'timestamp': logout_dt}))


class CreatePartnerTestCase(TestCase):
    @tag('local')
    def test_mongo_logging(self):
        now = datetime.datetime(2018, 10, 10, 10, 10, 10)
        validated_data = {
            "username": 'some@mail.com',
            "password": random_len_string(3, 15),
            "phone": '+71234445566',
            "messenger": {'type': 'skype', 'username': 'wasapupkin228'},
            "first_name": "Wasa",
            "last_name": "Pupkin",
            "site": 'somesite.com'
        }

        with freeze_time(now):
            partner = CreatePartner(validated_data).run()

        logged_data = PartnerSerializer(instance=partner).data
        logged_partner = MongoLogging().get(collection=PARTNER_COLLECTION, data=logged_data)
        logged_partner.pop('_id', None)

        self.assertEqual(logged_partner, dict(**logged_data, **{'action': PartnerAction.CREATE, 'timestamp': now}))
        self.assertEqual(validated_data['username'], logged_partner['username'])
        self.assertEqual(validated_data['first_name'], logged_partner['first_name'])
        self.assertEqual(validated_data['last_name'], logged_partner['last_name'])
        self.assertEqual(validated_data['phone'], logged_partner['phone'])
        self.assertEqual(validated_data['messenger']['type'], logged_partner['messenger']['type'])
        self.assertEqual(validated_data['messenger']['username'], logged_partner['messenger']['username'])
        self.assertEqual(validated_data['site'], logged_partner['site'])


class UpdatePartnerTestCase(TestCase):
    @tag('local')
    def test_mongo_logging(self):
        now = datetime.datetime(2018, 10, 10, 10, 10, 10)
        partner = PartnerFactory()
        validated_data = {
            "username": 'newname@cyber.bet',
            "password": 'newpass',
        }

        with freeze_time(now):
            partner = UpdatePartner(partner, deepcopy(validated_data)).run()

        logged_data = PartnerSerializer(instance=partner).data
        logged_partner = MongoLogging().get(collection=PARTNER_COLLECTION, data=logged_data)
        logged_partner.pop('_id', None)

        self.assertEqual(logged_partner, dict(**logged_data, **{'action': PartnerAction.UPDATE, 'timestamp': now}))
        self.assertEqual(validated_data['username'], logged_partner['username'])


class UpdateStatusPartnerTestCase(TestCase):
    @tag('local')
    def test_mongo_logging(self):
        now = datetime.datetime(2018, 10, 10, 10, 10, 10)
        partner = PartnerFactory()
        validated_data = {
            "status": Status.DELETED,
        }

        with freeze_time(now):
            partner = UpdateStatusPartner(partner, validated_data).run()

        logged_data = PartnerSerializer(instance=partner).data
        logged_partner = MongoLogging().get(collection=PARTNER_COLLECTION, data=logged_data)
        logged_partner.pop('_id', None)

        self.assertEqual(logged_partner, dict(**logged_data, **{'action': PartnerAction.UPDATE, 'timestamp': now}))
        self.assertEqual(validated_data['status'], logged_partner['status'])


class CreateStaffTestCase(TestCase):
    @tag('local')
    def test_create_mongo_logging(self):
        now = datetime.datetime(2018, 10, 10, 10, 10, 10)
        validated_data = {
            "username": 'uwasa',
            "password": 'superuwasa',
        }

        with freeze_time(now):
            staff = CreateStaff(validated_data).run()

        logged_data = StaffSerializer(instance=staff).data
        logged_staff = MongoLogging().get(collection=STAFF_COLLECTION, data=logged_data)
        logged_staff.pop('_id', None)

        self.assertEqual(logged_staff, dict(**logged_data, **{'action': StaffAction.CREATE, 'timestamp': now}))
        self.assertEqual(validated_data['username'], logged_staff['username'])


class UpdateStaffTestCase(TestCase):
    @tag('local')
    def test_mongo_logging(self):
        now = datetime.datetime(2018, 10, 10, 10, 10, 10)
        staff = StaffFactory()
        validated_data = {
            "username": 'newname',
            "password": 'newpass',
            "status": Status.DELETED,
        }

        with freeze_time(now):
            staff = UpdateStaff(staff, deepcopy(validated_data)).run()

        logged_data = StaffSerializer(instance=staff).data
        logged_staff = MongoLogging().get(collection=STAFF_COLLECTION, data=logged_data)
        logged_staff.pop('_id', None)

        self.assertEqual(logged_staff, dict(**logged_data, **{'action': PartnerAction.UPDATE, 'timestamp': now}))
        self.assertEqual(validated_data['username'], logged_staff['username'])
        self.assertEqual(validated_data['status'], logged_staff['status'])
