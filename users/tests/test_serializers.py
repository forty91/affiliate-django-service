from rest_framework.exceptions import ValidationError
from django.test import TestCase

from users.rest.serializers import PartnerCreateSerializer
from users.tests.factories import PartnerFactory


# class PartnerCreateSerializerTestCase(TestCase):
#     def test_duplicate_phone(self):
#         phone = '+71232223344'
#         partner = PartnerFactory(phone='+71232223344')
#
#         data = {
#             "username": 'some_partner',
#             "password": 'some_password',
#             "email": 'somepartner@mail.com',
#             "first_name": 'Wasa',
#             "last_name": 'Pupkin',
#             'phone': phone,
#             'messenger': {'type': 'skype', 'username': 'wasapupkin228'},
#         }
#
#         serializer = PartnerCreateSerializer(data=data)
#
#         with self.assertRaisesRegex(ValidationError, 'Phone already used.'):
#             serializer.is_valid(raise_exception=True)
