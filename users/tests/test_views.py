import json
import os
from unittest.mock import patch, ANY
from urllib.parse import urlencode

from django.urls import reverse
from django.conf import settings
from rest_framework.status import HTTP_201_CREATED, HTTP_200_OK, HTTP_400_BAD_REQUEST, HTTP_403_FORBIDDEN

from base.utils.email import SendEmail
from base.utils.template import render_template
from users.rest.serializers import MessengerSerializer, PartnerSerializer
from base.tests import AffiliateAPITestCase
from permissions.models import Role
from permissions.serializers import RoleSerializer
from users.consts import Status, TEMPLATES_DIR
from users.models import Partner, Staff, ConfirmationToken
from users.tests.factories import PartnerFactory, USER_FACTORY_PASSWORD, StaffFactory, UserFactory


class PartnerViewSetTestCase(AffiliateAPITestCase):
    def test_create_auth(self):
        """
        Регистрируем партнера. Изначально его статус inactive.
        Пытаемся получить токен и получаем ошибку 400.
        Меняем статус партнера на active.
        Успешно получаем токен.
        """
        username = 'somepartner@mail.com'
        password = 'some_password'
        first_name = 'Wasa'
        last_name = 'Pupkin'
        phone = '+71234445566'
        messenger = {'type': 'skype', 'username': 'wasapupkin228'}

        url = reverse('public:partner-list')
        data = {
            "username": username,
            "password": password,
            "first_name": first_name,
            "last_name": last_name,
            'phone': phone,
            'messenger': messenger,
        }

        response = self.client.post(url, data, format='json')
        created_partner = Partner.objects.get(user__username=username)

        expected_data = {
            'id': created_partner.uuid,
            "username": username,
            "first_name": first_name,
            "last_name": last_name,
            "phone": phone,
            "messenger": MessengerSerializer(instance=created_partner.messenger).data,
            "site": None,
            "status": Status.INACTIVE
        }

        self.assertEqual(response.status_code, HTTP_201_CREATED)
        self.assertEqual(response.data, expected_data)
        self.assertEqual(created_partner.messenger.type, messenger['type'])
        self.assertEqual(created_partner.messenger.username, messenger['username'])

        token_resp = self.client.post(
            reverse('public:authenticate'),
            {
                'username': username,
                'password': password
            },
            format='json'
        )
        self.assertEqual(token_resp.status_code, HTTP_400_BAD_REQUEST)
        self.assertEqual(
            token_resp.data,
            {'non_field_errors': ['Unable to login with status: inactive.']}
        )

        created_partner.status = Status.ACTIVE
        created_partner.save()
        token_resp = self.client.post(
            reverse('public:authenticate'),
            {
                'username': username,
                'password': password
            },
            format='json'
        )
        self.assertEqual(token_resp.status_code, HTTP_200_OK)
        jwt_token = json.loads(token_resp.content.decode('utf-8'))['token']
        self.assertTrue(jwt_token)

    def test_update(self):
        """
        Проверим что если обновить параметры партнера включая username и password,
        изменения появятся в базе, а под новыми username и password можно авторизоваться
        """
        partner = PartnerFactory(messenger=None)
        new_username = 'new@mail.com'
        new_password = 'new_password'
        new_first_name = 'Sidor'
        new_last_name = 'Petrov'
        new_site = 'http://newsite.com'
        new_phone = '+351010100'
        new_messenger = {'type': 'telegram', 'username': 'sidorpetrov123'}
        url = reverse('public:partner-detail', kwargs={'pk': partner.uuid})
        data = {
            "username": new_username,
            "first_name": new_first_name,
            "last_name": new_last_name,
            "phone": new_phone,
            "messenger": new_messenger,
            "password": new_password,
            "site": new_site
        }

        token_resp = self.client.post(
            reverse('public:authenticate'),
            {
                'username': partner.user.username,
                'password': USER_FACTORY_PASSWORD
            },
            format='json'
        )
        jwt_token = json.loads(token_resp.content.decode('utf-8'))['token']

        response = self.client.put(url, data, format='json',
                                   HTTP_AUTHORIZATION='JWT %s' % jwt_token)

        updated_partner = Partner.objects.get(user__username=new_username)

        expected_data = {
            "id": updated_partner.uuid,
            "username": new_username,
            "first_name": new_first_name,
            "last_name": new_last_name,
            "phone": new_phone,
            "messenger": MessengerSerializer(instance=updated_partner.messenger).data,
            "site": new_site,
            "status": Status.ACTIVE
        }
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertEqual(response.data, expected_data)
        self.assertEqual(updated_partner.messenger.type, new_messenger['type'])
        self.assertEqual(updated_partner.messenger.username, new_messenger['username'])

        token_resp = self.client.post(
            reverse('public:authenticate'),
            {
                'username': new_username,
                'password': new_password
            },
            format='json'
        )
        jwt_token = json.loads(token_resp.content.decode('utf-8'))['token']
        self.assertTrue(jwt_token)

    def test_update_status(self):
        partner = PartnerFactory(status=Status.INACTIVE)
        url = reverse('public:partner-status', kwargs={'pk': partner.uuid})
        data = {
            'status': Status.ACTIVE
        }
        response = self.client.put(url, data, format='json',
                                   HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token)

        partner.refresh_from_db()
        self.assertEqual(partner.status, Status.ACTIVE)

        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertEqual(response.data['status'], Status.ACTIVE)


class StaffViewSetTestCase(AffiliateAPITestCase):
    def test_create_login(self):
        """
        Создем staff,
        авторизуемся под staff и получаем jwt token,
        с использованием этого token делаем запрос на список staff,
        разлогиниваем token,
        опять делаем запрос на список staff с разлогиненным token,
        получаем 403 из-за отсутствия прав.
        """
        username = 'some_staff@cyber.bet'
        password = 'some_password'

        url_staff_list = reverse('public:staff-list')
        data = {
            'username': username,
            'password': password,
        }

        response = self.client.post(url_staff_list, data, format='json',
                                    HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token)
        created_staff = Staff.objects.get(user__username=username)

        expected_data = {
            'id': created_staff.uuid,
            "username": username,
            "role": RoleSerializer(instance=Role.objects.default()).data,
            "status": Status.ACTIVE
        }

        self.assertEqual(response.status_code, HTTP_201_CREATED)
        self.assertEqual(response.data, expected_data)

        token_resp = self.client.post(
            reverse('public:authenticate'),
            {
                'username': username,
                'password': password
            },
            format='json'
        )
        jwt_token = json.loads(token_resp.content.decode('utf-8'))['token']
        self.assertTrue(jwt_token)

        response = self.client.get(url_staff_list, data, format='json', HTTP_AUTHORIZATION='JWT %s' % jwt_token)
        self.assertEqual(response.status_code, HTTP_200_OK)

        response = self.client.post(reverse('public:logout'), data, format='json',
                                    HTTP_AUTHORIZATION='JWT %s' % jwt_token)
        self.assertEqual(response.status_code, HTTP_200_OK)

        response = self.client.get(url_staff_list, data, format='json', HTTP_AUTHORIZATION='JWT %s' % jwt_token)
        self.assertEqual(response.status_code, HTTP_403_FORBIDDEN)
        self.assertEqual(response.data, {"detail": "You do not have permission to perform this action."})

    def test_update(self):
        """
        Проверим что если обновить параметры сотрудника включая username и password,
        изменения появятся в базе, а под новыми username и password можно авторизоваться
        """
        staff = StaffFactory()
        new_username = 'new_username@cyber.bet'
        new_password = 'new_password'
        url = reverse('public:staff-detail', kwargs={'pk': staff.uuid})
        data = {
            "username": new_username,
            "password": new_password,
        }

        token_resp = self.client.post(
            reverse('public:authenticate'),
            {
                'username': staff.user.username,
                'password': USER_FACTORY_PASSWORD
            },
            format='json'
        )
        jwt_token = json.loads(token_resp.content.decode('utf-8'))['token']
        self.assertTrue(jwt_token)

        response = self.client.put(url, data, format='json',
                                   HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token)

        updated_staff = Staff.objects.get(user__username=new_username)

        expected_data = {
            'id': updated_staff.uuid,
            "username": new_username,
            "role": None,
            "status": Status.ACTIVE
        }
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertEqual(response.data, expected_data)

        token_resp = self.client.post(
            reverse('public:authenticate'),
            {
                'username': new_username,
                'password': new_password
            },
            format='json'
        )
        jwt_token = json.loads(token_resp.content.decode('utf-8'))['token']
        self.assertTrue(jwt_token)


class LogoutViewTestCase(AffiliateAPITestCase):
    def test_ok(self):
        """
        После logout, токен появляется в BlackListedToken,
        партнер больше не может выполнять запросы под указанным токеном.
        """
        self.client.post(reverse('public:logout'), HTTP_AUTHORIZATION='JWT %s' % self.partner_jwt_token)
        response = self.client.get(reverse('public:partner-list'), HTTP_AUTHORIZATION='JWT %s' % self.partner_jwt_token)

        self.assertEqual(response.status_code, HTTP_403_FORBIDDEN)


class ConfirmationTestCase(AffiliateAPITestCase):
    @patch.object(SendEmail, 'run', autospec=True)
    def test_ok(self, send_email_run_mock):
        """
        1 Создаем партнера (state inactive)
        2 Отправляем письмо содержащее ссылку на фронт с get параметром confirmation_token
        3 Подтверждаем почту партнера спомощью confirmation_token (state confirmed)
        4 Проверяем что confirmation_token удален из базы
        5 Сотрудник видит партнера в списке ожидающих активации
        6 Сотрудник активирует партнера (state active)
        7 При активации высылается welcome письмо
        """
        # 1
        user = UserFactory()
        partner = PartnerFactory(user=user, status=Status.INACTIVE)

        # 2
        response = self.client.post(
            reverse('public:confirmation-send-email'),
            {'partner': partner.uuid},
            format='json'
        )

        self.assertEqual(response.status_code, HTTP_200_OK)
        confirmation_token = ConfirmationToken.objects.filter(user=user).order_by('-timestamp').first()
        confirmation_email_body = render_template(
            template_path=os.path.join(TEMPLATES_DIR, 'confirmation.html'),
            link='%s?%s' % (settings.FRONTEND_SERVICE_URL, urlencode({'confirmation-token': confirmation_token.token}))
        )
        send_email_run_mock.assert_called_with(ANY, recipient=user.email, subject='Email confirmation.',
                                               body=confirmation_email_body)

        # 3
        response = self.client.post(
            reverse('public:confirmation-confirm'),
            {'token': confirmation_token.token},
            format='json'
        )

        partner.refresh_from_db()
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertEqual(response.data, PartnerSerializer(instance=partner).data)
        self.assertEqual(response.data['status'], Status.CONFIRMED)
        # 4
        self.assertEqual(ConfirmationToken.objects.filter(token=confirmation_token.token).exists(), False)

        # 5
        response = self.client.get(
            reverse('public:partner-list'),
            data={
                'status': Status.CONFIRMED,
            },
            HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token
        )

        expected_data = [PartnerSerializer(instance=partner).data]
        self.assertTrue(response.data['results'], expected_data)

        # 6
        response = self.client.put(
            reverse('public:partner-status', kwargs={'pk': partner.uuid}),
            data={
                'status': Status.ACTIVE
            },
            HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token
        )
        # 7
        welcome_email_body = render_template(
            template_path=os.path.join(TEMPLATES_DIR, 'welcome.html')
        )
        send_email_run_mock.assert_called_with(ANY, recipient=user.email, subject='Email welcome.',
                                               body=welcome_email_body)
