import uuid

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_auto_20190213_1401'),
    ]

    operations = [
        migrations.CreateModel(
            name='Messenger',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('type', models.SlugField(choices=[('skype', 'skype'), ('telegram', 'telegram'), ('viber', 'viber'),
                                                   ('whatsapp', 'whatsapp')],
                                          help_text="Messenger type. Any of ['skype', 'telegram', 'viber', 'whatsapp'].")),
                ('username', models.CharField(max_length=255)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='partner',
            name='phone',
            field=models.CharField(help_text='Phone number of user.', max_length=20, null=True, unique=True),
        ),
        migrations.AddField(
            model_name='partner',
            name='messenger',
            field=models.OneToOneField(null=True, on_delete=models.deletion.SET_NULL,
                                       related_name='partner_messenger', to='users.Messenger'),
        ),
    ]
