FROM cyberproject/python:alpine

ENV PYTHONUNBUFFERED 1
ENV GUNICORN_WORKERS 1
ENV GUNICORN_THREADS 2
ENV PYTHONPATH /app
ENV DJANGO_SETTINGS_MODULE affiliate.settings.docker

COPY . /app/
WORKDIR /app
RUN pip install wheels/* && \
    rm -rf wheels

EXPOSE 8080

ENTRYPOINT ["/app/docker-entrypoint.sh"]
CMD ["server"]
