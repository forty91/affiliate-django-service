from django.conf.urls import url
from rest_framework import routers

from api_public.views import ConstanseViewSet
from finances.rest.views import FlowViewSet, FlowDaysView
from payments.rest.views import PaymentViewSet
from bets.rest.views import BetViewSet
from players.rest.views import PlayerViewSet
from promos.rest.views import PromoViewSet, DirectLinkViewSet, BannerViewSet, S3StaticViewSet, LandingViewSet
from reports.rest.views import ReportDaysView, ReportMonthsView, ReportPromosView, DashboardView
from users.rest.views import AuthenticateView, LogoutView, StaffViewSet, PartnerViewSet, ConfirmationConfirmView, \
    ConfirmationSendEmailView

router = routers.DefaultRouter()
router.register(r'users/staff', StaffViewSet)
router.register(r'users/partners', PartnerViewSet)
router.register(r'promos', PromoViewSet)
router.register(r'players', PlayerViewSet)
router.register(r'bets', BetViewSet)
router.register(r'payments', PaymentViewSet)
router.register(r'direct-links', DirectLinkViewSet)
router.register(r's3-static', S3StaticViewSet)
router.register(r'banners', BannerViewSet)
router.register(r'settings', ConstanseViewSet, base_name='settings')
router.register(r'landings', LandingViewSet)
router.register(r'flows', FlowViewSet)


urlpatterns = [
    url(r'^users/auth/', AuthenticateView.as_view(), name='authenticate'),
    url(r'^users/logout/', LogoutView.as_view(), name='logout'),
    url(r'^confirmation/confirm/', ConfirmationConfirmView.as_view(), name='confirmation-confirm'),
    url(r'^confirmation/send/email/', ConfirmationSendEmailView.as_view(), name='confirmation-send-email'),

    url(r'^dashboard/', DashboardView.as_view(), name='dashboard'),
    url(r'^reports/days/', ReportDaysView.as_view(), name='report-list-days'),
    url(r'^reports/months/', ReportMonthsView.as_view(), name='report-list-months'),
    url(r'^reports/promos/', ReportPromosView.as_view(), name='report-list-promos'),
    url(r'^flows/days/', FlowDaysView.as_view(), name='flow-list-days'),
    # url(r'^users/messengers/', MessengersView.as_view()),
]

urlpatterns += router.urls
