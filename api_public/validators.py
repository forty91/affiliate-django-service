import re

from rest_framework import serializers
from django.core.exceptions import ValidationError

from players.models import Player


class ClearUnusedJsonFieldsAndRunValidators:
    """
    Валидатор подчищает неиспользуемые поля и запускает необходимые валидаторы.
    """
    def __init__(self, allowed_fields, validators):
        self.allowed_fields = allowed_fields
        self.validators = validators

    def __call__(self, fields):
        for field in list(fields.keys()):
            if field not in self.allowed_fields:
                fields.pop(field)

        for validator in self.validators:
            validator(fields)


def check_created_at(created_at):
    seconds = created_at.get('seconds')
    nanos = created_at.get('nanos')

    if not isinstance(seconds, int):
        raise serializers.ValidationError(
            {
                'seconds': 'should exists and have integer format',
            }
        )

    if nanos and not isinstance(nanos, int):
        raise serializers.ValidationError(
            {
                'nanos': 'should have integer format',
            }
        )


def check_player_cyberbet_id(player_cyberbet_id):
    try:
        player = Player.objects.get(cyberbet_id=player_cyberbet_id)
    except (Player.DoesNotExist, ValidationError) as e:
        raise serializers.ValidationError(
            'player with such cyberbet_id not exists.'
        )


def validate_latin_numbers_underscores_dashes(value):
    """
    Валидатор для строк содержащих только
    латинские буквы, цифры, тире, нижние подчеркивания.
    Начинаться могут только с латинских букв.
    """
    if re.match(r"^[A-Za-z][A-Za-z0-9_-]*$", value):
        return value
    else:
        raise serializers.ValidationError(
            'Field contains invalid character. '
            'Only latin letters, numbers, underscores and dashes allowed. '
            'First symbol should be latin letter.'
        )


def validate_phone_number(value):
    """
    Валидация телефонного номера
    """
    if re.match(r"^[+][0-9]{9,20}$", value):
        return value
    else:
        raise serializers.ValidationError(
            'Invalid phone number.'
        )
