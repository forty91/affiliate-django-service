from rest_framework import serializers
from rest_framework.fields import SerializerMethodField

from bets.models import Bet
from payments.models import Payment
from players.models import Player
from promos.models import DirectLink, Promo, S3Static, Landing, Banner
from users.models import Partner, Staff


class ConstanseSerializer(serializers.Serializer):
    key = serializers.CharField(max_length=255)
    value = serializers.CharField()


class PartnerNameSerializer(serializers.ModelSerializer):
    username = SerializerMethodField()
    email = SerializerMethodField()

    def get_username(self, obj):
        return obj.user.username

    def get_email(self, obj):
        return obj.user.email

    class Meta:
        model = Partner
        fields = ('id', 'username', 'email', 'phone')


class StaffNameSerializer(serializers.ModelSerializer):
    username = SerializerMethodField()

    def get_username(self, obj):
        return obj.user.username

    class Meta:
        model = Staff
        fields = ('id', 'username')


class DirectLinkNameSerializer(serializers.ModelSerializer):

    class Meta:
        model = DirectLink
        fields = ('id',)


class BannerNameSerializer(serializers.ModelSerializer):

    class Meta:
        model = Banner
        fields = ('id',)


class PromoNameSerializer(serializers.ModelSerializer):

    class Meta:
        model = Promo
        fields = ('id', 'name', 'created_at', 'modified_at')


class S3StaticNameSerializer(serializers.ModelSerializer):

    class Meta:
        model = S3Static
        fields = ('id', 'link')


class PlayerNameSerializer(serializers.ModelSerializer):

    class Meta:
        model = Player
        fields = ('id', 'cyberbet_id')


class PaymentNameSerializer(serializers.ModelSerializer):

    class Meta:
        model = Payment
        fields = ('id', 'cyberbet_id')


class BetNameSerializer(serializers.ModelSerializer):

    class Meta:
        model = Bet
        fields = ('id', 'cyberbet_hash')


class LandingNameSerializer(serializers.ModelSerializer):

    class Meta:
        model = Landing
        fields = ('id', 'name')
