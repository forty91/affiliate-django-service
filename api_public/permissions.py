from rest_framework.permissions import IsAuthenticated

from permissions.consts import Status as PermStatus
from permissions.models import Permission
from users.consts import Status as UserStatus
from users.models import Staff, Partner, BlackListedToken


class AffiliateIsAuthenticated(IsAuthenticated):
    def has_permission(self, request, view):
        result = super().has_permission(request, view)
        if result is False:
            return False

        user_id = request.user.id
        token = request.auth.decode("utf-8")
        if BlackListedToken.objects.filter(user=user_id, token=token).exists():
            return False

        return True


class IsPartner(AffiliateIsAuthenticated):
    """
    Является партнером
    """
    def has_permission(self, request, view):
        result = super().has_permission(request, view)
        if result is False:
            return False

        try:
            partner = request.user.partner_profile
        except Partner.DoesNotExist:
            return False

        if partner.status != UserStatus.ACTIVE:
            return False

        return True


class IsStaff(AffiliateIsAuthenticated):
    """
    Является сотрудником (особые права не требуются)
    """
    def has_permission(self, request, view):
        if not super().has_permission(request, view):
            return False

        try:
            staff = request.user.staff_profile
        except Staff.DoesNotExist:
            return False

        if staff.status != UserStatus.ACTIVE:
            return False

        return True


class StaffPermissionBase(AffiliateIsAuthenticated):
    """
    Является сотрудником с определенными правами, супер-пользователь имеет доступ ко всем правам
    """
    ACTION = None
    BLOCK = None

    def has_permission(self, request, view):
        # проверка на то, что пользователь аутентифицирован:
        if not super().has_permission(request, view):
            return False

        # проверка на то, что пользователь Staff:
        try:
            staff = request.user.staff_profile
        except Staff.DoesNotExist:
            return False

        if not staff.role:
            return False

        # проверка на супер-пользователя:
        if staff.role.is_super:
            return True

        if staff.status != UserStatus.ACTIVE or staff.role.status != PermStatus.ACTIVE:
            return False

        # проверка наличия необходимых прав:
        return Permission.objects.filter(role__users=staff,
                                         action=self.ACTION,
                                         block=self.BLOCK).exists()


def staff_permission(block, action):

    """
    Фабричная функция. Возвращает DRF Permission проверяющий доступ к указанному блоку и действию.
    :type block: str
    :type action: str
    :rtype: rest_framework.permissions.BasePermission
    """

    class StaffPermission(StaffPermissionBase):
        ACTION = action
        BLOCK = block

    return StaffPermission
