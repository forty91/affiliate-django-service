from constance import config
from django.conf import settings
from django.http import JsonResponse

from api_public.permissions import staff_permission
from api_public.serializers import ConstanseSerializer
from base.utils.constance import get_settings
from base.views import AffiliateViewSet
from permissions.consts import Block, Action


class ConstanseViewSet(AffiliateViewSet):
    """
    list:
    Return a list of all the available settings.

    create:
    Create or modify one setting value.
    """
    serializer_class = ConstanseSerializer

    permission_classes_by_action = {
        'create': [staff_permission(Block.CONSTANCE, Action.CREATE)],
        'list': [staff_permission(Block.CONSTANCE, Action.READ)],
    }

    def create(self, request):
        allow_settings = [key for key, options in getattr(settings, 'CONSTANCE_CONFIG', {}).items()]

        for key in request.data:
            if key in allow_settings and key in getattr(settings, 'CONSTANCE_CONFIG', {}):
                value = request.data[key]
                setattr(config, key, '' if value is None else value)

        return JsonResponse({'settings': get_settings(allow_settings)})

    def list(self, request, *args, **kwargs):
        allow_settings = [key for key, options in getattr(settings, 'CONSTANCE_CONFIG', {}).items()]
        return JsonResponse({'settings': get_settings(allow_settings)})
