from rest_framework.pagination import LimitOffsetPagination


class AffiliatePagination(LimitOffsetPagination):
    """
    Pagination class for API list views
    """
    default_limit = 25

