import functools

from django_filters import rest_framework as filters

from base.consts import UTC_DATETIME_FORMAT


IsoDateTimeFilter = functools.partial(filters.IsoDateTimeFilter, input_formats=[UTC_DATETIME_FORMAT])
