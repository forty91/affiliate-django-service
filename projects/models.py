from decimal import Decimal

from django.db import models
from django.conf import settings

from base.help_texts import PROJECT_NAME, PROGRAM_TYPE, PROGRAM_RATE, PROGRAM_AMOUNT, PROJECT, PROGRAM_TYPE_NAME
from base.models import AffiliateModel


class ProjectManager(models.Manager):
    def default(self):
        return self.get(name=settings.DEFAULT_PROJECT_NAME)


class Project(AffiliateModel):
    name = models.CharField(max_length=255, unique=True, help_text=PROJECT_NAME)

    objects = ProjectManager()


class ProgramTypeManager(models.Manager):
    def default(self):
        return self.get(name=settings.DEFAULT_PROGRAM_TYPE_NAME)


class ProgramType(AffiliateModel):
    name = models.CharField(max_length=255, unique=True, help_text=PROGRAM_TYPE_NAME)
    # TODO: добавить формулу расчета программ

    objects = ProgramTypeManager()


class ProgramManager(models.Manager):
    def default(self):
        default_program_type = ProgramType.objects.default()
        default_project = Project.objects.default()
        program, is_created = self.get_or_create(
            type=default_program_type,
            project=default_project,
            rate=settings.DEFAULT_PROGRAM_RATE,
            amount=0.0
        )
        return program


class Program(AffiliateModel):
    type = models.ForeignKey('projects.ProgramType', null=True, on_delete=models.SET_NULL,
                             related_name='programs', help_text=PROGRAM_TYPE)
    project = models.ForeignKey('projects.Project', null=True, on_delete=models.SET_NULL,
                                related_name='programs', help_text=PROJECT)
    rate = models.PositiveSmallIntegerField(help_text=PROGRAM_RATE)
    amount = models.DecimalField(default=Decimal(0), max_digits=12, decimal_places=4, help_text=PROGRAM_AMOUNT)

    objects = ProgramManager()
