import random
from decimal import Decimal

import factory
from factory import LazyAttribute

from base.utils.string import random_len_string
from projects.models import Project, ProgramType, Program


class ProjectFactory(factory.django.DjangoModelFactory):
    name = LazyAttribute(lambda x: random_len_string(3, 15))

    class Meta:
        model = Project


class ProgramTypeFactory(factory.django.DjangoModelFactory):
    name = LazyAttribute(lambda x: random_len_string(3, 15))

    class Meta:
        model = ProgramType


class ProgramFactory(factory.django.DjangoModelFactory):
    type = factory.SubFactory(ProgramTypeFactory)
    project = factory.SubFactory(ProjectFactory)
    rate = factory.LazyAttribute(lambda x: random.randint(10, 100))
    amount = Decimal(0)

    class Meta:
        model = Program
