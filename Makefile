IMAGE_NAME ?= cyberproject/affiliate-django-service
IMAGE_TAG ?= develop

REQ_IN  := requirements.in
REQ_OUT := requirements.txt

DC_SERVICE ?= affiliate-django-service
DB_VOLUME ?= ccserver_pgdata
DC_CMD := docker-compose -f docker-compose.yml -f docker-compose.dev.yml

.PHONY: cleanup build test isort pip-compile protobuf

cleanup:
	@-find . -name \*.pyc -delete
	@echo "[cleanup] Finished ..."

build:
	docker build -t $(IMAGE_NAME):$(IMAGE_TAG) .

pip-compile:
	pip-compile $(REQ_IN) --output-file $(REQ_OUT)

migrate:
	$(DC_CMD) run --rm $(DC_SERVICE) python manage.py migrate

run:
	$(DC_CMD) up -d $(DC_SERVICE)

stop:
	$(DC_CMD) stop

test:
	$(DC_CMD) run --rm $(DC_SERVICE) pytest

resetdb:
	$(DC_CMD) stop
	$(DC_CMD) rm -f
	docker volume rm -f $(DB_VOLUME)
	$(DC_CMD) run cc-server sh -c "python /app/manage.py makemigrations && python /app/manage.py migrate && python /app/manage.py load_fixtures"
