from base.utils.enumerate import BaseEnumerate


class FlowType(BaseEnumerate):
    CREDITS = 'credits'
    PAYOUT = 'payout'


FLOW_COLLECTION = 'flows'


class FlowAction(BaseEnumerate):
    CREATE = 'create'
    UPDATE = 'update'
