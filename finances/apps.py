from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class FundsConfig(AppConfig):
    name = 'finances'
    verbose_name = _('finances')

    def ready(self):
        import finances.signals
