from django.db.models.signals import post_save
from django.dispatch import receiver

from bets.consts import State as BetState, Status as BetStatus
from bets.models import Bet
from finances.bl import CreateFlow


@receiver(post_save, sender=Bet)
def create_funds_flow(sender, instance, **kwargs):
    """
    Сигнал отвечающий за создание движения денежных средств на счетах партнеров, основываясь на изменении
    состояния ставки игрока привлеченного этим партнером
    """
    if instance.is_freebet is False \
            and instance.original_state != instance.state and instance.state in (BetState.WIN, BetState.LOSE)\
            and instance.status == BetStatus.ACCEPTED:
        CreateFlow(bet=instance).run()
