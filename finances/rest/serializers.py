from rest_framework import serializers

from api_public.serializers import PartnerNameSerializer
from base.consts import DATE_FORMAT
from base.help_texts import FLOW_DATE, FLOW_TYPE, FLOW_AMOUNT, FLOW_BALANCE, DATE_LTE, DATE_GTE
from base.utils.datetime import datetime_now
from finances.models import Flow
from finances.utils import datetime_to_date_repr


class FlowSerializer(serializers.ModelSerializer):
    partner = PartnerNameSerializer()

    class Meta:
        model = Flow
        fields = ('id', 'type', 'amount', 'partner', 'description', 'balance', 'modified_at')


class FlowDaySerializer(serializers.Serializer):
    date = serializers.SerializerMethodField(help_text=FLOW_DATE)
    type = serializers.CharField(help_text=FLOW_TYPE)
    amount = serializers.DecimalField(max_digits=9, decimal_places=2, help_text=FLOW_AMOUNT)
    balance = serializers.DecimalField(max_digits=9, decimal_places=2, help_text=FLOW_BALANCE)

    def get_date(self, obj):
        return datetime_to_date_repr(obj.date)


class FlowParamsSerilizer(serializers.Serializer):
    date__lte = serializers.DateField(format=DATE_FORMAT, required=True, help_text=DATE_LTE)
    date__gte = serializers.DateField(format=DATE_FORMAT, required=True, help_text=DATE_GTE)

    def validate(self, attrs):
        date__gte = attrs['date__gte']
        date__lte = attrs['date__lte']

        if date__gte > date__lte:
            msg = 'Parameter date__gte should be lower or equal date__lte.'
            raise serializers.ValidationError(msg)

        today = datetime_now().date()
        if date__gte > today:
            msg = 'Parameter date__gte should be lower or equal today.'
            raise serializers.ValidationError(msg)

        if date__lte > today:
            msg = 'Parameter date__lte should be lower or equal today.'
            raise serializers.ValidationError(msg)

        return attrs
