from drf_yasg.utils import swagger_auto_schema

from api_public.pagination import AffiliatePagination
from rest_framework.filters import OrderingFilter
from django_filters import rest_framework as filters

from api_public.permissions import IsPartner, staff_permission
from base.filters import CustomObjectsOrderingFilter
from base.views import AffiliateViewSet, AffiliateListModelMixin, AffiliateRetrieveModelMixin, AffiliateAPIView
from finances.bl import CollectDaysFlows
from finances.models import Flow
from finances.rest.filters import FlowFilter
from finances.rest.pagination import FlowTotalsPegination
from finances.rest.serializers import FlowSerializer, FlowDaySerializer, FlowParamsSerilizer
from permissions.consts import Block, Action


class FlowViewSet(AffiliateViewSet, AffiliateListModelMixin, AffiliateRetrieveModelMixin):
    """
    list:
    For staff user return a list of all the existing flows.
    For partner user return a list with flows which corresponds
    to promos created by current partner.

    retrieve:
    Return a single flow. Flow must exists in list for current user.
    """
    serializer_class = FlowSerializer
    queryset = Flow.objects.all()
    queryset_user_lookup_expr = 'partner__user'
    pagination_class = AffiliatePagination
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter)
    filter_class = FlowFilter
    ordering_fields = ('created_at', 'modified_at', 'amount')
    ordering = ('-modified_at', )

    permission_classes_by_action = {
        'list': [IsPartner, staff_permission(Block.FLOWS, Action.READ)],
        'retrieve': [IsPartner, staff_permission(Block.FLOWS, Action.READ)]
    }


class FlowDaysView(AffiliateAPIView):
    """
    Список движений денежных средств аггрерированный по дням.
    """
    permission_classes = (IsPartner, staff_permission(Block.FLOWS, Action.READ))
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter)

    ordering_fields = ('date', 'amount')
    ordering = ('-date', )

    @swagger_auto_schema(query_serializer=FlowParamsSerilizer,
                         responses={200: FlowDaySerializer})
    def get(self, request):
        serializer = FlowParamsSerilizer(data=request.query_params)
        serializer.is_valid(raise_exception=True)

        flows = CollectDaysFlows(self.is_staff, request, serializer.validated_data).run()
        flows = CustomObjectsOrderingFilter().filter(request, flows, self)

        paginator = FlowTotalsPegination()
        page = paginator.paginate_queryset(flows, self.request, view=self)
        serializer = FlowDaySerializer(page, many=True)

        return paginator.get_paginated_response(serializer.data)
