from collections import OrderedDict, namedtuple
from decimal import Decimal

from rest_framework.response import Response

from api_public.pagination import AffiliatePagination
from finances.models import Flow
from users.models import Partner


class FlowTotalsPegination(AffiliatePagination):
    def paginate_queryset(self, flows, request, view=None):
        # TODO: при просмотре статистики по движениям дс админом, текущий баланс равен 0,
        # воможно требуется доработка бизнес логики
        last_flow = namedtuple('last_flow', ['balance'])(balance=Decimal(0))

        if hasattr(request.user, 'partner_profile'):
            partner = Partner.objects.get(user=request.user)
            real_last_flow = Flow.objects.filter(partner=partner).order_by('-created_at').first()
            if real_last_flow:
                last_flow = real_last_flow

        self.ending_balance = round(last_flow.balance, 2)
        self.count = len(flows)
        self.limit = self.get_limit(request)
        if self.limit is None:
            return None

        self.offset = self.get_offset(request)
        self.request = request

        if self.count == 0 or self.offset > self.count:
            return []
        return flows[self.offset:self.offset + self.limit]

    def get_paginated_response(self, data):
        return Response(OrderedDict([
            ('count', self.count),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('results', data),
            ('ending_balance', self.ending_balance)
        ]))
