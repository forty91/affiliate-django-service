from django_filters import rest_framework as filters

from api_public.filters import IsoDateTimeFilter
from base.help_texts import MODIFIED_AT_LTE, MODIFIED_AT_GTE, FLOW_TYPE_EXACT
from finances.models import Flow


class FlowFilter(filters.FilterSet):
    type = filters.CharFilter(field_name='type', help_text=FLOW_TYPE_EXACT)
    modified_at__lte = IsoDateTimeFilter(field_name='modified_at', lookup_expr='lte', help_text=MODIFIED_AT_LTE)
    modified_at__gte = IsoDateTimeFilter(field_name='modified_at', lookup_expr='gte', help_text=MODIFIED_AT_GTE)

    class Meta:
        model = Flow
        fields = ('type', 'modified_at')
