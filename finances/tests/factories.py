import random
from decimal import Decimal

import factory

from finances.consts import FlowType
from finances.models import Flow
from users.tests.factories import PartnerFactory


class FlowFactory(factory.django.DjangoModelFactory):
    type = factory.LazyAttribute(lambda x: FlowType.CREDITS)
    amount = factory.LazyAttribute(lambda x: Decimal(random.randrange(-5000, 7000))/100)
    partner = factory.SubFactory(PartnerFactory)
    description = ''
    balance = factory.LazyAttribute(lambda x: x.amount)

    class Meta:
        model = Flow
