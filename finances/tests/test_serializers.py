from datetime import datetime

from django.test import TestCase
from rest_framework.exceptions import ValidationError

from base.consts import DATE_FORMAT
from finances.rest.serializers import FlowParamsSerilizer


class FlowParamsSerilizerTestCase(TestCase):
    def test_date__gte_later_date__lte(self):
        long_ago = datetime(2018, 8, 8, 8, 8, 8)
        now = datetime(2018, 9, 9, 9, 9, 9)

        data = {
            'date__gte': now.strftime(DATE_FORMAT),
            'date__lte': long_ago.strftime(DATE_FORMAT),
        }

        serializer = FlowParamsSerilizer(data=data)

        with self.assertRaisesRegex(
                ValidationError,
                "Parameter date__gte should be lower or equal date__lte."
        ):
            serializer.is_valid(raise_exception=True)
