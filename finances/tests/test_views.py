import datetime

from django.urls import reverse
from freezegun import freeze_time
from rest_framework.status import HTTP_200_OK

from base.consts import DATE_FORMAT
from base.tests import AffiliateAPITestCase
from finances.consts import FlowType
from finances.tests.factories import FlowFactory
from finances.utils import datetime_to_date_repr


class FlowViewSetTestCase(AffiliateAPITestCase):
    def test_list_days(self):
        """
        Проверка что валидности сагрегированных по дням значений.
        """
        url = reverse('public:flow-list-days')
        long_long_ago = datetime.datetime(2018, 8, 8, 8, 8, 8)
        long_ago = datetime.datetime(2018, 8, 9, 9, 9, 9)
        now = datetime.datetime(2018, 8, 10, 10, 10, 10)

        with freeze_time(long_long_ago):
            flow1_1 = FlowFactory(partner=self.partner)
            flow1_1.balance = flow1_1.amount
            flow1_1.save()

        with freeze_time(long_long_ago + datetime.timedelta(minutes=1)):
            flow1_2 = FlowFactory(partner=self.partner)
            flow1_2.balance = flow1_1.balance + flow1_2.amount
            flow1_2.save()

        with freeze_time(long_long_ago + datetime.timedelta(minutes=2)):
            flow1_3 = FlowFactory(partner=self.partner)
            flow1_3.balance = flow1_2.balance + flow1_3.amount
            flow1_3.save()

        with freeze_time(long_ago):
            flow2_1 = FlowFactory(partner=self.partner)
            flow2_1.balance = flow1_3.balance + flow2_1.amount
            flow2_1.save()

        with freeze_time(long_ago + datetime.timedelta(minutes=1)):
            flow2_2 = FlowFactory(partner=self.partner)
            flow2_2.balance = flow2_1.balance + flow2_2.amount
            flow2_2.save()

        with freeze_time(now):
            flow3_1 = FlowFactory(partner=self.partner)
            flow3_1.balance = flow2_2.balance + flow3_1.amount
            flow3_1.save()

        response = self.client.get(
            url,
            data={
                'date__gte': long_long_ago.strftime(DATE_FORMAT),
                'date__lte': now.strftime(DATE_FORMAT),
            },
            HTTP_AUTHORIZATION='JWT %s' % self.partner_jwt_token)

        expected_data = {
            'count': 3,
            'next': None,
            'previous': None,
            'ending_balance': flow1_1.amount + flow1_2.amount + flow1_3.amount +
                              flow2_1.amount + flow2_2.amount + flow3_1.amount,
            'results': [
                {
                    'date': datetime_to_date_repr(now),
                    'type': FlowType.CREDITS,
                    'amount': str(round(flow3_1.amount, 2)),
                    'balance': str(round(flow1_1.amount + flow1_2.amount + flow1_3.amount +
                                         flow2_1.amount + flow2_2.amount + flow3_1.amount, 2)),
                },
                {
                    'date': datetime_to_date_repr(long_ago),
                    'type': FlowType.CREDITS,
                    'amount': str(round(flow2_1.amount + flow2_2.amount, 2)),
                    'balance': str(round(flow1_1.amount + flow1_2.amount + flow1_3.amount +
                                         flow2_1.amount + flow2_2.amount, 2)),
                },
                {
                    'date': datetime_to_date_repr(long_long_ago),
                    'type': FlowType.CREDITS,
                    'amount': str(round(flow1_1.amount + flow1_2.amount + flow1_3.amount, 2)),
                    'balance': str(round(flow1_1.amount + flow1_2.amount + flow1_3.amount, 2)),
                },
            ],
        }
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertEqual(response.data, expected_data)
