from decimal import Decimal

from django.db import models

from base.help_texts import FLOW_TYPE, FLOW_AMOUNT, PARTNER, FLOW_DESCRIPTION, MODIFIED_AT, FLOW_BALANCE, CREATED_AT
from base.models import AffiliateModel
from finances.consts import FlowType


class Flow(AffiliateModel):
    type = models.SlugField(choices=FlowType.choices, help_text=FLOW_TYPE)
    amount = models.DecimalField(default=Decimal(0), max_digits=12, decimal_places=4, help_text=FLOW_AMOUNT)
    partner = models.ForeignKey('users.Partner', null=True, on_delete=models.SET_NULL,
                                related_name='flows', help_text=PARTNER)
    description = models.TextField(default='', help_text=FLOW_DESCRIPTION)
    balance = models.DecimalField(default=Decimal(0), max_digits=12, decimal_places=4, help_text=FLOW_BALANCE)
    created_at = models.DateTimeField(auto_now_add=True, help_text=CREATED_AT)
    modified_at = models.DateTimeField(auto_now=True, help_text=MODIFIED_AT)
