#!/bin/sh

set -e

if [[ "$1" = 'server' ]]; then
#  exec python /app/manage.py runserver 0.0.0.0:8080 --settings=affiliate.settings.docker
    exec gunicorn affiliate.wsgi:application \
        --name affiliate \
        --bind 0.0.0.0:8080 \
        --workers=${GUNICORN_WORKERS} \
        --threads=${GUNICORN_THREADS}
fi

exec "$@"