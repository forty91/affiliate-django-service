from decimal import Decimal

from django.db import models

from base.consts import Currency
from base.help_texts import CYBERBET_ID, PLAYER, PAYMENT_AMOUNT, PAYMENT_STATUS, PAYMENT_DIRECTION, CURRENCY, \
    CREATED_AT, MODIFIED_AT, PAYMENT_IS_FIRST, PAYMENT_AMOUNT_EUR, CURRENCY_RATE
from base.models import AffiliateModel
from payments.consts import Status, Direction


class Payment(AffiliateModel):
    cyberbet_id = models.IntegerField(unique=True, null=False, help_text=CYBERBET_ID)
    player = models.ForeignKey('players.Player', on_delete=models.CASCADE, related_name='payments', help_text=PLAYER)
    amount = models.DecimalField(default=Decimal(0), max_digits=12, decimal_places=4, help_text=PAYMENT_AMOUNT)
    amount_eur = models.DecimalField(default=Decimal(0), max_digits=12, decimal_places=4, help_text=PAYMENT_AMOUNT_EUR)
    status = models.SlugField(null=True, choices=Status.choices, default=Status.PENDING, help_text=PAYMENT_STATUS)
    direction = models.SlugField(null=True, choices=Direction.choices, help_text=PAYMENT_DIRECTION)
    currency = models.SlugField(null=True, choices=Currency.choices, help_text=CURRENCY)
    currency_rate = models.DecimalField(default=Decimal(0), max_digits=12, decimal_places=4, help_text=CURRENCY_RATE)
    created_at = models.DateTimeField(auto_now_add=True, help_text=CREATED_AT)
    modified_at = models.DateTimeField(auto_now=True, help_text=MODIFIED_AT)
    is_first = models.BooleanField(default=False, help_text=PAYMENT_IS_FIRST)
