# Generated by Django 2.1.7 on 2019-02-26 11:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payments', '0004_auto_20190225_1553'),
    ]

    operations = [
        migrations.AlterField(
            model_name='payment',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, help_text='Creation UTC datetime.'),
        ),
        migrations.AlterField(
            model_name='payment',
            name='modified_at',
            field=models.DateTimeField(auto_now=True, help_text='Last modification UTC datetime.'),
        ),
    ]
