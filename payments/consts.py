from base.utils.enumerate import BaseEnumerate


class Status(BaseEnumerate):
    PENDING = 'pending'
    SUCCESS = 'success'
    DECLINE = 'decline'
    AWAITING = 'awaiting'
    CANCELED = 'canceled'
    REFUNDED = 'refunded'
    PROCESS = 'process'
    FAILED = 'failed'


class Direction(BaseEnumerate):
    INCOMING = 'incoming'
    OUTGOING = 'outgoing'


PAYMENT_COLLECTION = 'payments'


class PaymentAction(BaseEnumerate):
    CREATE = 'create'
    UPDATE = 'update'
