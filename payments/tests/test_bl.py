import datetime
from decimal import Decimal

from django.test import TestCase, tag
from freezegun import freeze_time
from mock import patch
from rest_framework.status import HTTP_200_OK

from base.consts import Currency
from base.utils.mongo import MongoLogging
from payments.bl import CreateUpdatePayment
from payments.consts import Direction, Status, PAYMENT_COLLECTION, PaymentAction
from payments.rest.serializers import PaymentSerializer
from payments.tests.factories import PaymentFactory
from players.tests.factories import PlayerFactory


class CreateUpdatePaymentTestCase(TestCase):
    def setUp(self):
        self.cyberbet_id = 123456
        self.player = PlayerFactory()
        self.amount = Decimal(100)
        self.direction = Direction.INCOMING
        self.currency = Currency.USD
        self.status = Status.PENDING
        self.seconds = 1540893894
        self.nanos = 440214540

    @patch('payments.bl.get_currency_rates', autospec=True)
    def test_is_first(self, get_currency_rates_mock):
        """
        Создаем payment1 в статусе pending. is_first = False
        Создаем payment2 в статусе pending. is_first = False
        Модифицируем payment2 статусом success. is_first = True
        Создаем payment3 со статусом success. is_first = False
        Cоздаем payment4 для другого player со статусом success. is_first = True
        """
        currency_rates = {'gbp': 0.8, 'eur': 1.0, 'usd': 1.1, 'rub': 74.4}
        get_currency_rates_mock.return_value = currency_rates

        validated_data = {
            "cyberbet_id": 111111,
            "player_cyberbet_id": self.player.cyberbet_id,
            "amount": self.amount,
            "status": self.status,
            "direction": self.direction,
            "currency": self.currency,
            "created_at": {
                "seconds": self.seconds,
                "nanos": self.nanos
            }
        }
        payment1, status = CreateUpdatePayment(validated_data).run()
        self.assertEqual(payment1.is_first, False)

        validated_data = {
            "cyberbet_id": 222222,
            "player_cyberbet_id": self.player.cyberbet_id,
            "amount": self.amount,
            "status": self.status,
            "direction": self.direction,
            "currency": self.currency,
            "created_at": {
                "seconds": self.seconds,
                "nanos": self.nanos
            }
        }
        payment2, status = CreateUpdatePayment(validated_data).run()
        self.assertEqual(payment2.is_first, False)

        validated_data = {
            "cyberbet_id": 222222,
            "status": Status.SUCCESS,
            "player_cyberbet_id": self.player.cyberbet_id,
            "created_at": {
                "seconds": self.seconds,
                "nanos": self.nanos
            }
        }
        payment2, status = CreateUpdatePayment(validated_data).run()
        self.assertEqual(payment2.is_first, True)

        validated_data = {
            "cyberbet_id": 333333,
            "player_cyberbet_id": self.player.cyberbet_id,
            "amount": self.amount,
            "status": Status.SUCCESS,
            "direction": self.direction,
            "currency": self.currency,
            "created_at": {
                "seconds": self.seconds,
                "nanos": self.nanos
            }
        }
        payment3, status = CreateUpdatePayment(validated_data).run()
        self.assertEqual(payment3.is_first, False)

        player = PlayerFactory()
        validated_data = {
            "cyberbet_id": 444444,
            "player_cyberbet_id": player.cyberbet_id,
            "amount": self.amount,
            "status": Status.SUCCESS,
            "direction": self.direction,
            "currency": self.currency,
            "created_at": {
                "seconds": self.seconds,
                "nanos": self.nanos
            }
        }
        payment3, status = CreateUpdatePayment(validated_data).run()
        self.assertEqual(payment3.is_first, True)

    @tag('local')
    @patch('payments.bl.get_currency_rates', autospec=True)
    def test_update_mongo_logging(self, get_currency_rates_mock):
        currency_rates = {'gbp': 0.8, 'eur': 1.0, 'usd': 1.1, 'rub': 74.4}
        get_currency_rates_mock.return_value = currency_rates

        now = datetime.datetime(2018, 10, 10, 10, 10, 10)
        player = PlayerFactory()
        payment = PaymentFactory(player=player)
        validated_data = {
            "cyberbet_id": payment.cyberbet_id,
            "player_cyberbet_id": player.cyberbet_id,
            "amount": self.amount,
            "status": self.status,
            "direction": self.direction,
            "currency": self.currency,
            "created_at": {
                "seconds": self.seconds,
                "nanos": self.nanos
            }
        }
        with freeze_time(now):
            payment, status = CreateUpdatePayment(validated_data).run()

        logged_data = PaymentSerializer(instance=payment).data
        logged_bet = MongoLogging().get(collection=PAYMENT_COLLECTION, data=logged_data)
        logged_bet.pop('_id', None)

        self.assertEqual(status, HTTP_200_OK)
        self.assertEqual(logged_bet, dict(**logged_data, **{'action': PaymentAction.UPDATE, 'timestamp': now}))
