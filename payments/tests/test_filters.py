from django.test import TestCase
from freezegun import freeze_time

from base.utils.datetime import utc_datetime
from payments.models import Payment
from payments.rest.filters import PaymentFilter
from payments.tests.factories import PaymentFactory
from players.tests.factories import PlayerFactory


class PaymentFilterTestCase(TestCase):
    def test_is_first(self):
        player1 = PlayerFactory()
        player2 = PlayerFactory()

        with freeze_time(utc_datetime(2018, 10, 10, 10, 10, 10)):
            payment1_1 = PaymentFactory(player=player1)
        with freeze_time(utc_datetime(2018, 8, 8, 8, 8, 8)):
            payment1_2 = PaymentFactory(player=player1, is_first=True)
            payment2_1 = PaymentFactory(player=player2, is_first=True)
        with freeze_time(utc_datetime(2018, 9, 9, 9, 9, 9)):
            payment1_3 = PaymentFactory(player=player1)
            payment2_2 = PaymentFactory(player=player2)

        self.assertEqual(
            set(PaymentFilter({'is_first': True}, Payment.objects.all()).qs),
            {payment1_2, payment2_1}
        )
