import datetime
from decimal import Decimal

from django.urls import reverse
from freezegun import freeze_time
from mock import patch
from rest_framework.status import HTTP_201_CREATED, HTTP_200_OK

from base.consts import Currency
from base.tests import AffiliateAPITestCase
from base.utils.currency import aggregate_to_one_currency
from payments.consts import Direction, Status
from payments.models import Payment
from api_public.serializers import PlayerNameSerializer
from payments.tests.factories import PaymentFactory
from players.tests.factories import PlayerFactory
from base.utils.datetime import utc_datetime_to_iso
from promos.tests.factories import PromoFactory


class PlayerViewSetTestCase(AffiliateAPITestCase):
    @patch('payments.bl.get_currency_rates', autospec=True)
    def test_create_and_modify(self, get_currency_rates_mock):
        """
        В cyberbet происходит создание платежа со стороны клиента на счет, затем платеж подтверждается.
        """
        currency_rates = {'gbp': 0.8, 'eur': 1.0, 'usd': 1.1, 'rub': 74.4}
        get_currency_rates_mock.return_value = currency_rates

        long_ago = datetime.datetime(2018, 9, 9, 9, 9, 9)
        now = datetime.datetime(2018, 10, 10, 10, 10, 10)
        url = reverse('public:payment-list')
        cyberbet_id = 123456
        player = PlayerFactory()
        amount = Decimal(100)
        direction = Direction.INCOMING
        currency = Currency.USD

        create_status = Status.PENDING
        create_data = {
            "cyberbet_id": cyberbet_id,
            "player_cyberbet_id": player.cyberbet_id,
            "amount": amount,
            "status": create_status,
            "direction": direction,
            "currency": currency,
        }

        with freeze_time(long_ago):
            response = self.client.post(url, create_data, format='json',
                                        HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token)
        created_payment = Payment.objects.get(cyberbet_id=cyberbet_id)

        expected_data = {
            'id': created_payment.uuid,
            'cyberbet_id': cyberbet_id,
            'player': PlayerNameSerializer(instance=player).data,
            'amount': '%.2f' % amount,
            'amount_eur': '%.2f' % (amount / Decimal(currency_rates[currency])),
            'status': create_status,
            'direction': direction,
            'currency': currency,
            'created_at': utc_datetime_to_iso(long_ago),
            'modified_at': utc_datetime_to_iso(long_ago),
            'is_first': False
        }
        self.assertEqual(response.status_code, HTTP_201_CREATED)
        self.assertEqual(response.data, expected_data)

        success_status = Status.SUCCESS
        success_data = {
            "cyberbet_id": cyberbet_id,
            "player_cyberbet_id": player.cyberbet_id,
            "amount": amount,
            "status": success_status,
            "direction": direction,
            "currency": currency,
        }

        with freeze_time(now):
            response = self.client.post(url, success_data, format='json',
                                        HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token)

        expected_data = {
            'id': created_payment.uuid,
            'cyberbet_id': cyberbet_id,
            'player': PlayerNameSerializer(instance=player).data,
            'amount': '%.2f' % amount,
            'amount_eur': '%.2f' % (amount / Decimal(currency_rates[currency])),
            'status': success_status,
            'direction': direction,
            'currency': currency,
            'created_at': utc_datetime_to_iso(long_ago),
            'modified_at': utc_datetime_to_iso(now),
            'is_first': True
        }
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertEqual(response.data, expected_data)

    def test_get_sum(self):
        """
        Проверим корректность взятия сумм платежей для текущего партнера с фильтрацией по игроку и без
        """
        PaymentFactory(amount=543.21, direction=Direction.INCOMING)
        PaymentFactory(amount=123.45, direction=Direction.OUTGOING)

        player1 = PlayerFactory(promo=PromoFactory(partner=self.partner))
        payment1_1 = PaymentFactory(amount=10.01, direction=Direction.INCOMING, currency=Currency.EUR, player=player1)
        payment1_2 = PaymentFactory(amount=5, direction=Direction.INCOMING, currency=Currency.RUB, player=player1)
        payment1_3 = PaymentFactory(amount=3, direction=Direction.OUTGOING, currency=Currency.EUR, player=player1)
        payment1_4 = PaymentFactory(amount=7.2, direction=Direction.OUTGOING, currency=Currency.RUB, player=player1)
        player2 = PlayerFactory(promo=PromoFactory(partner=self.partner))
        payment2_1 = PaymentFactory(amount=3.2, direction=Direction.INCOMING, currency=Currency.EUR, player=player2)
        payment2_2 = PaymentFactory(amount=4.99, direction=Direction.INCOMING, currency=Currency.RUB, player=player2)
        payment2_3 = PaymentFactory(amount=11.012, direction=Direction.INCOMING, currency=Currency.GBP, player=player2)
        payment2_4 = PaymentFactory(amount=1.01, direction=Direction.OUTGOING, currency=Currency.EUR, player=player2)
        payment2_5 = PaymentFactory(amount=2, direction=Direction.OUTGOING, currency=Currency.RUB, player=player2)
        payment2_6 = PaymentFactory(amount=2.999, direction=Direction.OUTGOING, currency=Currency.GBP, player=player2)

        url = reverse('public:payment-sum')
        response = self.client.get(url, format='json', HTTP_AUTHORIZATION='JWT %s' % self.partner_jwt_token)

        expected_data = {
            'incoming_sum': {
                Currency.EUR: payment1_1.amount + payment2_1.amount,
                Currency.RUB: payment1_2.amount + payment2_2.amount,
                Currency.GBP: payment2_3.amount,
                Currency.USD: 0.0,
            },
            'outgoing_sum': {
                Currency.EUR: payment1_3.amount + payment2_4.amount,
                Currency.RUB: payment1_4.amount + payment2_5.amount,
                Currency.GBP: payment2_6.amount,
                Currency.USD: 0.0,
            },
            'incoming_sum_eur': aggregate_to_one_currency(
                {
                    Currency.EUR: payment1_1.amount + payment2_1.amount,
                    Currency.RUB: payment1_2.amount + payment2_2.amount,
                    Currency.GBP: payment2_3.amount,
                    Currency.USD: 0.0,
                }
            ),
            'outgoing_sum_eur': aggregate_to_one_currency(
                {
                    Currency.EUR: payment1_3.amount + payment2_4.amount,
                    Currency.RUB: payment1_4.amount + payment2_5.amount,
                    Currency.GBP: payment2_6.amount,
                    Currency.USD: 0.0,
                }
            )
        }
        self.assertEqual(response.data, expected_data)

        url = "%s?player=%s" % (reverse('public:payment-sum'), player1.uuid)
        response = self.client.get(url, format='json', HTTP_AUTHORIZATION='JWT %s' % self.partner_jwt_token)

        expected_data = {
            'incoming_sum': {
                Currency.EUR: payment1_1.amount,
                Currency.RUB: payment1_2.amount,
                Currency.GBP: 0.0,
                Currency.USD: 0.0,
            },
            'outgoing_sum': {
                Currency.EUR: payment1_3.amount,
                Currency.RUB: payment1_4.amount,
                Currency.GBP: 0.0,
                Currency.USD: 0.0,
            },
            'incoming_sum_eur': aggregate_to_one_currency(
                {
                    Currency.EUR: payment1_1.amount,
                    Currency.RUB: payment1_2.amount,
                    Currency.GBP: 0.0,
                    Currency.USD: 0.0,
                }
            ),
            'outgoing_sum_eur': aggregate_to_one_currency(
                {
                    Currency.EUR: payment1_3.amount,
                    Currency.RUB: payment1_4.amount,
                    Currency.GBP: 0.0,
                    Currency.USD: 0.0,
                }
            )
        }
        self.assertEqual(response.data, expected_data)
