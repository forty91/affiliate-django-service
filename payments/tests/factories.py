import random
from decimal import Decimal

import factory

from payments.consts import Status, Direction
from payments.models import Payment
from base.consts import Currency
from players.tests.factories import PlayerFactory


class PaymentFactory(factory.django.DjangoModelFactory):
    cyberbet_id = factory.LazyAttribute(lambda x: random.randint(0, 1000000))
    player = factory.SubFactory(PlayerFactory)
    amount = Decimal(100)
    amount_eur = Decimal(100)
    status = Status.SUCCESS
    direction = Direction.INCOMING
    currency = Currency.EUR
    currency_rate = Decimal(1)
    is_first = False

    class Meta:
        model = Payment
