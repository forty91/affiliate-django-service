from django_filters import rest_framework as filters

from api_public.filters import IsoDateTimeFilter
from base.help_texts import CYBERBET_ID_EXACT, PLAYER_EXACT, AMOUNT_EXACT, AMOUNT_LTE, AMOUNT_GTE, PAYMENT_STATUS_EXACT, \
    PAYMENT_DIRECTION_EXACT, CURRENCY_EXACT, CREATED_AT_LTE, CREATED_AT_GTE, MODIFIED_AT_LTE, MODIFIED_AT_GTE, \
    PROMO_EXACT, IS_FIRST_BOOL, PROMO_NAME_ICONTAINS
from payments.models import Payment


class PaymentFilter(filters.FilterSet):
    cyberbet_id = filters.CharFilter(field_name='cyberbet_id', help_text=CYBERBET_ID_EXACT)
    player = filters.UUIDFilter(field_name='player', help_text=PLAYER_EXACT)
    promo = filters.UUIDFilter(field_name='player__promo', help_text=PROMO_EXACT)
    promo__name = filters.CharFilter(field_name='player__promo', lookup_expr='name__icontains', help_text=PROMO_NAME_ICONTAINS)
    amount = filters.NumberFilter(field_name='amount', help_text=AMOUNT_EXACT)
    amount__lte = filters.NumberFilter(field_name='amount', lookup_expr='lte', help_text=AMOUNT_LTE)
    amount__gte = filters.NumberFilter(field_name='amount', lookup_expr='gte', help_text=AMOUNT_GTE)
    status = filters.CharFilter(field_name='status', help_text=PAYMENT_STATUS_EXACT)
    direction = filters.CharFilter(field_name='direction', help_text=PAYMENT_DIRECTION_EXACT)
    currency = filters.CharFilter(field_name='currency', help_text=CURRENCY_EXACT)
    created_at__lte = IsoDateTimeFilter(field_name='created_at', lookup_expr='lte', help_text=CREATED_AT_LTE)
    created_at__gte = IsoDateTimeFilter(field_name='created_at', lookup_expr='gte', help_text=CREATED_AT_GTE)
    modified_at__lte = IsoDateTimeFilter(field_name='modified_at', lookup_expr='lte', help_text=MODIFIED_AT_LTE)
    modified_at__gte = IsoDateTimeFilter(field_name='modified_at', lookup_expr='gte', help_text=MODIFIED_AT_GTE)
    is_first = filters.BooleanFilter(field_name='is_first', help_text=IS_FIRST_BOOL)

    class Meta:
        model = Payment
        fields = ('cyberbet_id', 'player', 'amount', 'status', 'direction', 'currency', 'is_first')
