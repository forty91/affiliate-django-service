from collections import namedtuple

from drf_yasg.utils import swagger_auto_schema
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.filters import OrderingFilter
from django_filters import rest_framework as filters
from rest_framework.status import HTTP_200_OK, HTTP_201_CREATED

from api_public.pagination import AffiliatePagination
from api_public.permissions import IsPartner, staff_permission
from base.utils.currency import aggregate_to_one_currency
from base.utils.db import aggregate_currencies_sum
from base.views import AffiliateViewSet, AffiliateListModelMixin, AffiliateRetrieveModelMixin
from payments.bl import CreateUpdatePayment
from payments.consts import Direction, Status
from payments.rest.filters import PaymentFilter
from payments.rest.serializers import PaymentSerializer, PaymentCreateModifySerializer, PaymentSumSerializer
from payments.models import Payment
from permissions.consts import Block, Action


class PaymentViewSet(AffiliateViewSet, AffiliateListModelMixin, AffiliateRetrieveModelMixin):
    """
    list:
    For staff user return a list of all the existing payments.
    For partner user return a list with payments which corresponds
    to players who went on promo created by current partner.

    retrieve:
    Return a single payment. Payment must exists in list for current user.

    create:
    Create a new payment instance. Allow for users with staff permissions only.

    sum:
    Return payments sums by filtered with query parameters queryset.
    """
    serializer_class = PaymentSerializer
    queryset = Payment.objects.all()
    queryset_user_lookup_expr = 'player__promo__partner__user'
    pagination_class = AffiliatePagination
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter)
    filter_class = PaymentFilter
    ordering_fields = ('modified_at', 'created_at')
    ordering = ('-modified_at', )

    permission_classes_by_action = {
        'create': [staff_permission(Block.PAYMENTS, Action.CREATE)],
        'list': [IsPartner, staff_permission(Block.PAYMENTS, Action.READ)],
        'sum': [IsPartner, staff_permission(Block.PAYMENTS, Action.READ)],
        'retrieve': [IsPartner, staff_permission(Block.PAYMENTS, Action.READ)]
    }

    @swagger_auto_schema(request_body=PaymentCreateModifySerializer,
                         responses={HTTP_200_OK: PaymentSerializer, HTTP_201_CREATED: PaymentSerializer})
    def create(self, request):
        serializer = PaymentCreateModifySerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        payment, status = CreateUpdatePayment(serializer.validated_data).run()
        return Response(data=PaymentSerializer(instance=payment).data,
                        status=status)

    @swagger_auto_schema(responses={HTTP_200_OK: PaymentSumSerializer})
    @action(methods=['get'], detail=False, url_path='sum')
    def sum(self, request, *args, **kwargs):
        self.queryset_user_filter = {self.queryset_user_lookup_expr: self.request.user}
        payments = self.filter_queryset(self.get_queryset())
        successed_payments = payments.filter(status=Status.SUCCESS)
        incoming_payments = successed_payments.filter(direction=Direction.INCOMING)
        outgoing_payments = successed_payments.filter(direction=Direction.OUTGOING)

        aggregated_data = {
            'incoming_sum': aggregate_currencies_sum(incoming_payments, 'amount'),
            'outgoing_sum': aggregate_currencies_sum(outgoing_payments, 'amount')
        }

        aggregated_data_eur = {
            _sum: aggregate_to_one_currency(aggregated_data[_sum])
            for _sum in ('incoming_sum', 'outgoing_sum')
        }

        instance = namedtuple('instance', ['incoming_sum', 'outgoing_sum',
                                           'incoming_sum_eur', 'outgoing_sum_eur'])(
            incoming_sum=aggregated_data['incoming_sum'],
            outgoing_sum=aggregated_data['outgoing_sum'],
            incoming_sum_eur=aggregated_data_eur['incoming_sum'],
            outgoing_sum_eur=aggregated_data_eur['outgoing_sum'],
        )
        return Response(data=PaymentSumSerializer(instance=instance).data,
                        status=HTTP_200_OK)
