from decimal import Decimal

from drf_queryfields import QueryFieldsMixin
from rest_framework import serializers

from base.help_texts import CYBERBET_ID, PAYMENT_AMOUNT, PAYMENT_STATUS, PAYMENT_DIRECTION, CURRENCY, \
    PAYMENT_INCOMING_SUM, PAYMENT_OUTGOING_SUM, PAYMENT_INCOMING_SUM_EUR, PAYMENT_OUTGOING_SUM_EUR, PAYMENT_AMOUNT_EUR
from payments.consts import Status, Direction
from payments.models import Payment
from api_public.validators import check_player_cyberbet_id
from base.consts import Currency
from api_public.serializers import PlayerNameSerializer


class PaymentSerializer(serializers.ModelSerializer):
    player = PlayerNameSerializer()
    amount = serializers.DecimalField(max_digits=9, decimal_places=2, min_value=Decimal(0), help_text=PAYMENT_AMOUNT)
    amount_eur = serializers.DecimalField(max_digits=9, decimal_places=2, min_value=Decimal(0),
                                          help_text=PAYMENT_AMOUNT_EUR)

    class Meta:
        model = Payment
        fields = ('id', 'cyberbet_id', 'player', 'amount', 'amount_eur', 'status',
                  'direction', 'currency', 'created_at', 'modified_at', 'is_first')


class PaymentCreateModifySerializer(QueryFieldsMixin, serializers.Serializer):
    cyberbet_id = serializers.IntegerField(allow_null=False, help_text=CYBERBET_ID)
    player_cyberbet_id = serializers.IntegerField(min_value=1, validators=[check_player_cyberbet_id],
                                                  help_text=CYBERBET_ID)
    amount = serializers.DecimalField(max_digits=12, decimal_places=4, min_value=Decimal(0), required=False,
                                      help_text=PAYMENT_AMOUNT)
    status = serializers.ChoiceField(choices=Status.choices, required=False, allow_null=False, help_text=PAYMENT_STATUS)
    direction = serializers.ChoiceField(choices=Direction.choices, required=False, help_text=PAYMENT_DIRECTION)
    currency = serializers.ChoiceField(choices=Currency.choices, required=False, help_text=CURRENCY)


class PaymentSumSerializer(serializers.Serializer):
    incoming_sum = serializers.DictField(help_text=PAYMENT_INCOMING_SUM)
    outgoing_sum = serializers.DictField(help_text=PAYMENT_OUTGOING_SUM)
    incoming_sum_eur = serializers.FloatField(help_text=PAYMENT_INCOMING_SUM_EUR)
    outgoing_sum_eur = serializers.FloatField(help_text=PAYMENT_OUTGOING_SUM_EUR)
