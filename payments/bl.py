from decimal import Decimal

from rest_framework.status import HTTP_200_OK, HTTP_201_CREATED

from base.utils.currency import get_currency_rates
from base.utils.mongo import MongoLogging
from payments.consts import PAYMENT_COLLECTION, PaymentAction, Status
from payments.models import Payment
from payments.rest.serializers import PaymentSerializer
from players.models import Player


class CreateUpdatePayment:
    def __init__(self, validated_data):
        self.validated_data = validated_data
        self.action = PaymentAction.CREATE

    def run(self):
        cyberbet_id = self.validated_data['cyberbet_id']
        player = Player.objects.get(cyberbet_id=self.validated_data['player_cyberbet_id'])
        player_payments = player.payments.all()

        is_first = False
        if self.validated_data.get('status') == Status.SUCCESS \
                and not player_payments.filter(is_first=True).exists():
            is_first = True

        if player_payments.filter(cyberbet_id=cyberbet_id).exists():
            self.action = PaymentAction.UPDATE

        payment, created = Payment.objects.get_or_create(
            cyberbet_id=cyberbet_id,
            player=player
        )

        for attr in ('status', 'amount', 'direction', 'currency'):
            attr_value = self.validated_data.get(attr)
            if attr_value is not None:
                setattr(payment, attr, attr_value)

        payment = self.calculate_attrs(payment)
        payment.is_first = is_first
        payment.save()

        MongoLogging().log(collection=PAYMENT_COLLECTION, action=self.action,
                           data=PaymentSerializer(instance=payment).data)

        status = {
            PaymentAction.CREATE: HTTP_201_CREATED,
            PaymentAction.UPDATE: HTTP_200_OK
        }[self.action]

        return payment, status

    def calculate_attrs(self, payment):
        """
        Расчет вычисляемых полей платежа.
        """
        if self.action == PaymentAction.CREATE:
            currency_rates = get_currency_rates()
            payment.currency_rate = Decimal(currency_rates[payment.currency])

        payment.amount_eur = payment.amount / payment.currency_rate

        return payment

