from django.core.exceptions import ValidationError
from django.conf import settings

from base.utils.mongo import MongoLogging
from players.consts import PLAYER_COLLECTION, PlayerAction
from players.models import Player
from players.rest.serializers import PlayerSerializer
from promos.models import Promo


class CreatePlayer:
    def __init__(self, validated_data):
        self.validated_data = validated_data

    def run(self):
        try:
            promo = Promo.objects.get(pk=self.validated_data['promo'])
        except (Promo.DoesNotExist, ValidationError, KeyError) as e:
            promo = Promo.objects.default()

        player = Player.objects.create(
            cyberbet_id=self.validated_data['cyberbet_id'],
            creation_params=self.validated_data['creation_params']
        )

        player.promo = promo
        player.save()

        try:
            create_player_knock_url = promo.knock_url.format(**player.creation_params)
        except KeyError as e:
            create_player_knock_url = ''

        if create_player_knock_url:
            settings.THREAD_POOL_REQUESTS.get(create_player_knock_url)

        MongoLogging().log(collection=PLAYER_COLLECTION, action=PlayerAction.CREATE,
                           data=PlayerSerializer(instance=player).data)

        return player
