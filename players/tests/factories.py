import random

import factory

from players.models import Player
from promos.tests.factories import PromoFactory


class PlayerFactory(factory.django.DjangoModelFactory):
    promo = factory.SubFactory(PromoFactory)
    cyberbet_id = factory.LazyAttribute(lambda x: random.randint(0, 1000000))

    class Meta:
        model = Player
