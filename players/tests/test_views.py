import random

from django.urls import reverse
from freezegun import freeze_time
from rest_framework.status import HTTP_201_CREATED

from base.tests import AffiliateAPITestCase
from players.models import Player
from api_public.serializers import PromoNameSerializer
from promos.models import Promo
from promos.tests.factories import PromoFactory
from base.utils.datetime import utc_datetime_to_iso, utc_datetime


class PlayerViewSetTestCase(AffiliateAPITestCase):
    def test_create(self):
        promo = PromoFactory()
        cyberbet_id = random.randint(0, 1000000)
        now = utc_datetime(2018, 10, 10, 10, 10, 10)
        creation_params = {'ololo': 123, 'trololo': '321', 'cid': '1234567890123456789012345'}

        url = reverse('public:player-list')
        data = {
            "promo": promo.uuid,
            "cyberbet_id": cyberbet_id,
            "creation_params": creation_params
        }

        with freeze_time(now):
            response = self.client.post(url, data, format='json',
                                        HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token)

        created_player = Player.objects.get(cyberbet_id=cyberbet_id)

        expected_data = {
            'id': created_player.uuid,
            'promo': PromoNameSerializer(instance=promo).data,
            'cyberbet_id': cyberbet_id,
            'creation_params': creation_params,
            'created_at': utc_datetime_to_iso(now),
        }

        self.assertEqual(response.status_code, HTTP_201_CREATED)
        self.assertEqual(response.data, expected_data)

    def test_create_only_cyberbet_id(self):
        cyberbet_id = random.randint(0, 1000000)
        now = utc_datetime(2018, 10, 10, 10, 10, 10)

        url = reverse('public:player-list')
        data = {
            "cyberbet_id": cyberbet_id,
        }

        with freeze_time(now):
            response = self.client.post(url, data, format='json',
                                        HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token)

        created_player = Player.objects.get(cyberbet_id=cyberbet_id)

        expected_data = {
            'id': created_player.uuid,
            'promo': PromoNameSerializer(instance=Promo.objects.default()).data,
            'cyberbet_id': cyberbet_id,
            'creation_params': {},
            'created_at': utc_datetime_to_iso(now),
        }

        self.assertEqual(response.status_code, HTTP_201_CREATED)
        self.assertEqual(response.data, expected_data)

    def test_create_incorrect_promo(self):
        now = utc_datetime(2018, 10, 10, 10, 10, 10)
        promo = 'SHIT'
        cyberbet_id = random.randint(0, 1000000)

        url = reverse('public:player-list')
        data = {
            "promo": promo,
            "cyberbet_id": cyberbet_id,
        }

        with freeze_time(now):
            response = self.client.post(url, data, format='json',
                                        HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token)
        created_player = Player.objects.get(cyberbet_id=cyberbet_id)

        expected_data = {
            'id': created_player.uuid,
            'promo': PromoNameSerializer(instance=Promo.objects.default()).data,
            'cyberbet_id': cyberbet_id,
            'creation_params': {},
            'created_at': utc_datetime_to_iso(now),
        }

        self.assertEqual(response.status_code, HTTP_201_CREATED)
        self.assertEqual(response.data, expected_data)
