import datetime
import random
from unittest.mock import patch

from django.conf import settings
from django.test import TestCase, tag
from freezegun import freeze_time

from base.utils.mongo import MongoLogging
from players.bl import CreatePlayer
from players.consts import PLAYER_COLLECTION, PlayerAction
from players.rest.serializers import PlayerSerializer
from promos.tests.factories import PromoFactory


class CreatePlayerTestCase(TestCase):
    @patch.object(settings.THREAD_POOL_REQUESTS, 'get', autospec=True)
    def check_knock(self, THREAD_POOL_REQUESTS_get_mock):
        knock_url = 'http://blah.org?/cid={click_id}&param=123&some={some}'
        promo = PromoFactory(knock_url=knock_url)
        cyberbet_id = random.randint(0, 1000000)
        created_at = 1543937335

        CreatePlayer({
            'promo': promo.uuid,
            'cyberbet_id': cyberbet_id,
            'created_at': created_at,
            'creation_params': {'spam': 'egg', 'click_id': 123, 'some': 'emos'}
        }).run()

        create_player_knock_url = 'http://blah.org?/cid=123&param=123&some=emos'
        THREAD_POOL_REQUESTS_get_mock.assert_called_with(create_player_knock_url)

    @tag('local')
    def test_create_mongo_logging(self):
        now = datetime.datetime(2018, 10, 10, 10, 10, 10)
        promo = PromoFactory()
        cyberbet_id = random.randint(0, 1000000)
        created_at = 1543937335
        validated_data = {
            'promo': promo.uuid,
            'cyberbet_id': cyberbet_id,
            'created_at': created_at,
            'creation_params': {'spam': 'egg', 'click_id': 123, 'some': 'emos'}
        }
        with freeze_time(now):
            player = CreatePlayer(validated_data).run()

        logged_data = PlayerSerializer(instance=player).data
        logged_bet = MongoLogging().get(collection=PLAYER_COLLECTION, data=logged_data)
        logged_bet.pop('_id', None)

        self.assertEqual(logged_bet, dict(**logged_data, **{'action': PlayerAction.CREATE, 'timestamp': now}))
