from base.utils.enumerate import BaseEnumerate

PLAYER_COLLECTION = 'players'


class PlayerAction(BaseEnumerate):
    CREATE = 'create'

