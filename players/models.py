from django.db import models
from django.contrib.postgres.fields import JSONField

from base.help_texts import PROMO, CYBERBET_ID, CREATED_AT, CREATION_PARAMS
from base.models import AffiliateModel
from base.utils.migrations import get_default_promo_uuid
from promos.models import Promo


class Player(AffiliateModel):
    promo = models.ForeignKey('promos.Promo', default=get_default_promo_uuid,
                              on_delete=models.SET_DEFAULT, related_name='players', help_text=PROMO)
    cyberbet_id = models.IntegerField(unique=True, null=False, help_text=CYBERBET_ID)
    created_at = models.DateTimeField(auto_now_add=True, help_text=CREATED_AT)
    creation_params = JSONField(default=dict(), help_text=CREATION_PARAMS)
