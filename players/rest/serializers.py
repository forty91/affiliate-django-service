from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from base.help_texts import PROMO, CREATION_PARAMS, CYBERBET_ID
from players.models import Player
from api_public.serializers import PromoNameSerializer


class PlayerSerializer(serializers.ModelSerializer):
    promo = PromoNameSerializer()

    class Meta:
        model = Player
        fields = ('id', 'promo', 'cyberbet_id', 'created_at', 'creation_params')


class PlayerCreateSerializer(serializers.Serializer):
    promo = serializers.CharField(required=False, allow_null=False, help_text=PROMO)
    creation_params = serializers.JSONField(required=False, allow_null=False, default=dict(), help_text=CREATION_PARAMS)
    cyberbet_id = serializers.IntegerField(
        allow_null=False, help_text=CYBERBET_ID,
        validators=[UniqueValidator(
            queryset=Player.objects.all(),
            message='cyberbet_id of player should be unique.'
        )]
    )
