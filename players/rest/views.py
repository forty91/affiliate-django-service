from rest_framework.response import Response
from rest_framework.status import HTTP_201_CREATED
from rest_framework.filters import OrderingFilter
from django_filters import rest_framework as filters

from api_public.pagination import AffiliatePagination
from api_public.permissions import IsPartner, staff_permission
from base.views import AffiliateViewSet, AffiliateListModelMixin, AffiliateRetrieveModelMixin
from permissions.consts import Block, Action
from players.bl import CreatePlayer
from players.models import Player
from players.rest.filters import PlayerFilter
from players.rest.serializers import PlayerSerializer, PlayerCreateSerializer


class PlayerViewSet(AffiliateViewSet, AffiliateListModelMixin, AffiliateRetrieveModelMixin):
    """
    list:
    For staff user return a list of all the existing players.
    For partner user return a list with players who went on promo created by current partner.

    retrieve:
    Return a single player. Player must exists in list for current user.

    create:
    Create a new player instance. Allow for users with staff permissions only.
    """
    serializer_class = PlayerSerializer
    queryset = Player.objects.all()
    queryset_user_lookup_expr = 'promo__partner__user'
    pagination_class = AffiliatePagination
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter)
    filter_class = PlayerFilter
    ordering_fields = ('created_at', )
    ordering = ('-created_at', )

    permission_classes_by_action = {
        'create': [staff_permission(Block.PLAYERS, Action.CREATE)],
        'list': [IsPartner, staff_permission(Block.PLAYERS, Action.READ)],
        'retrieve': [IsPartner, staff_permission(Block.PLAYERS, Action.READ)]
    }

    def create(self, request):
        serializer = PlayerCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        player = CreatePlayer(serializer.validated_data).run()
        return Response(data=PlayerSerializer(instance=player).data,
                        status=HTTP_201_CREATED)
