from django_filters import rest_framework as filters

from api_public.filters import IsoDateTimeFilter
from base.help_texts import PROMO_EXACT, PROMO_NAME_ICONTAINS, CYBERBET_ID_EXACT, CREATED_AT_LTE, CREATED_AT_GTE
from players.models import Player


class PlayerFilter(filters.FilterSet):
    promo = filters.UUIDFilter(field_name='promo', help_text=PROMO_EXACT)
    promo__name = filters.CharFilter(field_name='promo', lookup_expr='name__icontains', help_text=PROMO_NAME_ICONTAINS)
    cyberbet_id = filters.CharFilter(field_name='cyberbet_id', help_text=CYBERBET_ID_EXACT)
    created_at__lte = IsoDateTimeFilter(field_name='created_at', lookup_expr='lte', help_text=CREATED_AT_LTE)
    created_at__gte = IsoDateTimeFilter(field_name='created_at', lookup_expr='gte', help_text=CREATED_AT_GTE)

    class Meta:
        model = Player
        fields = ('promo', 'cyberbet_id')
