import requests

from io import BytesIO
from urllib.parse import urljoin

from django.conf import settings
from rest_framework.status import HTTP_200_OK, HTTP_503_SERVICE_UNAVAILABLE

from base.exceptions import affiliate_api_exception


def upload_static_to_s3_service(file, category):
    try:
        response = requests.post(
            settings.S3_UPLOAD_SERVICE_URL,
            files={'file': file},
            data={'path': category}
        )
    except requests.exceptions.ConnectionError as e:
        raise affiliate_api_exception(status=HTTP_503_SERVICE_UNAVAILABLE,
                                      detail='S3 upload service unavailable for a while.',
                                      code='service unavailable')
    if response.status_code != HTTP_200_OK:
        raise affiliate_api_exception(status=HTTP_503_SERVICE_UNAVAILABLE,
                                      detail='S3 upload service unavailable for a while.',
                                      code='service unavailable')

    link = response.json()['path']
    return link


def download_static_from_s3_service(link):
    response = requests.get(
        urljoin(settings.AFFILIATE_STATIC_SERVICE_URL, 'download/direct/%s' % link),
        stream=True
    )
    f = BytesIO(response.content)
    return f
