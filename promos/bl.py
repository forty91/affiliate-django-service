from collections import namedtuple
from io import BytesIO

from rest_framework import serializers

from base.bl import UpdateEntity, DestroyEntity
from base.utils.clickhouse import ClickhouseClient
from base.utils.datetime import datetime_now, EPOCH_DATETIME
from base.utils.mongo import MongoLogging
from promos.consts import PROMO_COLLECTION, PromoAction, DIRECT_LINK_COLLECTION, DirectLinkAction, BANNER_COLLECTION, \
    BannerAction, LandingAction, LANDING_COLLECTION, S3StaticAction, S3_STATIC_COLLECTION, TrafficActionType
from promos.models import Promo, DirectLink, S3Static, Banner, Landing
from promos.rest.serializers import PromoSerializer, DirectLinkSerializer, BannerSerializer, LandingSerializer, \
    S3StaticSerializer
from promos.utils import upload_static_to_s3_service, download_static_from_s3_service
from traffic.bl import GetTraffic


class CreatePromo:
    def __init__(self, request, validated_data):
        self.request = request
        self.validated_data = validated_data

    def validate(self):
        request_user = self.request.user
        if hasattr(request_user, 'partner_profile') and request_user != self.validated_data['partner'].user:
            raise serializers.ValidationError(
                {
                    'partner': ['Partner can create promo only for himself.'],
                }
            )

    def run(self):
        self.validate()
        promo = Promo.objects.create(**self.validated_data)

        MongoLogging().log(collection=PROMO_COLLECTION, action=PromoAction.CREATE,
                           data=PromoSerializer(instance=promo).data)
        return promo


class UpdatePromo(UpdateEntity):
    def run(self):
        promo = super().run()
        MongoLogging().log(collection=PROMO_COLLECTION, action=PromoAction.UPDATE,
                           data=PromoSerializer(instance=promo).data)
        return promo


class DestroyPromo(DestroyEntity):
    def run(self):
        super().run()
        MongoLogging().log(collection=PROMO_COLLECTION, action=PromoAction.DESTROY,
                           data=PromoSerializer(instance=self.entity).data)


class CreateDirectLink:
    def __init__(self, request, validated_data):
        self.request = request
        self.promo = validated_data['promo']
        self.creation_params = validated_data['creation_params']

    def validate(self):
        request_user = self.request.user
        if hasattr(request_user, 'partner_profile') and request_user != self.promo.partner.user:
            raise serializers.ValidationError(
                {
                    'promo': ['Partner can create direct links only for promos created by himself.'],
                }
            )

    def run(self):
        self.validate()
        direct_link = DirectLink.objects.create(promo=self.promo, creation_params=self.creation_params)
        MongoLogging().log(collection=DIRECT_LINK_COLLECTION, action=DirectLinkAction.CREATE,
                           data=DirectLinkSerializer(instance=direct_link).data)
        return direct_link


class UpdateDirectLink(UpdateEntity):
    def run(self):
        direct_link = super().run()
        MongoLogging().log(collection=DIRECT_LINK_COLLECTION, action=DirectLinkAction.UPDATE,
                           data=DirectLinkSerializer(instance=direct_link).data)
        return direct_link


class DestroyDirectLink(DestroyEntity):
    def run(self):
        super().run()
        MongoLogging().log(collection=DIRECT_LINK_COLLECTION, action=DirectLinkAction.DESTROY,
                           data=DirectLinkSerializer(instance=self.entity).data)


class CreateS3Static:
    def __init__(self, validated_data):
        self.file = validated_data['file']
        self.category = validated_data['category']

    def run(self):
        upload_service_path = upload_static_to_s3_service(self.file, self.category)
        s3_static = S3Static.objects.create(
            name=self.file.name,
            link=upload_service_path,
            category=self.category
        )
        MongoLogging().log(collection=S3_STATIC_COLLECTION, action=S3StaticAction.CREATE,
                           data=S3StaticSerializer(instance=s3_static).data)
        return s3_static


class UpdateS3Static:
    def __init__(self, s3_static, validated_data):
        self.s3_static = s3_static
        self.validated_data = validated_data

    def run(self):
        file = self.validated_data.get('file') or download_static_from_s3_service(self.s3_static.link)
        if isinstance(file, BytesIO):
            file_name = self.s3_static.name
            file = file.getvalue()
        else:
            file_name = file.name

        category = self.validated_data.get('category') or self.s3_static.category
        link = upload_static_to_s3_service(file, category)

        self.s3_static.name = file_name
        self.s3_static.link = link
        self.s3_static.category = category
        self.s3_static.modified_at = datetime_now()
        self.s3_static.save()

        MongoLogging().log(collection=S3_STATIC_COLLECTION, action=S3StaticAction.UPDATE,
                           data=S3StaticSerializer(instance=self.s3_static).data)
        return self.s3_static


class DestroyS3Static(DestroyEntity):
    def validate(self):
        banners_count = self.entity.banners.count() if hasattr(self.entity, 'banners') else 0
        if banners_count > 0:
            raise serializers.ValidationError(
                f'There is {banners_count} banners which are applied on this static. '
                'Switch them to another static before deleting.'
            )

    def run(self):
        self.validate()
        super().run()
        MongoLogging().log(collection=S3_STATIC_COLLECTION, action=S3StaticAction.DESTROY,
                           data=S3StaticSerializer(instance=self.entity).data)


class CreateBanner:
    def __init__(self, request, validated_data):
        self.request = request
        self.promo = validated_data['promo']
        self.image = validated_data['image']
        self.creation_params = validated_data['creation_params']

    def validate(self):
        request_user = self.request.user
        if hasattr(request_user, 'partner_profile') and request_user != self.promo.partner.user:
            raise serializers.ValidationError(
                {
                    'promo': ['Partner can create banners only for promos created by himself.'],
                }
            )

    def run(self):
        self.validate()
        banner = Banner.objects.create(promo=self.promo, image=self.image, creation_params=self.creation_params)
        MongoLogging().log(collection=BANNER_COLLECTION, action=BannerAction.CREATE,
                           data=BannerSerializer(instance=banner).data)
        return banner


class UpdateBanner(UpdateEntity):
    def run(self):
        banner = super().run()
        MongoLogging().log(collection=BANNER_COLLECTION, action=BannerAction.UPDATE,
                           data=BannerSerializer(instance=banner).data)
        return banner


class DestroyBanner(DestroyEntity):
    def run(self):
        super().run()
        MongoLogging().log(collection=BANNER_COLLECTION, action=BannerAction.DESTROY,
                           data=BannerSerializer(instance=self.entity).data)


class CreateLanding:
    def __init__(self, validated_data):
        self.name = validated_data['name']
        self.link = validated_data['link']

    def run(self):
        landing = Landing.objects.create(
            link=self.link,
            name=self.name
        )
        MongoLogging().log(collection=LANDING_COLLECTION, action=LandingAction.CREATE,
                           data=LandingSerializer(instance=landing).data)
        return landing


class UpdateLanding(UpdateEntity):
    def run(self):
        landing = super().run()
        MongoLogging().log(collection=LANDING_COLLECTION, action=LandingAction.UPDATE,
                           data=LandingSerializer(instance=landing).data)
        return landing


class DestroyLanding(DestroyEntity):
    def run(self):
        super().run()
        MongoLogging().log(collection=LANDING_COLLECTION, action=LandingAction.DESTROY,
                           data=LandingSerializer(instance=self.entity).data)


class CollectTraffic:
    def __init__(self, user, promos, validated_data):
        self.user = user
        self.promos = promos
        self.promo = validated_data.get('promo')
        self.timestamp_gte = validated_data.get('timestamp__gte', EPOCH_DATETIME)
        self.timestamp_lte = validated_data.get('timestamp__lte', datetime_now())
        self.is_unique = validated_data.get('is_unique')

    def validate(self):
        if self.promo and self.promo not in self.promos:
            raise serializers.ValidationError(
                {
                    'promo': ['Promo not exists in available promos.'],
                }
            )

    def run(self):
        self.validate()
        downloads = 0
        clicks = 0
        traffic_key = 'unique' if self.is_unique else 'all'
        clickhouse_client = ClickhouseClient()

        if self.promo:
            promos = Promo.objects.filter(pk=self.promo.uuid)
        else:
            promos = Promo.objects.filter(pk__in=[promo.uuid for promo in self.promos])

        traffic = GetTraffic(self.user, promos, clickhouse_client).get_promos_traffic(
            datetime_gte=self.timestamp_gte, datetime_lte=self.timestamp_lte,
            actions=(TrafficActionType.DOWNLOAD, TrafficActionType.CLICK)
        )

        for key in traffic:
            if TrafficActionType.DOWNLOAD in key:
                downloads += traffic[key][traffic_key]
            if TrafficActionType.CLICK in key:
                clicks += traffic[key][traffic_key]

        result = namedtuple('result', ['downloads', 'clicks'])(
            downloads=downloads,
            clicks=clicks,
        )
        return result
