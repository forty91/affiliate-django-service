import os

from base.utils.enumerate import BaseEnumerate


PROMO_COLLECTION = 'promos'
DIRECT_LINK_COLLECTION = 'direct-links'
BANNER_COLLECTION = 'banners'
LANDING_COLLECTION = 'landings'
S3_STATIC_COLLECTION = 's3-static'


class TrafficActionType(BaseEnumerate):
    DOWNLOAD = 'download'
    CLICK = 'click'


class PromoAction(BaseEnumerate):
    CREATE = 'create'
    UPDATE = 'update'
    DESTROY = 'destroy'


class BannerAction(BaseEnumerate):
    CREATE = 'create'
    UPDATE = 'update'
    DESTROY = 'destroy'


class DirectLinkAction(BaseEnumerate):
    CREATE = 'create'
    UPDATE = 'update'
    DESTROY = 'destroy'


class LandingAction(BaseEnumerate):
    CREATE = 'create'
    UPDATE = 'update'
    DESTROY = 'destroy'


class S3StaticAction(BaseEnumerate):
    CREATE = 'create'
    UPDATE = 'update'
    DESTROY = 'destroy'


TEMPLATES_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../promos/templates')
