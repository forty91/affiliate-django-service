# Generated by Django 2.1.2 on 2018-11-07 12:46

from django.db import migrations, models
from django.conf import settings
import uuid

from base.utils.migrations import get_default_partner_uuid
from users.models import Partner


def fill_default_entities(apps, schema_editor):
    Partner = apps.get_model("users", "Partner")
    Promo = apps.get_model("promos", "Promo")

    default_partner = Partner.objects.get(user__username=settings.DEFAULT_PARTNER_NAME)
    default_promo = Promo.objects.create(partner=default_partner, name=settings.DEFAULT_PROMO_NAME)


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('users', '0004_auto_20190219_1488'),
    ]

    operations = [
        migrations.CreateModel(
            name='Promo',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=255, unique=True)),
                ('partner', models.ForeignKey(default=get_default_partner_uuid, on_delete=models.deletion.SET_DEFAULT, related_name='promos', to='users.Partner')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.RunPython(fill_default_entities, reverse_code=migrations.RunPython.noop),
    ]
