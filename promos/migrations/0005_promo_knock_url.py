# Generated by Django 2.1.2 on 2018-12-04 14:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('promos', '0004_auto_20181129_1348'),
    ]

    operations = [
        migrations.AddField(
            model_name='promo',
            name='knock_url',
            field=models.CharField(blank=True, default='', help_text='Template of url for sending GET request on player registration.', max_length=2000),
        ),
    ]
