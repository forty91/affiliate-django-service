import os

from django.db import models
from django.conf import settings
from django.contrib.postgres.fields import JSONField

from base.help_texts import NAME, PARTNER, PROMO, CREATION_PARAMS, S3_STATIC_LINK, S3_STATIC, CREATED_AT, MODIFIED_AT, \
    S3_STATIC_NAME, S3_STATIC_CATEGORY, PROMO_KNOCK_URL, LANDING_LINK, LANDING, PROGRAM
from base.models import AffiliateModel
from base.utils.migrations import get_default_partner_uuid, get_default_program_uuid, get_default_landing_uuid
from base.utils.template import render_template
from projects.models import Program
from promos.consts import TEMPLATES_DIR
from users.models import Partner


class LandingManager(models.Manager):
    def default(self):
        return self.get(name=settings.DEFAULT_LANDING_NAME)


class Landing(AffiliateModel):
    name = models.CharField(max_length=255, unique=True, blank=False, help_text=NAME)
    link = models.CharField(max_length=2000, default='', blank=True, null=False, help_text=LANDING_LINK)

    objects = LandingManager()


class PromoManager(models.Manager):
    def default(self):
        return self.get(name=settings.DEFAULT_PROMO_NAME)


class Promo(AffiliateModel):
    name = models.CharField(max_length=255, blank=False, help_text=NAME)
    partner = models.ForeignKey('users.Partner', default=get_default_partner_uuid,
                                on_delete=models.SET_DEFAULT, related_name='promos', help_text=PARTNER)
    program = models.ForeignKey('projects.Program', default=get_default_program_uuid,
                                on_delete=models.SET_DEFAULT, related_name='promos', help_text=PROGRAM)
    knock_url = models.CharField(max_length=2000, default='', blank=True, null=False, help_text=PROMO_KNOCK_URL)
    landing = models.ForeignKey('promos.Landing', default=get_default_landing_uuid,
                                on_delete=models.SET_DEFAULT, related_name='promos', help_text=LANDING)
    created_at = models.DateTimeField(auto_now_add=True, help_text=CREATED_AT)
    modified_at = models.DateTimeField(auto_now=True, help_text=MODIFIED_AT)

    objects = PromoManager()


class DirectLink(AffiliateModel):
    promo = models.ForeignKey('promos.Promo', on_delete=models.CASCADE, related_name='direct_links', help_text=PROMO)
    creation_params = JSONField(default=dict(), help_text=CREATION_PARAMS)

    @property
    def generate_link(self):
        return render_template(
            template_path=os.path.join(TEMPLATES_DIR, 'direct_link.txt'),
            affiliate_static_service_url=settings.AFFILIATE_STATIC_SERVICE_URL,
            direct_link_id=self.uuid,
        )


class S3Static(AffiliateModel):
    name = models.CharField(max_length=255, help_text=S3_STATIC_NAME)
    category = models.CharField(max_length=255, help_text=S3_STATIC_CATEGORY)
    link = models.URLField(help_text=S3_STATIC_LINK)
    created_at = models.DateTimeField(auto_now_add=True, help_text=CREATED_AT)
    modified_at = models.DateTimeField(auto_now=True, help_text=MODIFIED_AT)


class Banner(AffiliateModel):
    promo = models.ForeignKey('promos.Promo', on_delete=models.CASCADE, related_name='banners', help_text=PROMO)
    creation_params = JSONField(default=dict(), help_text=CREATION_PARAMS)
    image = models.ForeignKey('promos.S3Static', null=True, on_delete=models.SET_NULL, related_name='banners',
                              help_text=S3_STATIC)

    @property
    def generate_html(self):
        return render_template(
            template_path=os.path.join(TEMPLATES_DIR, 'banner.html'),
            affiliate_static_service_url=settings.AFFILIATE_STATIC_SERVICE_URL,
            banner_id=self.uuid,
            alt=self.image.name
        )
