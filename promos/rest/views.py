from drf_yasg.utils import swagger_auto_schema
from rest_framework.decorators import action
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin, DestroyModelMixin
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.status import HTTP_201_CREATED, HTTP_200_OK, HTTP_204_NO_CONTENT
from rest_framework.filters import OrderingFilter
from django_filters import rest_framework as filters

from api_public.pagination import AffiliatePagination
from api_public.permissions import IsPartner, staff_permission
from base.views import AffiliateViewSet, AffiliateListModelMixin, AffiliateRetrieveModelMixin, \
    AffiliateDestroyModelMixin
from permissions.consts import Block, Action
from promos.bl import CreatePromo, CreateDirectLink, CreateS3Static, CreateBanner, UpdatePromo, CreateLanding, \
    UpdateLanding, UpdateS3Static, UpdateDirectLink, UpdateBanner, DestroyPromo, DestroyDirectLink, DestroyS3Static, \
    DestroyBanner, DestroyLanding, CollectTraffic
from promos.models import Promo, DirectLink, Banner, S3Static, Landing
from promos.rest.filters import DirectLinkFilter, PromoFilter, S3StaticFilter, BannerFilter
from promos.rest.serializers import PromoSerializer, PromoCreateSerializer, DirectLinkSerializer, \
    DirectLinkCreateSerializer, BannerSerializer, S3StaticCreateSerializer, S3StaticSerializer, \
    BannerCreateSerializer, PromoUpdateSerializer, LandingSerializer, LandingCreateSerializer, LandingUpdateSerializer, \
    S3StaticUpdateSerializer, DirectLinkUpdateSerializer, BannerUpdateSerializer, TrafficSerializer, \
    TrafficParamsSerializer


class PromoViewSet(AffiliateViewSet, AffiliateListModelMixin, AffiliateRetrieveModelMixin):
    """
    list:
    For staff user return a list of all the existing promos.
    For partner user return a list with promos created by current partner.

    retrieve:
    Return a single promo. Promo must exists in list for current user.

    create:
    Create a new promo instance.
    Promo name should be unique for the same partner.
    Partner can create promo for himself only.
    Staff user can create promo for any partner.

    update:
    Update existed promo.
    Partner can update his promos only.
    Staff user can update any promos.

    destroy:
    Delete existed promo.
    Partner can delete his promos only.
    Staff user can delete any promos.

    traffic:
    Get traffic information about promo.
    Partner can get information about his promos only.
    Staff can get information about any promos.
    """
    serializer_class = PromoSerializer
    queryset = Promo.objects.all()
    queryset_user_lookup_expr = 'partner__user'
    pagination_class = AffiliatePagination
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter)
    filter_class = PromoFilter
    ordering_fields = ('name', )
    ordering = ('name', )

    permission_classes_by_action = {
        'create': [IsPartner, staff_permission(Block.PROMOS, Action.CREATE)],
        'list': [IsPartner, staff_permission(Block.PROMOS, Action.READ)],
        'retrieve': [IsPartner, staff_permission(Block.PROMOS, Action.READ)],
        'update': [IsPartner, staff_permission(Block.PROMOS, Action.UPDATE)],
        'destroy': [IsPartner, staff_permission(Block.PROMOS, Action.DELETE)],
        'traffic': [IsPartner, staff_permission(Block.PROMOS, Action.READ)],
    }

    @swagger_auto_schema(request_body=PromoCreateSerializer,
                         responses={HTTP_201_CREATED: PromoSerializer})
    def create(self, request):
        serializer = PromoCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        promo = CreatePromo(request, serializer.validated_data).run()
        return Response(data=PromoSerializer(instance=promo).data,
                        status=HTTP_201_CREATED)

    @swagger_auto_schema(request_body=PromoUpdateSerializer,
                         responses={HTTP_200_OK: PromoSerializer})
    def update(self, request, *args, **kwargs):
        serializer = PromoUpdateSerializer(data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)

        self.queryset_user_filter = {self.queryset_user_lookup_expr: self.request.user}

        promo = UpdatePromo(self.get_object(), serializer.validated_data).run()
        return Response(data=PromoSerializer(instance=promo).data,
                        status=HTTP_200_OK)

    def destroy(self, request, *args, **kwargs):
        self.queryset_user_filter = {self.queryset_user_lookup_expr: self.request.user}

        DestroyPromo(self.get_object()).run()
        return Response(status=HTTP_204_NO_CONTENT)

    @swagger_auto_schema(query_serializer=TrafficParamsSerializer,
                         responses={HTTP_200_OK: TrafficSerializer})
    @action(methods=['get'], detail=False, url_path='traffic')
    def traffic(self, request, *args, **kwargs):
        """
        Расчет показов/переходов всех/уникальных по promo за временной промежуток.
        """
        serializer = TrafficParamsSerializer(data=request.query_params)
        serializer.is_valid(raise_exception=True)

        self.queryset_user_filter = {self.queryset_user_lookup_expr: self.request.user}
        promos = self.filter_queryset(self.get_queryset())

        traffic = CollectTraffic(request.user, promos, serializer.validated_data).run()

        return Response(data=TrafficSerializer(instance=traffic).data,
                        status=HTTP_200_OK)


class DirectLinkViewSet(AffiliateViewSet, AffiliateListModelMixin, AffiliateRetrieveModelMixin):
    """
    list:
    For staff user return a list of all the existing direct links.
    For partner user return a list with direct links which corresponds to promos created by current partner.

    retrieve:
    Return a single direct link. Direct link must exists in list for current user.

    create:
    Create a new direct link.
    Partner can create direct link for his promos only.
    Staff user can create direct link for any promos.

    update:
    Update existed direct link.
    Partner can update direct link for his promos only.
    Staff user can update direct link for any promos.

    destroy:
    Delete existed direct link.
    Partner can delete direct link for his promos only.
    Staff user can delete direct link for any promos.
    """
    serializer_class = DirectLinkSerializer
    queryset = DirectLink.objects.all()
    queryset_user_lookup_expr = 'promo__partner__user'
    pagination_class = AffiliatePagination
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter)
    filter_class = DirectLinkFilter
    ordering_fields = ('promo__modified_at', 'promo__name')
    ordering = ('-promo__modified_at', )

    permission_classes_by_action = {
        'create': [IsPartner, staff_permission(Block.PROMOS, Action.CREATE)],
        'list': [IsPartner, staff_permission(Block.PROMOS, Action.READ)],
        'retrieve': [IsPartner, staff_permission(Block.PROMOS, Action.READ)],
        'update': [IsPartner, staff_permission(Block.PROMOS, Action.UPDATE)],
        'destroy': [IsPartner, staff_permission(Block.PROMOS, Action.DELETE)]
    }

    @swagger_auto_schema(request_body=DirectLinkCreateSerializer,
                         responses={HTTP_201_CREATED: DirectLinkSerializer})
    def create(self, request):
        serializer = DirectLinkCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        direct_link = CreateDirectLink(request, serializer.validated_data).run()
        return Response(data=DirectLinkSerializer(instance=direct_link).data,
                        status=HTTP_201_CREATED)

    @swagger_auto_schema(request_body=DirectLinkUpdateSerializer,
                         responses={HTTP_200_OK: DirectLinkSerializer})
    def update(self, request, *args, **kwargs):
        serializer = DirectLinkUpdateSerializer(data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)

        self.queryset_user_filter = {self.queryset_user_lookup_expr: self.request.user}

        direct_link = UpdateDirectLink(self.get_object(), serializer.validated_data).run()
        return Response(data=DirectLinkSerializer(instance=direct_link).data,
                        status=HTTP_200_OK)

    def destroy(self, request, *args, **kwargs):
        self.queryset_user_filter = {self.queryset_user_lookup_expr: self.request.user}

        DestroyDirectLink(self.get_object()).run()
        return Response(status=HTTP_204_NO_CONTENT)


class S3StaticViewSet(AffiliateViewSet, ListModelMixin, RetrieveModelMixin):
    """
    list:
    Return a list of all the available AWS S3 static files.

    retrieve:
    Return a single available AWS S3 static file.

    create:
    Create a new available AWS S3 static file. Available for staff users only.

    update:
    Partial update existed AWS S3 static file. Available for staff users only.

    destroy:
    Delete existed AWS S3 static file. Available for staff users only.
    """
    serializer_class = S3StaticSerializer
    queryset = S3Static.objects.all()
    pagination_class = AffiliatePagination
    parser_classes = (MultiPartParser,)
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter)
    filter_class = S3StaticFilter
    ordering_fields = ('created_at', 'modified_at')
    ordering = ('modified_at', )

    permission_classes_by_action = {
        'create': [staff_permission(Block.PROMOS, Action.CREATE)],
        'list': [IsAuthenticated],
        'retrieve': [IsAuthenticated],
        'update': [staff_permission(Block.PROMOS, Action.UPDATE)],
        'destroy': [staff_permission(Block.PROMOS, Action.DELETE)]
    }

    @swagger_auto_schema(request_body=S3StaticCreateSerializer,
                         responses={HTTP_201_CREATED: S3StaticSerializer})
    def create(self, request):
        serializer = S3StaticCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        s3_static = CreateS3Static(serializer.validated_data).run()
        return Response(data=S3StaticSerializer(instance=s3_static).data,
                        status=HTTP_201_CREATED)

    @swagger_auto_schema(request_body=S3StaticUpdateSerializer,
                         responses={HTTP_200_OK: S3StaticSerializer})
    def update(self, request, *args, **kwargs):
        serializer = S3StaticUpdateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        s3_static = UpdateS3Static(self.get_object(), serializer.validated_data).run()
        return Response(data=S3StaticSerializer(instance=s3_static).data,
                        status=HTTP_200_OK)

    def destroy(self, request, *args, **kwargs):
        DestroyS3Static(self.get_object()).run()
        return Response(status=HTTP_204_NO_CONTENT)


class BannerViewSet(AffiliateViewSet, AffiliateListModelMixin, AffiliateRetrieveModelMixin):
    """
    list:
    For staff user return a list of all the existing banners.
    For partner user return a list with banners which corresponds to promos created by current partner.

    retrieve:
    Return a single banner. Banner must exists in list for current user.

    create:
    Create a new banner.
    Partner can create banner for his promos only.
    Staff user can create banner for any promos.

    update:
    Update existed banner.
    Partner can update banner for his promos only.
    Staff user can update banner for any promos.

    destroy:
    Delete existed banner.
    Partner can delete banner for his promos only.
    Staff user can delete banner for any promos.
    """
    serializer_class = BannerSerializer
    queryset = Banner.objects.all()
    queryset_user_lookup_expr = 'promo__partner__user'
    pagination_class = AffiliatePagination
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter)
    filter_class = BannerFilter
    ordering_fields = ('promo__name', 'image__created_at', 'image__modified_at', 'promo__modified_at')
    ordering = ('-promo__modified_at', )

    permission_classes_by_action = {
        'create': [IsPartner, staff_permission(Block.PROMOS, Action.CREATE)],
        'list': [IsPartner, staff_permission(Block.PROMOS, Action.READ)],
        'retrieve': [IsPartner, staff_permission(Block.PROMOS, Action.READ)],
        'update': [IsPartner, staff_permission(Block.PROMOS, Action.UPDATE)],
        'destroy': [IsPartner, staff_permission(Block.PROMOS, Action.DELETE)]
    }

    @swagger_auto_schema(request_body=BannerCreateSerializer,
                         responses={HTTP_201_CREATED: BannerSerializer})
    def create(self, request):
        serializer = BannerCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        banner = CreateBanner(request, serializer.validated_data).run()
        return Response(data=BannerSerializer(instance=banner).data,
                        status=HTTP_201_CREATED)

    @swagger_auto_schema(request_body=BannerUpdateSerializer,
                         responses={HTTP_200_OK: BannerSerializer})
    def update(self, request, *args, **kwargs):
        serializer = BannerUpdateSerializer(data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)

        self.queryset_user_filter = {self.queryset_user_lookup_expr: self.request.user}

        banner = UpdateBanner(self.get_object(), serializer.validated_data).run()
        return Response(data=BannerSerializer(instance=banner).data,
                        status=HTTP_200_OK)

    def destroy(self, request, *args, **kwargs):
        self.queryset_user_filter = {self.queryset_user_lookup_expr: self.request.user}

        DestroyBanner(self.get_object()).run()
        return Response(status=HTTP_204_NO_CONTENT)


class LandingViewSet(AffiliateViewSet, ListModelMixin, RetrieveModelMixin):
    """
    list:
    Return a list of all the landing pages.

    retrieve:
    Return a single landings page.

    create:
    Create a new landing page. Available for staff users only.
    """
    serializer_class = LandingSerializer
    queryset = Landing.objects.all()
    pagination_class = AffiliatePagination
    ordering_fields = ('name',)
    ordering = ('name',)

    permission_classes_by_action = {
        'create': [staff_permission(Block.PROMOS, Action.CREATE)],
        'list': [IsAuthenticated],
        'retrieve': [IsAuthenticated],
        'update': [staff_permission(Block.PROMOS, Action.UPDATE)],
        'destroy': [staff_permission(Block.PROMOS, Action.DELETE)]
    }

    @swagger_auto_schema(request_body=LandingCreateSerializer,
                         responses={HTTP_201_CREATED: LandingSerializer})
    def create(self, request):
        serializer = LandingCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        landing = CreateLanding(serializer.validated_data).run()
        return Response(data=LandingSerializer(instance=landing).data,
                        status=HTTP_201_CREATED)

    @swagger_auto_schema(request_body=LandingUpdateSerializer,
                         responses={HTTP_200_OK: LandingSerializer})
    def update(self, request, *args, **kwargs):
        serializer = LandingUpdateSerializer(data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)

        landing = UpdateLanding(self.get_object(), serializer.validated_data).run()
        return Response(data=LandingSerializer(instance=landing).data,
                        status=HTTP_200_OK)

    def destroy(self, request, *args, **kwargs):
        DestroyLanding(self.get_object()).run()
        return Response(status=HTTP_204_NO_CONTENT)
