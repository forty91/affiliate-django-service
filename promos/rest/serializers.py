from collections import OrderedDict

from drf_queryfields import QueryFieldsMixin
from rest_framework import serializers

from base.consts import UTC_DATETIME_FORMAT
from base.help_texts import S3_STATIC_FILE, S3_STATIC_CATEGORY, PROMO, CREATION_PARAMS, S3_STATIC, PROMO_KNOCK_URL, \
    NAME, PARTNER, IS_UNIQUE_USERS, TIMESTAMP_GTE, TIMESTAMP_LTE, TRAFFIC_DOWNLOADS, TRAFFIC_CLICKS, LANDING_EXACT, \
    PROMO_EXACT, LANDING_LINK, LANDING
from promos.models import Promo, DirectLink, Banner, S3Static, Landing
from users.models import Partner
from api_public.serializers import PartnerNameSerializer, DirectLinkNameSerializer, \
    PromoNameSerializer, S3StaticNameSerializer, LandingNameSerializer, BannerNameSerializer


class PromoSerializer(serializers.ModelSerializer):
    partner = PartnerNameSerializer()
    direct_links = DirectLinkNameSerializer(many=True)
    banners = BannerNameSerializer(many=True)
    landing = LandingNameSerializer()

    class Meta:
        model = Promo
        fields = ('id', 'partner', 'name', 'direct_links', 'banners', 'knock_url', 'landing', 'created_at', 'modified_at')


class PromoCreateSerializer(QueryFieldsMixin, serializers.ModelSerializer):
    partner = serializers.PrimaryKeyRelatedField(queryset=Partner.objects.all(), help_text=PARTNER)
    name = serializers.CharField(min_length=4, max_length=255, allow_blank=False, allow_null=False, help_text=NAME)
    knock_url = serializers.CharField(max_length=2000, allow_blank=True, allow_null=False, required=False,
                                      help_text=PROMO_KNOCK_URL)

    class Meta:
        model = Promo
        fields = ('partner', 'name', 'knock_url', 'landing')

    def validate(self, attrs):
        partner = attrs['partner']
        name = attrs['name']
        if partner.promos.filter(name=name).exists():
            raise serializers.ValidationError(
                {
                    'name': ['Promo should have unique name for the same partner.'],
                }
            )

        return attrs


class PromoUpdateSerializer(serializers.ModelSerializer):
    name = serializers.CharField(min_length=4, max_length=255, allow_blank=False, allow_null=False, required=False)
    knock_url = serializers.CharField(max_length=2000, allow_blank=True, allow_null=False, required=False)

    class Meta:
        model = Promo
        fields = ('name', 'knock_url', 'landing')


class DirectLinkSerializer(serializers.ModelSerializer):
    link = serializers.SerializerMethodField()
    promo = PromoNameSerializer()

    def get_link(self, obj):
        return obj.generate_link

    class Meta:
        model = DirectLink
        fields = ('id', 'promo', 'creation_params', 'link')


class DirectLinkCreateSerializer(QueryFieldsMixin, serializers.Serializer):
    promo = serializers.PrimaryKeyRelatedField(queryset=Promo.objects.all(), help_text=PROMO)
    creation_params = serializers.JSONField(allow_null=False, required=False, default=OrderedDict(),
                                            help_text=CREATION_PARAMS)


class DirectLinkUpdateSerializer(QueryFieldsMixin, serializers.Serializer):
    promo = serializers.PrimaryKeyRelatedField(queryset=Promo.objects.all(), required=False, help_text=PROMO)
    creation_params = serializers.JSONField(allow_null=False, required=False, help_text=CREATION_PARAMS)


class PrivateDirectLinkSerializer(serializers.Serializer):
    promo_id = serializers.CharField(help_text=PROMO)
    landing_id = serializers.CharField(help_text=LANDING)
    landing_link = serializers.CharField(help_text=LANDING_LINK)
    partner_id = serializers.CharField(help_text=PARTNER)
    creation_params = serializers.JSONField(help_text=CREATION_PARAMS)

    def to_representation(self, obj):
        promo = obj.promo
        promo_id = promo.uuid
        landing = promo.landing
        landing_id = landing.uuid
        landing_link = landing.link
        partner_id = promo.partner.uuid
        creation_params = obj.creation_params

        return OrderedDict([
            ('promo_id', promo_id),
            ('landing_id', landing_id),
            ('landing_link', landing_link),
            ('partner_id', partner_id),
            ('creation_params', creation_params)
        ])


class S3StaticSerializer(serializers.ModelSerializer):
    banners = BannerNameSerializer(many=True)

    class Meta:
        model = S3Static
        fields = ('id', 'link', 'name', 'category', 'banners', 'created_at', 'modified_at')


class S3StaticCreateSerializer(serializers.Serializer):
    file = serializers.FileField(required=True, help_text=S3_STATIC_FILE)
    category = serializers.CharField(max_length=255, required=True, help_text=S3_STATIC_CATEGORY,
                                     validators=[])


class S3StaticUpdateSerializer(serializers.Serializer):
    file = serializers.FileField(required=False, help_text=S3_STATIC_FILE)
    category = serializers.CharField(max_length=255, required=False, help_text=S3_STATIC_CATEGORY)


class BannerSerializer(serializers.ModelSerializer):
    html = serializers.SerializerMethodField()
    promo = PromoNameSerializer()
    image = S3StaticNameSerializer()

    def get_html(self, obj):
        return obj.generate_html

    class Meta:
        model = Banner
        fields = ('id', 'promo', 'image', 'creation_params', 'html')


class BannerCreateSerializer(QueryFieldsMixin, serializers.Serializer):
    promo = serializers.PrimaryKeyRelatedField(queryset=Promo.objects.all(), required=True, help_text=PROMO)
    image = serializers.PrimaryKeyRelatedField(queryset=S3Static.objects.all(), required=True, help_text=S3_STATIC)
    creation_params = serializers.JSONField(required=False, allow_null=False, default=dict(), help_text=CREATION_PARAMS)


class BannerUpdateSerializer(QueryFieldsMixin, serializers.Serializer):
    promo = serializers.PrimaryKeyRelatedField(queryset=Promo.objects.all(), required=False, help_text=PROMO)
    image = serializers.PrimaryKeyRelatedField(queryset=S3Static.objects.all(), required=False, help_text=S3_STATIC)
    creation_params = serializers.JSONField(required=False, allow_null=False, help_text=CREATION_PARAMS)


class PrivateBannerSerializer(serializers.Serializer):
    promo_id = serializers.CharField(help_text=PROMO)
    landing_id = serializers.CharField(help_text=LANDING)
    landing_link = serializers.CharField(help_text=LANDING_LINK)
    partner_id = serializers.CharField(help_text=PARTNER)
    creation_params = serializers.JSONField(help_text=CREATION_PARAMS)

    def to_representation(self, obj):
        promo = obj.promo
        promo_id = promo.uuid
        landing = promo.landing
        landing_id = landing.uuid
        landing_link = landing.link
        partner_id = promo.partner.uuid
        creation_params = obj.creation_params

        return OrderedDict([
            ('promo_id', promo_id),
            ('landing_id', landing_id),
            ('landing_link', landing_link),
            ('partner_id', partner_id),
            ('creation_params', creation_params)
        ])


class LandingSerializer(serializers.ModelSerializer):

    class Meta:
        model = Landing
        fields = ('id', 'name', 'link')


class LandingCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Landing
        fields = ('name', 'link')


class LandingUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Landing
        fields = ('name', 'link')


class TrafficParamsSerializer(serializers.Serializer):
    promo = serializers.PrimaryKeyRelatedField(queryset=Promo.objects.all(), required=False, help_text=PROMO_EXACT)
    timestamp__lte = serializers.DateTimeField(format=UTC_DATETIME_FORMAT, required=False, help_text=TIMESTAMP_LTE)
    timestamp__gte = serializers.DateTimeField(format=UTC_DATETIME_FORMAT, required=False, help_text=TIMESTAMP_GTE)
    is_unique = serializers.BooleanField(default=False, required=False, help_text=IS_UNIQUE_USERS)


class TrafficSerializer(serializers.Serializer):
    downloads = serializers.IntegerField(help_text=TRAFFIC_DOWNLOADS)
    clicks = serializers.IntegerField(help_text=TRAFFIC_CLICKS)
