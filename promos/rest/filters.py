from django_filters import rest_framework as filters

from api_public.filters import IsoDateTimeFilter
from base.help_texts import NAME_ICONTAINS, PARTNER_EXACT, PARTNER_USERNAME_ICONTAINS, PROMO_EXACT, \
    PROMO_NAME_ICONTAINS, S3_STATIC_LINK_ICONTAINS, CREATED_AT_LTE, CREATED_AT_GTE, MODIFIED_AT_LTE, MODIFIED_AT_GTE, \
    S3_STATIC_EXACT, S3_STATIC_NAME_ICONTAINS, S3_STATIC_CATEGORY_EXACT
from promos.models import DirectLink, Promo, Banner, S3Static


class PromoFilter(filters.FilterSet):
    name = filters.CharFilter(field_name='name', lookup_expr='icontains', help_text=NAME_ICONTAINS)
    partner = filters.UUIDFilter(field_name='partner', help_text=PARTNER_EXACT)
    partner__username = filters.CharFilter(field_name='partner', lookup_expr='user__username__icontains', help_text=PARTNER_USERNAME_ICONTAINS)

    class Meta:
        model = Promo
        fields = ('name', 'partner')


class DirectLinkFilter(filters.FilterSet):
    promo = filters.UUIDFilter(field_name='promo', help_text=PROMO_EXACT)
    promo__name = filters.CharFilter(field_name='promo', lookup_expr='name__icontains', help_text=PROMO_NAME_ICONTAINS)

    class Meta:
        model = DirectLink
        fields = ['promo']


class S3StaticFilter(filters.FilterSet):
    name = filters.CharFilter(field_name='name', lookup_expr='icontains', help_text=S3_STATIC_NAME_ICONTAINS)
    category = filters.CharFilter(field_name='category', help_text=S3_STATIC_CATEGORY_EXACT)
    link = filters.CharFilter(field_name='link', lookup_expr='icontains', help_text=S3_STATIC_LINK_ICONTAINS)
    created_at__lte = IsoDateTimeFilter(field_name='created_at', lookup_expr='lte', help_text=CREATED_AT_LTE)
    created_at__gte = IsoDateTimeFilter(field_name='created_at', lookup_expr='gte', help_text=CREATED_AT_GTE)
    modified_at__lte = IsoDateTimeFilter(field_name='modified_at', lookup_expr='lte', help_text=MODIFIED_AT_LTE)
    modified_at__gte = IsoDateTimeFilter(field_name='modified_at', lookup_expr='gte', help_text=MODIFIED_AT_GTE)

    class Meta:
        model = S3Static
        fields = ['name', 'category', 'link']


class BannerFilter(filters.FilterSet):
    promo = filters.UUIDFilter(field_name='promo', help_text=PROMO_EXACT)
    promo__name = filters.CharFilter(field_name='promo', lookup_expr='name__icontains', help_text=PROMO_NAME_ICONTAINS)
    image = filters.CharFilter(field_name='image', help_text=S3_STATIC_EXACT)

    class Meta:
        model = Banner
        fields = ['promo', 'image']
