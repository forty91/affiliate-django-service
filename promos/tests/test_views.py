import datetime
import os
from unittest.mock import patch, call, ANY

from django.urls import reverse
from freezegun import freeze_time
from rest_framework.status import HTTP_201_CREATED, HTTP_400_BAD_REQUEST, HTTP_200_OK, HTTP_204_NO_CONTENT

from base.consts import UTC_DATETIME_FORMAT
from base.tests import AffiliateAPITestCase
from base.utils.clickhouse import ClickhouseClient
from base.utils.datetime import utc_datetime, utc_datetime_to_iso
from base.utils.tests import generate_tmp_image
from base.utils.template import render_template
from promos.models import Promo, DirectLink, Banner, S3Static, Landing
from promos.tests.factories import PromoFactory, S3StaticFactory, LandingFactory, DirectLinkFactory, BannerFactory
from api_public.serializers import PartnerNameSerializer, PromoNameSerializer, S3StaticNameSerializer, \
    LandingNameSerializer
from traffic.utils import get_partner_table_name
from traffic.consts import QUERIES_DIR
from users.models import Partner
from users.tests.factories import PartnerFactory


class PromoViewSetTestCase(AffiliateAPITestCase):
    def test_create(self):
        now = datetime.datetime(2018, 10, 10, 10, 10, 10)
        landing = LandingFactory()
        name = 'some_promo'
        url = reverse('public:promo-list')
        data = {
            "partner": self.partner.uuid,
            "name": name,
            "knock_url": "https://habr.com/post/{post_id}/",
            "landing": landing.uuid,
        }

        with freeze_time(now):
            response = self.client.post(url, data, format='json',
                                        HTTP_AUTHORIZATION='JWT %s' % self.partner_jwt_token)

        created_promo = Promo.objects.get(partner=self.partner, name=name)

        expected_data = {
            'id': created_promo.uuid,
            "name": name,
            'partner': PartnerNameSerializer(instance=self.partner).data,
            'knock_url': 'https://habr.com/post/{post_id}/',
            'direct_links': [],
            'banners': [],
            'landing': LandingNameSerializer(instance=landing).data,
            'created_at': utc_datetime_to_iso(now),
            'modified_at': utc_datetime_to_iso(now),
        }
        self.assertEqual(response.status_code, HTTP_201_CREATED)
        self.assertEqual(response.data, expected_data)

    def test_create_name_not_unique(self):
        """
        Не уникальное имя промо в рамках одного партнера
        """
        existed_promo = PromoFactory(partner=self.partner)

        url = reverse('public:promo-list')
        data = {
            "partner": self.partner.uuid,
            "name": existed_promo.name
        }

        response = self.client.post(url, data, format='json',
                                    HTTP_AUTHORIZATION='JWT %s' % self.partner_jwt_token)
        self.assertEqual(response.status_code, HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'name': ['Promo should have unique name for the same partner.']})

    def test_create_by_other_partner(self):
        """
        Партнер пытается создать промо для другого партнера
        """
        other_partner = PartnerFactory()
        name = 'some_promo'

        url = reverse('public:promo-list')
        data = {
            "partner": other_partner.uuid,
            "name": name
        }

        response = self.client.post(url, data, format='json',
                                    HTTP_AUTHORIZATION='JWT %s' % self.partner_jwt_token)
        self.assertEqual(response.status_code, HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'partner': ['Partner can create promo only for himself.']})

    def test_create_by_staff(self):
        """
        Сотрудник создает промо для партнера
        """
        now = datetime.datetime(2018, 10, 10, 10, 10, 10)
        name = 'some_promo'
        url = reverse('public:promo-list')
        data = {
            "partner": self.partner.uuid,
            "name": name
        }

        with freeze_time(now):
            response = self.client.post(url, data, format='json',
                                        HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token)

        created_promo = Promo.objects.get(partner=self.partner, name=name)

        expected_data = {
            'id': created_promo.uuid,
            "name": name,
            'partner': PartnerNameSerializer(instance=self.partner).data,
            'knock_url': '',
            'direct_links': [],
            'banners': [],
            'landing': LandingNameSerializer(instance=Landing.objects.default()).data,
            'created_at': utc_datetime_to_iso(now),
            'modified_at': utc_datetime_to_iso(now),
        }
        self.assertEqual(response.status_code, HTTP_201_CREATED)
        self.assertEqual(response.data, expected_data)

    def test_update(self):
        long_ago = datetime.datetime(2018, 9, 9, 9, 9, 9)
        now = datetime.datetime(2018, 10, 10, 10, 10, 10)
        with freeze_time(long_ago):
            promo = PromoFactory(partner=self.partner)
        landing = LandingFactory()
        new_name = 'new_name'
        new_knock_url = 'https://google.com/'
        url = reverse('public:promo-detail', kwargs={'pk': promo.id})
        data = {
            "name": new_name,
            "knock_url": new_knock_url,
            "landing": landing.uuid
        }

        with freeze_time(now):
            response = self.client.put(url, data, format='json',
                                       HTTP_AUTHORIZATION='JWT %s' % self.partner_jwt_token)

        created_promo = Promo.objects.get(partner=self.partner, name=new_name)

        expected_data = {
            'id': created_promo.uuid,
            "name": new_name,
            'partner': PartnerNameSerializer(instance=self.partner).data,
            'knock_url': 'https://google.com/',
            'direct_links': [],
            'banners': [],
            'landing': LandingNameSerializer(instance=landing).data,
            'created_at': utc_datetime_to_iso(long_ago),
            'modified_at': utc_datetime_to_iso(now),
        }
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertEqual(response.data, expected_data)

    def test_incorrect_promo_traffic_by_partner(self):
        """
        Если партнер запрашивает траффик который не относится к его promos, то получает ошибку.
        """
        partner_promo = PromoFactory(partner=self.partner)
        not_partner_promo = PromoFactory()
        url = '{endpoint}?promo={promo}'.format(
            endpoint=reverse('public:promo-traffic'),
            promo=not_partner_promo.uuid
        )
        response = self.client.get(url, format='json',
                                   HTTP_AUTHORIZATION='JWT %s' % self.partner_jwt_token)

        self.assertEqual(response.status_code, HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data,
                         {'promo': ['Promo not exists in available promos.']})

    # def test_incorrect_partner_traffic_by_partner(self):
    #     """
    #     Если партнер запрашивает траффик который не относится к нему, то не получает ничего.
    #     """
    #     promo = PromoFactory(partner=self.partner)
    #     other_partner = PartnerFactory()
    #     other_promo = PromoFactory(partner=other_partner)
    #     url = '{endpoint}?partner={partner}'.format(
    #         endpoint=reverse('public:promo-traffic'),
    #         partner=other_partner.uuid
    #     )
    #     response = self.client.get(url, format='json',
    #                                HTTP_AUTHORIZATION='JWT %s' % self.partner_jwt_token)
    #
    #     self.assertEqual(response.status_code, HTTP_200_OK)
    #     self.assertEqual(response.data, {'downloads': 0, 'clicks': 0})

    @patch.object(ClickhouseClient, 'execute', return_value=((100,),), autospec=True)
    def staff_get_all_traffic(self, clickhouse_client_execute_mock):
        """
        Сотрудник запрашивая весь траффик без фильтров получает траффик по всем promo всех партнеров.
        """
        partner1 = PartnerFactory()
        partner2 = PartnerFactory()
        promo1 = PromoFactory(partner=partner1)
        promo2 = PromoFactory(partner=partner2)
        url = reverse('public:promo-traffic')
        response = self.client.get(url, format='json',
                                   HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token)

        self.assertEqual(response.data, {'downloads': 300, 'clicks': 300})
        calls = []
        for partner in (Partner.objects.default(), partner1, partner2):
            downloads_query = render_template(
                os.path.join(QUERIES_DIR, 'select_traffic.sql'),
                column='count(UserToken)',
                table_name=get_partner_table_name(partner),
                action='download', promo_id=None,
                timestamp__gte=None, timestamp__lte=None
            )
            calls.append(call(ANY, downloads_query))
            click_query = render_template(
                os.path.join(QUERIES_DIR, 'select_traffic.sql'),
                column='count(UserToken)',
                table_name=get_partner_table_name(partner),
                action='click', promo_id=None,
                timestamp__gte=None, timestamp__lte=None
            )
            calls.append(call(ANY, click_query))

        clickhouse_client_execute_mock.assert_has_calls(calls, any_order=True)

    @patch.object(ClickhouseClient, 'execute', return_value=((200,),), autospec=True)
    def staff_get_traffic_by_partner(self, clickhouse_client_execute_mock):
        """
        Сотрудник запрашивает трафиик конкретного партнера и фильтрует по времени.
        """
        partner1 = PartnerFactory()
        partner2 = PartnerFactory()
        promo1 = PromoFactory(partner=partner1)
        promo2 = PromoFactory(partner=partner2)
        timestamp__gte = utc_datetime(2018, 10, 10, 10, 10, 10)
        timestamp__lte = utc_datetime(2018, 11, 11, 11, 11, 11)
        url = '{endpoint}?partner={partner}&timestamp__gte={timestamp__gte}&timestamp__lte={timestamp__lte}'.format(
            endpoint=reverse('public:promo-traffic'),
            partner=partner2.uuid,
            timestamp__gte=timestamp__gte.strftime(UTC_DATETIME_FORMAT),
            timestamp__lte=timestamp__lte.strftime(UTC_DATETIME_FORMAT),
        )
        response = self.client.get(url, format='json',
                                   HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token)

        self.assertEqual(response.data, {'downloads': 200, 'clicks': 200})

        downloads_query = render_template(
            os.path.join(QUERIES_DIR, 'select_traffic.sql'),
            column='count(UserToken)',
            table_name=get_partner_table_name(partner2),
            action='download', promo_id=None,
            timestamp__gte=int(timestamp__gte.timestamp()), timestamp__lte=int(timestamp__lte.timestamp())
        )
        click_query = render_template(
            os.path.join(QUERIES_DIR, 'select_traffic.sql'),
            column='count(UserToken)',
            table_name=get_partner_table_name(partner2),
            action='click', promo_id=None,
            timestamp__gte=int(timestamp__gte.timestamp()), timestamp__lte=int(timestamp__lte.timestamp())
        )
        calls = [call(ANY, downloads_query), call(ANY, click_query)]
        clickhouse_client_execute_mock.assert_has_calls(calls, any_order=False)


class DirectLinkViewSetTestCase(AffiliateAPITestCase):
    def test_create(self):
        url = reverse('public:directlink-list')
        promo = PromoFactory(partner=self.partner)
        creation_params = {
            'ololo': '12345',
            'trololo': '54321'
        }
        data = {
            "promo": promo.uuid,
            "creation_params": creation_params
        }

        response = self.client.post(url, data, format='json',
                                    HTTP_AUTHORIZATION='JWT %s' % self.partner_jwt_token)

        created_dl = DirectLink.objects.get(promo=promo)

        expected_data = {
            'id': created_dl.uuid,
            'promo': PromoNameSerializer(instance=promo).data,
            'creation_params': creation_params,
            'link': created_dl.generate_link
        }
        self.assertEqual(response.status_code, HTTP_201_CREATED)
        self.assertEqual(response.data, expected_data)

    def test_create_by_other_partner(self):
        """
        Партнер пытается создать direct link для другого партнера
        """
        other_partner_promo = PromoFactory()

        url = reverse('public:directlink-list')
        data = {
            "promo": other_partner_promo.uuid,
        }

        response = self.client.post(url, data, format='json',
                                    HTTP_AUTHORIZATION='JWT %s' % self.partner_jwt_token)
        self.assertEqual(response.status_code, HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'promo': ['Partner can create direct links only for promos created by himself.']})

    def test_create_by_staff(self):
        """
        Сотрудник создает direct link для партнера
        """
        promo = PromoFactory(partner=self.partner)
        url = reverse('public:directlink-list')
        data = {"promo": promo.uuid}

        response = self.client.post(url, data, format='json',
                                    HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token)

        created_dl = DirectLink.objects.get(promo=promo)

        expected_data = {
            'id': created_dl.uuid,
            'promo': PromoNameSerializer(instance=promo).data,
            'creation_params': {},
            'link': created_dl.generate_link
        }
        self.assertEqual(response.status_code, HTTP_201_CREATED)
        self.assertEqual(response.data, expected_data)

    def test_retrieve_destroy(self):
        direct_link = DirectLinkFactory(promo=PromoFactory(partner=self.partner))
        url = reverse('public:directlink-detail', kwargs={'pk': direct_link.uuid})

        response = self.client.get(url, format='json',
                                   HTTP_AUTHORIZATION='JWT %s' % self.partner_jwt_token)
        expected_data = {
            'id': direct_link.uuid,
            'promo': PromoNameSerializer(instance=direct_link.promo).data,
            'creation_params': direct_link.creation_params,
            'link': direct_link.generate_link,
        }
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertEqual(response.data, expected_data)

        response = self.client.delete(url, format='json',
                                      HTTP_AUTHORIZATION='JWT %s' % self.partner_jwt_token)
        self.assertEqual(response.status_code, HTTP_204_NO_CONTENT)
        self.assertEqual(response.data, None)

    def test_update_partial(self):
        direct_link = DirectLinkFactory(promo=PromoFactory(partner=self.partner))
        url = reverse('public:directlink-detail', kwargs={'pk': direct_link.uuid})

        new_creation_params = {'new_param': 'new_value'}
        data = {'creation_params': new_creation_params}
        response = self.client.put(url, data, format='json',
                                   HTTP_AUTHORIZATION='JWT %s' % self.partner_jwt_token)
        expected_data = {
            'id': direct_link.uuid,
            'promo': PromoNameSerializer(instance=direct_link.promo).data,
            'creation_params': new_creation_params,
            'link': direct_link.generate_link
        }
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertEqual(response.data, expected_data)

        new_promo = PromoFactory(partner=self.partner)
        data = {'promo': new_promo.uuid}
        response = self.client.put(url, data, format='json',
                                   HTTP_AUTHORIZATION='JWT %s' % self.partner_jwt_token)
        expected_data = {
            'id': direct_link.uuid,
            'promo': PromoNameSerializer(instance=new_promo).data,
            'creation_params': new_creation_params,
            'link': direct_link.generate_link
        }
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertEqual(response.data, expected_data)


class S3StaticTestCase(AffiliateAPITestCase):
    def test_create(self):
        created_at = utc_datetime(2018, 10, 10, 10, 10, 10)
        url = reverse('public:s3static-list')
        category = 'test'
        tmp_image = generate_tmp_image()

        with open(tmp_image.name, 'rb') as file, freeze_time(created_at):
            response = self.client.post(url, {'file': file, 'category': category}, format='multipart',
                                        HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token)

        created_s3_static = S3Static.objects.last()

        expected_data = {
            'id': created_s3_static.uuid,
            'name': os.path.basename(tmp_image.name),
            'link': created_s3_static.link,
            'category': category,
            'created_at': utc_datetime_to_iso(created_at),
            'modified_at': utc_datetime_to_iso(created_at),
            'banners': []
        }

        self.assertEqual(response.status_code, HTTP_201_CREATED)
        self.assertEqual(response.data, expected_data)

    def test_update_partial(self):
        """
        Создаем в базе фабричную статику.
        Делаем запрос на изменение ее файла.
        Делаем запрос на изменение ее категории.
        """
        old_category = 'old_category'
        new_category = 'new_category'
        old_name = 'old_name'
        old_link = 'old_category/image.png'

        created_at = utc_datetime(2018, 10, 10, 10, 10, 10)
        with freeze_time(created_at):
            s3_static = S3StaticFactory(name=old_name, category=old_category, link=old_link)
        url = reverse('public:s3static-detail', kwargs={'pk': s3_static.uuid})

        modified_file_at = utc_datetime(2018, 11, 11, 11, 11, 11)
        tmp_image = generate_tmp_image()
        with open(tmp_image.name, 'rb') as file, freeze_time(modified_file_at):
            response = self.client.put(url, {'file': file}, format='multipart',
                                       HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token)

        s3_static.refresh_from_db()

        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertEqual(s3_static.created_at, created_at)
        self.assertEqual(s3_static.modified_at, modified_file_at)
        self.assertEqual(s3_static.name, os.path.basename(tmp_image.name))
        self.assertNotEqual(s3_static.link, old_link)
        self.assertEqual(s3_static.category, old_category)

        modified_category_at = utc_datetime(2018, 11, 11, 11, 11, 11)
        with freeze_time(modified_file_at):
            response = self.client.put(url, {'category': new_category}, format='multipart',
                                       HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token)

        s3_static.refresh_from_db()

        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertEqual(s3_static.created_at, created_at)
        self.assertEqual(s3_static.modified_at, modified_category_at)
        self.assertEqual(s3_static.name, os.path.basename(tmp_image.name))
        self.assertNotEqual(s3_static.link, old_link)
        self.assertEqual(s3_static.category, new_category)

    def test_retrieve_destroy(self):
        s3_static = S3StaticFactory()
        url = reverse('public:s3static-detail', kwargs={'pk': s3_static.uuid})

        response = self.client.get(url, format='json',
                                   HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token)
        expected_data = {
            'id': s3_static.uuid,
            'link': s3_static.link,
            'name': s3_static.name,
            'category': s3_static.category,
            'created_at': utc_datetime_to_iso(s3_static.created_at),
            'modified_at': utc_datetime_to_iso(s3_static.modified_at),
            'banners': []
        }
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertEqual(response.data, expected_data)

        response = self.client.delete(url, format='json',
                                      HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token)
        self.assertEqual(response.status_code, HTTP_204_NO_CONTENT)
        self.assertEqual(response.data, None)

    def test_incorrect_destroy_banners_exists(self):
        """
        Попытка удалить s3_static на которую ссылаются баннеры должна возращать сообщение об ошибке.
        """
        s3_static = S3StaticFactory()
        banner = BannerFactory(image=s3_static)
        url = reverse('public:s3static-detail', kwargs={'pk': s3_static.uuid})

        response = self.client.delete(url, format='json',
                                      HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token)
        self.assertEqual(response.status_code, HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.data,
            ['There is 1 banners which are applied on this static. Switch them to another static before deleting.']
        )


class BannerViewSetTestCase(AffiliateAPITestCase):
    def test_create(self):
        promo = PromoFactory(partner=self.partner)
        image = S3StaticFactory()
        creation_params = {
            'ololo': 'SPAM',
            'trololo': '123'
        }

        url = reverse('public:banner-list')
        data = {
            "promo": promo.uuid,
            "image": image.uuid,
            "creation_params": creation_params
        }

        response = self.client.post(url, data, format='json',
                                    HTTP_AUTHORIZATION='JWT %s' % self.partner_jwt_token)

        created_banner = Banner.objects.get(promo=promo, image=image)

        expected_data = {
            'id': created_banner.uuid,
            'promo': PromoNameSerializer(instance=promo).data,
            'image': S3StaticNameSerializer(instance=image).data,
            'creation_params': creation_params,
            'html': created_banner.generate_html
        }
        self.assertEqual(response.status_code, HTTP_201_CREATED)
        self.assertEqual(response.data, expected_data)

    def test_retrieve_destroy(self):
        banner = BannerFactory(promo=PromoFactory(partner=self.partner))
        url = reverse('public:banner-detail', kwargs={'pk': banner.uuid})

        response = self.client.get(url, format='json',
                                   HTTP_AUTHORIZATION='JWT %s' % self.partner_jwt_token)
        expected_data = {
            'id': banner.uuid,
            'promo': PromoNameSerializer(instance=banner.promo).data,
            'image': S3StaticNameSerializer(instance=banner.image).data,
            'creation_params': banner.creation_params,
            'html': banner.generate_html
        }
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertEqual(response.data, expected_data)

        response = self.client.delete(url, format='json',
                                      HTTP_AUTHORIZATION='JWT %s' % self.partner_jwt_token)
        self.assertEqual(response.status_code, HTTP_204_NO_CONTENT)
        self.assertEqual(response.data, None)

    def test_update(self):
        banner = BannerFactory(promo=PromoFactory(partner=self.partner))
        url = reverse('public:banner-detail', kwargs={'pk': banner.uuid})
        new_promo = PromoFactory(partner=self.partner)
        new_image = S3StaticFactory()
        new_creation_params = {'new_param': 'new_value'}
        data = {
            'promo': new_promo.uuid,
            'image': new_image.uuid,
            'creation_params': new_creation_params
        }
        response = self.client.put(url, data, format='json',
                                   HTTP_AUTHORIZATION='JWT %s' % self.partner_jwt_token)

        banner.refresh_from_db()

        expected_data = {
            'id': banner.uuid,
            'promo': PromoNameSerializer(instance=new_promo).data,
            'image': S3StaticNameSerializer(instance=new_image).data,
            'creation_params': new_creation_params,
            'html': banner.generate_html
        }
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertEqual(response.data, expected_data)


class LandingTestCase(AffiliateAPITestCase):
    def test_create(self):
        url = reverse('public:landing-list')
        name = 'some landing'
        link = 'http://google.com'
        data = {
            'name': name,
            'link': link
        }

        response = self.client.post(url, data, format='json', HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token)

        created_landing = Landing.objects.get(name=name)

        expected_data = {
            'id': created_landing.uuid,
            'name': name,
            'link': link
        }
        self.assertEqual(response.status_code, HTTP_201_CREATED)
        self.assertEqual(response.data, expected_data)

    def test_update(self):
        landing = LandingFactory()
        new_name = 'new_name'
        new_link = 'https://upyachka.io/'
        url = reverse('public:landing-detail', kwargs={'pk': landing.id})
        data = {
            "name": new_name,
            "link": new_link,
        }

        response = self.client.put(url, data, format='json',
                                   HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token)

        updated_landing = Landing.objects.get(name=new_name)

        expected_data = {
            'id': updated_landing.uuid,
            "name": new_name,
            'link': new_link
        }
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertEqual(response.data, expected_data)

    def test_retrieve_destroy(self):
        landing = LandingFactory()
        url = reverse('public:landing-detail', kwargs={'pk': landing.uuid})

        response = self.client.get(url, format='json',
                                   HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token)
        expected_data = {
            'id': landing.uuid,
            'name': landing.name,
            'link': landing.link,
        }
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertEqual(response.data, expected_data)

        response = self.client.delete(url, format='json',
                                      HTTP_AUTHORIZATION='JWT %s' % self.staff_jwt_token)
        self.assertEqual(response.status_code, HTTP_204_NO_CONTENT)
        self.assertEqual(response.data, None)
