import datetime
import json

from django.test import TestCase, tag
from django.urls import reverse
from freezegun import freeze_time
from rest_framework.test import APIRequestFactory

from base.consts import UTC_DATETIME_FORMAT
from base.utils.datetime import utc_datetime
from base.utils.mongo import MongoLogging
from base.utils.tests import ClickhouseInfrastructure, generate_tmp_image
from players.tests.factories import PlayerFactory
from promos.bl import UpdatePromo, CreatePromo, CreateDirectLink, UpdateDirectLink, CreateBanner, UpdateBanner, \
    CreateLanding, UpdateLanding, CreateS3Static, UpdateS3Static, DestroyPromo, DestroyDirectLink, DestroyS3Static, \
    DestroyBanner, DestroyLanding, CollectTraffic
from promos.consts import PromoAction, PROMO_COLLECTION, DIRECT_LINK_COLLECTION, DirectLinkAction, BANNER_COLLECTION, \
    BannerAction, LANDING_COLLECTION, LandingAction, S3_STATIC_COLLECTION, S3StaticAction
from promos.models import Promo, Banner, DirectLink, Landing
from promos.rest.serializers import PromoSerializer, DirectLinkSerializer, BannerSerializer, BannerCreateSerializer, \
    PromoCreateSerializer, DirectLinkCreateSerializer, LandingSerializer, S3StaticSerializer, TrafficParamsSerializer
from promos.tests.factories import PromoFactory, DirectLinkFactory, S3StaticFactory, BannerFactory, LandingFactory
from promos.utils import download_static_from_s3_service
from users.tests.factories import PartnerFactory


class CreatePromoTestCase(TestCase):
    @tag('local')
    def test_mongo_logging(self):
        now = datetime.datetime(2018, 10, 10, 10, 10, 10)
        partner = PartnerFactory()
        data = {
            "partner": partner.uuid,
            "name": 'some cool promo',
        }
        url = reverse('public:promo-list')
        request_factory = APIRequestFactory()
        request = request_factory.post(url, data, format='json')
        request.user = partner.user
        serializer = PromoCreateSerializer(data=json.loads(request.body))
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data

        with freeze_time(now):
            promo = CreatePromo(request, validated_data).run()

        logged_data = PromoSerializer(instance=promo).data
        logged_promo = MongoLogging().get(collection=PROMO_COLLECTION, data=logged_data)
        logged_promo.pop('_id', None)

        self.assertEqual(logged_promo, dict(**logged_data, **{'action': PromoAction.CREATE, 'timestamp': now}))
        self.assertEqual(data['partner'], logged_promo['partner']['id'])
        self.assertEqual(data['name'], logged_promo['name'])


class UpdatePromoTestCase(TestCase):
    @tag('local')
    def test_mongo_logging(self):
        now = datetime.datetime(2018, 10, 10, 10, 10, 10)
        promo = PromoFactory()
        name = 'some cool promo'
        validated_data = {
            "name": name,
        }

        with freeze_time(now):
            promo = UpdatePromo(promo, validated_data).run()

        logged_data = PromoSerializer(instance=promo).data
        logged_promo = MongoLogging().get(collection=PROMO_COLLECTION, data=logged_data)
        logged_promo.pop('_id', None)

        self.assertEqual(logged_promo, dict(**logged_data, **{'action': PromoAction.UPDATE, 'timestamp': now}))
        self.assertEqual(validated_data['name'], logged_promo['name'])


class DestroyPromoTestCase(TestCase):
    @tag('local')
    def test_mongo_logging_players_move_to_default_promo(self):
        """
        При удалении promo должна появиться запись в логах в mongo.
        Cвязанные с promo direct-link и banner должны быть удалены а players перейти в дефолтную promo.
        """
        now = datetime.datetime(2018, 10, 10, 10, 10, 10)
        promo = PromoFactory()
        banner = BannerFactory(promo=promo)
        direct_link = DirectLinkFactory(promo=promo)
        player = PlayerFactory(promo=promo)

        with freeze_time(now):
            DestroyPromo(promo).run()

        logged_data = PromoSerializer(instance=promo).data
        logged_promo = MongoLogging().get(collection=PROMO_COLLECTION, data=logged_data)
        logged_promo.pop('_id', None)

        self.assertEqual(logged_promo, dict(**logged_data, **{'action': PromoAction.DESTROY, 'timestamp': now}))

        default_promo = Promo.objects.default()
        player.refresh_from_db()
        self.assertEqual(player.promo, default_promo)
        self.assertEqual(Banner.objects.filter(pk=banner.uuid).count(), 0)
        self.assertEqual(DirectLink.objects.filter(pk=direct_link.uuid).count(), 0)


class CreateDirectLinkTestCase(TestCase):
    @tag('local')
    def test_create_mongo_logging(self):
        now = datetime.datetime(2018, 10, 10, 10, 10, 10)
        partner = PartnerFactory()
        promo = PromoFactory(partner=partner)
        data = {
            "promo": promo.uuid,
            "creation_params": {
                'ololo': '12345',
                'trololo': '54321'
            }
        }
        url = reverse('public:directlink-list')
        request_factory = APIRequestFactory()
        request = request_factory.post(url, data, format='json')
        request.user = partner.user
        serializer = DirectLinkCreateSerializer(data=json.loads(request.body))
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data

        with freeze_time(now):
            direct_link = CreateDirectLink(request, validated_data).run()

        logged_data = DirectLinkSerializer(instance=direct_link).data
        logged_bet = MongoLogging().get(collection=DIRECT_LINK_COLLECTION, data=logged_data)
        logged_bet.pop('_id', None)

        self.assertEqual(logged_bet, dict(**logged_data, **{'action': DirectLinkAction.CREATE, 'timestamp': now}))
        self.assertEqual(data['promo'], logged_bet['promo']['id'])
        self.assertEqual(data['creation_params'], logged_bet['creation_params'])


class UpdateDirectLinkTestCase(TestCase):
    @tag('local')
    def test_update_mongo_logging(self):
        now = datetime.datetime(2018, 10, 10, 10, 10, 10)
        partner = PartnerFactory()
        direct_link = DirectLinkFactory(promo=PromoFactory(partner=partner))
        new_promo = PromoFactory(partner=partner)
        validated_data = {
            "promo": new_promo,
            "creation_params": {
                'new': 'param'
            }
        }

        with freeze_time(now):
            direct_link = UpdateDirectLink(direct_link, validated_data).run()

        logged_data = DirectLinkSerializer(instance=direct_link).data
        logged_bet = MongoLogging().get(collection=DIRECT_LINK_COLLECTION, data=logged_data)
        logged_bet.pop('_id', None)

        self.assertEqual(logged_bet, dict(**logged_data, **{'action': DirectLinkAction.UPDATE, 'timestamp': now}))
        self.assertEqual(validated_data['promo'].uuid, logged_bet['promo']['id'])
        self.assertEqual(validated_data['creation_params'], logged_bet['creation_params'])


class DestroyDirectLinkTestCase(TestCase):
    @tag('local')
    def test_mongo_logging(self):
        now = datetime.datetime(2018, 10, 10, 10, 10, 10)
        direct_link = DirectLinkFactory()

        with freeze_time(now):
            DestroyDirectLink(direct_link).run()

        logged_data = DirectLinkSerializer(instance=direct_link).data
        logged_direct_link = MongoLogging().get(collection=DIRECT_LINK_COLLECTION, data=logged_data)
        logged_direct_link.pop('_id', None)

        self.assertEqual(logged_direct_link, dict(**logged_data, **{'action': DirectLinkAction.DESTROY, 'timestamp': now}))


class CreateBannerTestCase(TestCase):
    @tag('local')
    def test_create_mongo_logging(self):
        now = datetime.datetime(2018, 10, 10, 10, 10, 10)
        partner = PartnerFactory()
        promo = PromoFactory(partner=partner)
        image = S3StaticFactory()
        data = {
            "promo": promo.uuid,
            "image": image.uuid,
            "creation_params": {
                'ololo': '12345',
                'trololo': '54321'
            }
        }
        url = reverse('public:banner-list')
        request_factory = APIRequestFactory()
        request = request_factory.post(url, data, format='json')
        request.user = partner.user
        serializer = BannerCreateSerializer(data=json.loads(request.body))
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data

        with freeze_time(now):
            banner = CreateBanner(request, validated_data).run()

        logged_data = BannerSerializer(instance=banner).data
        logged_bet = MongoLogging().get(collection=BANNER_COLLECTION, data=logged_data)
        logged_bet.pop('_id', None)

        self.assertEqual(logged_bet, dict(**logged_data, **{'action': BannerAction.CREATE, 'timestamp': now}))
        self.assertEqual(data['promo'], logged_bet['promo']['id'])
        self.assertEqual(data['image'], logged_bet['image']['id'])
        self.assertEqual(data['creation_params'], logged_bet['creation_params'])


class UpdateBannerTestCase(TestCase):
    @tag('local')
    def test_update_mongo_logging(self):
        now = datetime.datetime(2018, 10, 10, 10, 10, 10)
        banner = BannerFactory()
        partner = PartnerFactory()
        promo = PromoFactory(partner=partner)
        image = S3StaticFactory()
        validated_data = {
            "promo": promo,
            "image": image,
            "creation_params": {
                'ololo': '12345',
                'trololo': '54321'
            }
        }

        with freeze_time(now):
            banner = UpdateBanner(banner, validated_data).run()

        logged_data = BannerSerializer(instance=banner).data
        logged_bet = MongoLogging().get(collection=BANNER_COLLECTION, data=logged_data)
        logged_bet.pop('_id', None)

        self.assertEqual(logged_bet, dict(**logged_data, **{'action': BannerAction.UPDATE, 'timestamp': now}))
        self.assertEqual(validated_data['promo'].uuid, logged_data['promo']['id'])
        self.assertEqual(validated_data['image'].uuid, logged_data['image']['id'])
        self.assertEqual(validated_data['creation_params'], logged_data['creation_params'])


class DestroyBannerTestCase(TestCase):
    @tag('local')
    def test_mongo_logging(self):
        now = datetime.datetime(2018, 10, 10, 10, 10, 10)
        banner = BannerFactory()

        with freeze_time(now):
            DestroyBanner(banner).run()

        logged_data = BannerSerializer(instance=banner).data
        logged_banner = MongoLogging().get(collection=BANNER_COLLECTION, data=logged_data)
        logged_banner.pop('_id', None)

        self.assertEqual(logged_banner, dict(**logged_data, **{'action': BannerAction.DESTROY, 'timestamp': now}))


class CreateLandingTestCase(TestCase):
    @tag('local')
    def test_create_mongo_logging(self):
        now = datetime.datetime(2018, 10, 10, 10, 10, 10)
        validated_data = {
            'name': 'some landing',
            'link': 'http://google.com'
        }

        with freeze_time(now):
            landing = CreateLanding(validated_data).run()

        logged_data = LandingSerializer(instance=landing).data
        logged_bet = MongoLogging().get(collection=LANDING_COLLECTION, data=logged_data)
        logged_bet.pop('_id', None)

        self.assertEqual(logged_bet, dict(**logged_data, **{'action': LandingAction.CREATE, 'timestamp': now}))
        self.assertEqual(validated_data['name'], logged_bet['name'])
        self.assertEqual(validated_data['link'], logged_bet['link'])


class UpdateLandingTestCase(TestCase):
    @tag('local')
    def test_update_mongo_logging(self):
        now = datetime.datetime(2018, 10, 10, 10, 10, 10)
        landing = LandingFactory()
        validated_data = {
            'name': 'new landing',
            'link': 'http://yandex.com'
        }

        with freeze_time(now):
            landing = UpdateLanding(landing, validated_data).run()

        logged_data = LandingSerializer(instance=landing).data
        logged_bet = MongoLogging().get(collection=LANDING_COLLECTION, data=logged_data)
        logged_bet.pop('_id', None)

        self.assertEqual(logged_bet, dict(**logged_data, **{'action': BannerAction.UPDATE, 'timestamp': now}))
        self.assertEqual(validated_data['name'], logged_bet['name'])
        self.assertEqual(validated_data['link'], logged_bet['link'])


class DestroyLandingTestCase(TestCase):
    @tag('local')
    def test_mongo_logging_promos_move_to_default_landing(self):
        """
        При удалении landing должна появиться запись в логах в mongo.
        Cвязанные с landing promo должны перейти в дефолтный landing.
        """
        now = datetime.datetime(2018, 10, 10, 10, 10, 10)
        landing = LandingFactory()
        promo = PromoFactory(landing=landing)

        with freeze_time(now):
            DestroyLanding(landing).run()

        logged_data = LandingSerializer(instance=landing).data
        logged_landing = MongoLogging().get(collection=LANDING_COLLECTION, data=logged_data)
        logged_landing.pop('_id', None)

        self.assertEqual(logged_landing, dict(**logged_data, **{'action': LandingAction.DESTROY, 'timestamp': now}))

        default_landing = Landing.objects.default()
        promo.refresh_from_db()
        self.assertEqual(promo.landing, default_landing)


class CreateS3StaticTestCase(TestCase):
    @tag('local')
    def test_create_s3_static_exists_mongo_logging(self):
        """
        Проверим что созданную статику можно скачать и ее содержимое идентично загруженной,
        а запись в логи монги реально существует.
        """
        now = datetime.datetime(2018, 10, 10, 10, 10, 10)
        tmp_image = generate_tmp_image()

        with freeze_time(now), open(tmp_image.name, 'rb') as file:
            validated_data = {
                'category': 'test',
                'file': file
            }
            s3_static = CreateS3Static(validated_data).run()

        with open(tmp_image.name, 'rb') as file:
            original_file_binary_data = file.read()
        downloaded_file_binary_data = download_static_from_s3_service(s3_static.link).read()
        self.assertEqual(original_file_binary_data, downloaded_file_binary_data)

        logged_data = S3StaticSerializer(instance=s3_static).data
        logged_bet = MongoLogging().get(collection=S3_STATIC_COLLECTION, data=logged_data)
        logged_bet.pop('_id', None)

        self.assertEqual(logged_bet, dict(**logged_data, **{'action': S3StaticAction.CREATE, 'timestamp': now}))
        self.assertEqual(tmp_image.name, logged_bet['name'])
        self.assertEqual(s3_static.link, logged_bet['link'])
        self.assertEqual(validated_data['category'], logged_bet['category'])


class UpdateS3StaticTestCase(TestCase):
    @tag('local')
    def test_update_s3_static_exists_mongo_logging(self):
        """
        Проверим что модифицированную статику можно скачать и ее содержимое идентично загруженной,
        а запись в логи монги реально существует.
        """
        now = datetime.datetime(2018, 10, 10, 10, 10, 10)
        s3_static = S3StaticFactory()
        tmp_image = generate_tmp_image()

        with freeze_time(now), open(tmp_image.name, 'rb') as file:
            validated_data = {
                'category': 'trololo',
                'file': file
            }
            s3_static = UpdateS3Static(s3_static, validated_data).run()

        with open(tmp_image.name, 'rb') as file:
            original_file_binary_data = file.read()
        downloaded_file_binary_data = download_static_from_s3_service(s3_static.link).read()
        self.assertEqual(original_file_binary_data, downloaded_file_binary_data)

        logged_data = S3StaticSerializer(instance=s3_static).data
        logged_bet = MongoLogging().get(collection=S3_STATIC_COLLECTION, data=logged_data)
        logged_bet.pop('_id', None)

        self.assertEqual(logged_bet, dict(**logged_data, **{'action': S3StaticAction.UPDATE, 'timestamp': now}))
        self.assertEqual(tmp_image.name, logged_bet['name'])
        self.assertEqual(s3_static.link, logged_bet['link'])
        self.assertEqual(validated_data['category'], logged_bet['category'])


class DestroyS3StaticTestCase(TestCase):
    @tag('local')
    def test_mongo_logging(self):
        now = datetime.datetime(2018, 10, 10, 10, 10, 10)
        s3_static = S3StaticFactory()

        with freeze_time(now):
            DestroyS3Static(s3_static).run()

        logged_data = S3StaticSerializer(instance=s3_static).data
        logged_s3_static = MongoLogging().get(collection=S3_STATIC_COLLECTION, data=logged_data)
        logged_s3_static.pop('_id', None)

        self.assertEqual(logged_s3_static, dict(**logged_data, **{'action': S3StaticAction.DESTROY, 'timestamp': now}))


class CollectTrafficTestCase(TestCase):
    @tag('local')
    def test_clickhouse_partner(self):
        """
        Фильтруем по партнеру
        """
        partner1 = PartnerFactory()
        partner2 = PartnerFactory()
        timestamp__gte = utc_datetime(2018, 10, 10, 10, 10, 10)
        timestamp__lte = utc_datetime(2018, 11, 11, 11, 11, 11)

        with ClickhouseInfrastructure(
            partners=[partner1, partner2],
            timestamp_range=[timestamp__gte, timestamp__lte],
            create_promos=True,
        ) as clickhouse_client:
            serializer = TrafficParamsSerializer(
                data={
                    'partner': partner1.uuid,
                }
            )
            serializer.is_valid(raise_exception=True)
            validated_data = serializer.validated_data

            traffic = CollectTraffic(partner1.user, partner1.promos.all(), validated_data).run()
            self.assertEqual(traffic.downloads, 8)
            self.assertEqual(traffic.clicks, 10)

    @tag('local')
    def test_clickhouse_partner_is_unique(self):
        """
        Фильтруем по партнеру, timestamp и уникальности
        """
        partner1 = PartnerFactory()
        partner2 = PartnerFactory()
        timestamp__gte = utc_datetime(2018, 10, 10, 10, 10, 10)
        timestamp__lte = utc_datetime(2018, 11, 11, 11, 11, 11)

        with ClickhouseInfrastructure(
            partners=[partner1, partner2],
            timestamp_range=[timestamp__gte, timestamp__lte],
            create_promos=True,
        ) as clickhouse_client:
            serializer = TrafficParamsSerializer(
                data={
                    'partner': partner1.uuid,
                    'timestamp__gte': timestamp__gte.strftime(UTC_DATETIME_FORMAT),
                    'timestamp__lte': timestamp__lte.strftime(UTC_DATETIME_FORMAT),
                    'is_unique': True,
                }
            )
            serializer.is_valid(raise_exception=True)
            validated_data = serializer.validated_data

            traffic = CollectTraffic(partner1.user, partner1.promos.all(), validated_data).run()
            self.assertEqual(traffic.downloads, 4)
            self.assertEqual(traffic.clicks, 6)

    @tag('local')
    def test_clickhouse_promo(self):
        """
        Фильтруем по promo
        """
        partner1 = PartnerFactory()
        partner2 = PartnerFactory()
        timestamp__gte = utc_datetime(2018, 10, 10, 10, 10, 10)
        timestamp__lte = utc_datetime(2018, 11, 11, 11, 11, 11)

        with ClickhouseInfrastructure(
            partners=[partner1, partner2],
            timestamp_range=[timestamp__gte, timestamp__lte],
            create_promos=True,
        ) as clickhouse_client:
            serializer = TrafficParamsSerializer(
                data={
                    'promo': partner1.promos.first().uuid,
                }
            )
            serializer.is_valid(raise_exception=True)
            validated_data = serializer.validated_data

            traffic = CollectTraffic(partner1.user, (partner1.promos.first(),), validated_data).run()
            self.assertEqual(traffic.downloads, 4)
            self.assertEqual(traffic.clicks, 5)
