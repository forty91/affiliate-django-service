import random

import factory
from factory import LazyAttribute, SubFactory

from projects.tests.factories import ProgramFactory
from promos.models import Promo, DirectLink, S3Static, Banner, Landing
from users.tests.factories import PartnerFactory
from base.utils.string import random_len_string


class LandingFactory(factory.django.DjangoModelFactory):
    name = LazyAttribute(lambda x: random_len_string(4, 20))
    link = LazyAttribute(lambda x: 'http://%s.com' % random_len_string(7, 10))

    class Meta:
        model = Landing


class PromoFactory(factory.django.DjangoModelFactory):
    name = LazyAttribute(lambda x: random_len_string(4, 20))
    partner = factory.SubFactory(PartnerFactory)
    program = factory.SubFactory(ProgramFactory)
    landing = factory.SubFactory(LandingFactory)

    class Meta:
        model = Promo


class DirectLinkFactory(factory.django.DjangoModelFactory):
    promo = SubFactory(PromoFactory)
    creation_params = {}

    class Meta:
        model = DirectLink


class S3StaticFactory(factory.django.DjangoModelFactory):
    name = LazyAttribute(lambda x: '%s.png' % random_len_string(5, 10))
    link = LazyAttribute(lambda x: 'http://%s.png' % random_len_string(10, 20))
    category = LazyAttribute(lambda x: random_len_string(5, 10))

    class Meta:
        model = S3Static


class BannerFactory(factory.django.DjangoModelFactory):
    promo = SubFactory(PromoFactory)
    image = SubFactory(S3StaticFactory)
    creation_params = {}

    class Meta:
        model = Banner
