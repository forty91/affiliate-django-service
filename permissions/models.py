from django.db import models
from django.conf import settings

from base.help_texts import NAME, ROLE_IS_SUPER
from base.models import AffiliateModel
from permissions.consts import Action, Block, Status


class RoleManager(models.Manager):
    def default(self):
        return self.get(name=settings.DEFAULT_ROLE)


class Role(AffiliateModel):
    name = models.CharField(max_length=255, help_text=NAME)
    is_super = models.BooleanField(default=False, help_text=ROLE_IS_SUPER)

    objects = RoleManager()


class Permission(AffiliateModel):
    role = models.ForeignKey(Role, on_delete=models.CASCADE, related_name='permissions')
    block = models.SlugField(choices=Block.choices)
    action = models.SlugField(choices=Action.choices)
    status = models.SlugField(choices=Status.choices, default=Status.ACTIVE)
