from base.utils.enumerate import BaseEnumerate


class Action(BaseEnumerate):
    READ = 'read'
    CREATE = 'create'
    UPDATE = 'update'
    DELETE = 'delete'
    EXECUTE = 'execute'


class Block(BaseEnumerate):
    PLAYERS = 'players'
    BETS = 'bets'
    PROMOS = 'promos'
    PARTNERS = 'partners'
    STAFF = 'staff'
    PAYMENTS = 'payments'
    CONSTANCE = 'constance'
    REPORTS = 'reports'
    FLOWS = 'flows'


class Status(BaseEnumerate):
    ACTIVE = 'active'
    BLOCKED = 'blocked'
    DELETED = 'deleted'
