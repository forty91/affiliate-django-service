# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations
from django.conf import settings


def create_super_role_for_default_staff(apps, schema_editor):
    Role = apps.get_model('permissions', 'Role')
    Staff = apps.get_model('users', 'Staff')

    default_role = Role.objects.create(name=settings.DEFAULT_ROLE, is_super=True)
    default_staff = Staff.objects.get(user__username=settings.DEFAULT_STAFF_NAME)
    default_staff.role = default_role
    default_staff.save()


class Migration(migrations.Migration):

    dependencies = [
        ('permissions', '0001_initial'),
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(create_super_role_for_default_staff)
    ]
