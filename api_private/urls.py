from django.conf.urls import url
from rest_framework import routers

from api_private.views import AliveView, PlayersView, BetsView, PaymentsView, DirectLinksView, BannersView

urlpatterns = [
    url(r'^health-check/', AliveView.as_view()),
    url(r'^players/', PlayersView.as_view()),
    url(r'^bets/', BetsView.as_view()),
    url(r'^payments/', PaymentsView.as_view()),
    url(r'^direct-links/(?P<pk>[^/.]+)/$', DirectLinksView.as_view()),
    url(r'^banners/(?P<pk>[^/.]+)/$', BannersView.as_view()),
]
