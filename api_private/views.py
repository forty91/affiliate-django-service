from django.http import HttpResponse, JsonResponse
from django.views import View
from drf_yasg.utils import swagger_auto_schema
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.status import HTTP_201_CREATED, HTTP_200_OK
from rest_framework.views import APIView

from base.utils.db import get_object_or_reject
from bets.bl import CreateUpdateBet
from bets.rest.serializers import BetCreateModifySerializer, BetSerializer
from payments.bl import CreateUpdatePayment
from payments.rest.serializers import PaymentCreateModifySerializer, PaymentSerializer
from players.bl import CreatePlayer
from players.rest.serializers import PlayerCreateSerializer, PlayerSerializer
from promos.models import DirectLink, Banner
from promos.rest.serializers import PrivateDirectLinkSerializer, PrivateBannerSerializer


class AliveView(View):
    def get(self, *args, **kwargs):
        return HttpResponse('alive')


class PlayersView(APIView):
    """
    Private endpoint for add players.
    """
    authentication_classes = ()
    permission_classes = (AllowAny,)

    @swagger_auto_schema(request_body=PlayerCreateSerializer,
                         responses={201: PlayerSerializer})
    def post(self, request, *args, **kwargs):
        serializer = PlayerCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        player = CreatePlayer(serializer.validated_data).run()
        return Response(data=PlayerSerializer(instance=player).data,
                        status=HTTP_201_CREATED)


class BetsView(APIView):
    """
    Private endpoint for add/modify bets.
    """
    authentication_classes = ()
    permission_classes = (AllowAny,)

    @swagger_auto_schema(request_body=BetCreateModifySerializer,
                         responses={HTTP_200_OK: BetSerializer, HTTP_201_CREATED: BetSerializer})
    def post(self, request, *args, **kwargs):
        serializer = BetCreateModifySerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        bet, status = CreateUpdateBet(serializer.validated_data).run()
        return Response(data=BetSerializer(instance=bet).data,
                        status=status)


class PaymentsView(APIView):
    """
    Private endpoint for add/modify payments.
    """
    authentication_classes = ()
    permission_classes = (AllowAny,)

    @swagger_auto_schema(request_body=PaymentCreateModifySerializer,
                         responses={HTTP_200_OK: PaymentSerializer, HTTP_201_CREATED: PaymentSerializer})
    def post(self, request, *args, **kwargs):
        serializer = PaymentCreateModifySerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        payment, status = CreateUpdatePayment(serializer.validated_data).run()
        return Response(data=PaymentSerializer(instance=payment).data,
                        status=status)


class DirectLinksView(APIView):
    """
    Private endpoint for retrieve direct-link info.
    """
    authentication_classes = ()
    permission_classes = (AllowAny,)

    @swagger_auto_schema(responses={200: PrivateDirectLinkSerializer})
    def get(self, request, pk):
        direct_link = get_object_or_reject(DirectLink.objects.all(), pk)
        return Response(data=PrivateDirectLinkSerializer(instance=direct_link).data,
                        status=HTTP_200_OK)


class BannersView(APIView):
    """
    Private endpoint for retrieve banner info.
    """
    authentication_classes = ()
    permission_classes = (AllowAny,)

    @swagger_auto_schema(responses={200: PrivateBannerSerializer})
    def get(self, request, pk):
        banner = get_object_or_reject(Banner.objects.all(), pk)
        return Response(data=PrivateBannerSerializer(instance=banner).data,
                        status=HTTP_200_OK)
